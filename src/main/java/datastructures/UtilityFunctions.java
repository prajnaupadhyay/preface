package datastructures;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.net.URLEncoder;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Scanner;
import java.util.Set;
import java.util.StringTokenizer;
import java.util.Map.Entry;

import com.aliasi.cluster.CompleteLinkClusterer;
import com.aliasi.cluster.Dendrogram;
import com.aliasi.cluster.HierarchicalClusterer;
import com.aliasi.cluster.KMeansClusterer;
import com.aliasi.util.Distance;

import cc.mallet.types.NormalizedDotProductMetric;
import cc.mallet.types.SparseVector;

public class UtilityFunctions 
{
	
	public class Feature
	{
		public String name;
		public double[] values;
		
		public Feature(String name, double[] values)
		{
			this.name=name;
			this.values = values;
		}
	}
	static Comparator<Entry<String, Double>> valueComparator = new Comparator<Entry<String, Double>>() 
	{
		 public int compare(Entry<String, Double> e1, Entry<String, Double> e2)
		 {
			 Double v1 = e1.getValue();
			 Double v2 = e2.getValue();
			 return v2.compareTo(v1);
		 }
	};
	
	public UtilityFunctions()
	{
		
	}
	
	public static Distance<Feature> COSINE_DISTANCE1 = new Distance<Feature>() 
	{
		public double distance(Feature lm1, Feature lm2) 
		{
			NormalizedDotProductMetric norm = new NormalizedDotProductMetric();
			
			SparseVector sv1 = new SparseVector(lm1.values);
			SparseVector sv2 = new SparseVector(lm2.values);
			
			//System.out.println(dist);
			return norm.distance(sv1, sv2);
	}};
	
	
	public static Distance<LanguageModelEntity> COSINE_DISTANCE = new Distance<LanguageModelEntity>() 
	{
		public double distance(LanguageModelEntity lm1, LanguageModelEntity lm2) 
		{
		
		HashMap<String, Double> s1 = lm1.mixture;
		HashMap<String, Double> s2 = lm2.mixture;
		double diff =0.0;
		for(String aaa:s1.keySet())
		{
			double val1 = s1.get(aaa);
			double val2 = s2.get(aaa);
			diff = diff + (val1 - val2)*(val1 - val2);
		}
		double dist = Math.sqrt(diff);
		//System.out.println(dist);
		return dist;
	}};
	
	/**
	 * return 1 hop entities for a query
	 * @param query
	 * @param a
	 * @param incidentList
	 * @return
	 */
	
	public HashSet<String> returnOneHopEntities(String query, AdjList a, HashMap<String, ArrayList<String>> incidentList)
	{
		ArrayList<Edge> ee = a.getAdjList().get(query);
		ArrayList<String> in = incidentList.get(query);
		HashSet<String> hs = new HashSet<String>();
		if(in!=null) hs.addAll(in);
		if(ee!=null)
		{
			for(Edge e:ee)
			{
				hs.add(e.getName());
			}
		}
		return hs;
	}
	
	/**
	 * return two hop entities
	 * @param query
	 * @param a
	 * @param incidentList
	 * @return
	 */
	
	public HashSet<String> returnTwoHopEntities(String query, AdjList a, HashMap<String, ArrayList<String>> incidentList)
	{
		HashSet<String> h = this.returnOneHopEntities(query, a, incidentList);
		HashSet<String> h1 = new HashSet<String>();
		for(String hh:h)
		{
			h1.addAll(this.returnOneHopEntities(hh, a, incidentList));
		}
		h1.addAll(h);
		return h1;
	}
	
	/**
	 * intersects list1 and list2 and returns the intersection
	 * @param list1
	 * @param list2
	 * @return
	 */
	
	public static ArrayList<String> intersect(ArrayList<String> list1, ArrayList<String> list2)
	{
		list1.retainAll(list2);
		return list1;
	}
	
	/**
	 * Given a hashmap of string to embeddings, return the closest string name (after stemming and disambiguation) that has an entry for an embedding in s. Returns empty string otherwise
	 * @param embedding
	 * @param entity
	 * @return
	 */
	
	public static String extractEmbedding(HashMap<String, LanguageModelEntity> s, String entity)
	{
		AdjList a = new AdjList();
		String changedName = entity;
		
		if(s.get(entity)==null) 
		{
			if(s.get(a.removeDisambiguation(entity))==null)
			{
				if(s.get(a.stem(a.removeDisambiguation(entity)))==null)
				{
					if(s.get(entity.replace("-", "_"))==null)
					{
						if(s.get(a.removeDisambiguation(entity.replace("-", "_")))==null)
						{
							if(s.get(a.stem(a.removeDisambiguation(entity.replace("-", "_"))))==null)
							{
								return "";
							}
							else
							{
								changedName = a.stem(a.removeDisambiguation(entity.replace("-", "_")));
								return changedName;
							}
						}
						else
						{
							changedName = a.removeDisambiguation(entity.replace("-", "_"));
							return changedName;
						}
						
					}
					else
					{
						changedName = entity.replace("-", "_");
						return changedName;
					}
					
					
				}
				else
				{
					
					changedName = a.stem(a.removeDisambiguation(entity));
					return changedName;
				}
			}
			else
			{
				
				changedName = a.removeDisambiguation(entity);
				return changedName;
			}
			
		}
		else 
		{
			return changedName;
		}
	}
	
	
	
	/**
	 * 
	 * @param kbName: kb whose embeddings are to be combined
	 * @param entityMap: mapping the entities in the kb to unique integer value
	 * @param embedding: embeddings generated from kb
	 * @param entityMapText: mapping of entities from text
	 * @param textEmbedding: embeddings generated from text
	 * @param operator: "concatenate" will concatenate 2 vectors, "sum" will add two vectors
	 * @throws Exception
	 */
	public static void combineKBAndTextEmbeddings(String kbName, String entityMap, String embedding, String entityMapText, String textEmbedding, String operator, String combined) throws Exception
	{
		
		HashMap<String, String> stringToId = createStringToIdMapping(entityMap);
		HashMap<String, String> stringToIdText = createStringToIdMapping(entityMapText);
		HashMap<String, String> stringToId1 = createStringToIdMapping1(entityMap);
		HashMap<String, LanguageModelEntity> s = readEmbeddingsFromFile(embedding,stringToId,new AdjList());
		HashMap<String, LanguageModelEntity> sText = readEmbeddingsFromFile(textEmbedding, stringToIdText,new AdjList());
		
		BufferedWriter bw = new BufferedWriter(new FileWriter(combined));
		HashMap<String, LanguageModelEntity> combinedEmbeddings = new HashMap<String, LanguageModelEntity>();
		
		for(String s1:s.keySet())
		{
			//System.out.println(s1);
			String s2 = extractEmbedding(sText, s1);
			if(!s2.equals(""))
			{
				LanguageModelEntity l1 = sText.get(s2);
				LanguageModelEntity l2 = s.get(s1);
				LanguageModelEntity l3 = new LanguageModelEntity();
				//System.out.println("l1 is: "+l1);
				//System.out.println("l2 is: "+l2);
				if(operator.equals("concatenate"))
					l3 = l2.join(l1);
				else if (operator.equals("sum"))
					l3 = l2.add(l1);
				combinedEmbeddings.put(s1, l3);
				System.out.println(l3.mixture.size());
				//combinedEmbeddings.put(s2, l3);
			}
		}
		System.out.println(combinedEmbeddings.size());
		if (operator.equals("concatenate"))
			bw.write(combinedEmbeddings.size()+" 256\n");
		else if (operator.equals("sum"))
			bw.write(combinedEmbeddings.size()+" 128\n");
		for(String s1:combinedEmbeddings.keySet())
		{
			//System.out.println(s1);
			LanguageModelEntity l3 = combinedEmbeddings.get(s1);
			l3.writeToFile(bw, s1, stringToId1);
		}
		bw.close();
	}
	

	
	
	/**
	 * 
	 * @param inputFile: file that stores the input concepts. Set the "input-concepts" config variable
	 * @param embedding: file that stores the embeddings for each entities. Set "embedding" config variable
	 * @param entityMap: file that stores the mapping of entities to their unique numbers. Set the "embeddingEntities" config variable
	 * @param suffix: file will be generated. Will have this suffix. Set the "suffix" config variable
	 * @param kb: KB that will look for the siblings of the input concepts. Set the "latest-category-graph" config variable
	 * @throws Exception
	 */
	
	public static void computeKMostSimiliarEntity(String inputFile, String embedding, String entityMap, String suffix, String kb) throws Exception
	{
		ReadSubgraph rr = new ReadSubgraph();
		AdjList a = rr.readKB(new BufferedReader(new FileReader(kb)));
		HashMap<String, String> stringToId = createStringToIdMapping(entityMap);
		HashMap<String, LanguageModelEntity> emb = readEmbeddingsFromFile(embedding,stringToId,a);
		System.out.println("read embeddings");
		BufferedReader br1 = new BufferedReader(new FileReader(inputFile));
		String line;
		HashSet<String> h = new HashSet<String>();
		while((line=br1.readLine())!=null)
		{
			//h.add(line.replace("-", "_"));
			h.add(line);
		}
		//BufferedReader br2 = new BufferedReader(new FileReader("/mnt/dell/prajna/refdScores/mostSimiliarlatestCategoryGraphSiblingsNonUnique"));
		//BufferedReader br3 = new BufferedReader(new FileReader("/mnt/dell/prajna/refdScores/mostSimiliarlatestCategoryGraphSiblingsNonUnique1"));
		//String line1;
		/*HashSet<String> simFoundEntities = new HashSet<String>();
		while((line1=br2.readLine())!=null)
		{
			StringTokenizer tok = new StringTokenizer(line1,"\t");
			while(tok.hasMoreTokens())
			{
				simFoundEntities.add(tok.nextToken());
				break;
			}
		}
		while((line1=br3.readLine())!=null)
		{
			StringTokenizer tok = new StringTokenizer(line1,"\t");
			while(tok.hasMoreTokens())
			{
				simFoundEntities.add(tok.nextToken());
				break;
			}
		}*/
		//h.removeAll(simFoundEntities);
		ArrayList<String> inputConcepts = new ArrayList<String>(h);
		BufferedWriter bw = new BufferedWriter(new FileWriter("/mnt/dell/prajna/refdScores/mostSimilarEntities/mostSimiliar"+suffix));
		int c=0;
		for(String s:inputConcepts)
		{
			//System.out.println(c);
			c++;
			//System.out.println(s);
			LanguageModelEntity l = emb.get(s);
			if(l==null) 
			{
				System.out.println("embedding for "+s+" is missing!!!");
				continue;
			}
			ArrayList<String> result = l.KMostSimiliarSiblings(10, emb, a);
			bw.write(s+"\t");
			for(String r:result)
			{
				bw.write(r+"\t");
			}
			bw.write("\n");
		}
		bw.close();
	}
	
	
	
	
	/**
	 * 
	 * @param ll: adds the vectors of the language model entity represented by this class and ll
	 * @return
	 */
	
	/*public LanguageModelEntity add(LanguageModelEntity ll)
	{
		
	}*/
	

	/**
	 * builds a language model on the category graph and then finds distance between the query entity and the the entities situated at a distance of 1-hops from it in TeKnowbase. Can be used to detect spurious triples. 
	 * @throws Exception
	 */
	

	/**
	 * directly reads the embedding file instead of trying to map the entities to an entity list
	 * @param f
	 * @param h
	 * @return
	 * @throws Exception
	 */
	
	public static HashMap<String, LanguageModelEntity> readEmbeddingsFromFileAlternate(String f, AdjList a) throws Exception
	{
		BufferedReader br = new BufferedReader(new FileReader(f));
		HashMap<String, LanguageModelEntity> s = new HashMap<String, LanguageModelEntity>();
		String line;
		int c=0;
		while((line=br.readLine())!=null)
		{
			//System.out.println(line);
			if(c==0) 
			{
				c++;
				continue;
			}
			else
			{
				c++;
				StringTokenizer tok = new StringTokenizer(line," ");
				String cc = tok.nextToken();
			//	System.out.println(cc);
				String entity = cc;
			//	System.out.println(cc+"\t"+entity);
				LanguageModelEntity lm = new LanguageModelEntity(entity, a, new HashMap<String, Double>(), new HashMap<String, HashMap<String, Double>>(), new HashMap<String, Double>());
				int d = 0;
				while(tok.hasMoreTokens())
				{
					Double val = Double.parseDouble(tok.nextToken());
					lm.mixture.put("e"+d, val);
					d++;
					
				}
				//System.out.println(entity+" mixture size: "+lm.mixture.size());
				s.put(entity,lm);
			}
		}
		System.out.println("read embeddings size: "+s.size());
		return s;
	}
	
	/**
	 * reads the embeddings of all entities into a hashmap structure from a file
	 * @param f: file contains the embeddings
	 * @param h: hashmap of id to entity name
	 * @return
	 * @throws Exception
	 */
	
	public static HashMap<String, LanguageModelEntity> readEmbeddingsFromFile(String f, HashMap<String, String> h, AdjList a) throws Exception
	{
		BufferedReader br = new BufferedReader(new FileReader(f));
		HashMap<String, LanguageModelEntity> s = new HashMap<String, LanguageModelEntity>();
		String line;
		int c=0;
		while((line=br.readLine())!=null)
		{
			//System.out.println(line);
			if(c==0) 
			{
				c++;
				continue;
			}
			else
			{
				c++;
				StringTokenizer tok = new StringTokenizer(line," ");
				String cc = tok.nextToken();
				//System.out.println(cc);
				String entity = h.get(cc);
				//System.out.println(cc+"\t"+entity);
				LanguageModelEntity lm = new LanguageModelEntity(entity, a, new HashMap<String, Double>(), new HashMap<String, HashMap<String, Double>>(), new HashMap<String, Double>());
				int d = 0;
				while(tok.hasMoreTokens())
				{
					Double val = Double.parseDouble(tok.nextToken());
					lm.mixture.put("e"+d, val);
					d++;
					
				}
				//System.out.println(entity+" mixture size: "+lm.mixture.size());
				s.put(entity,lm);
			}
		}
		System.out.println("read embeddings size: "+s.size());
		return s;
	}
	
	/**
	 * return the centroid for a cluster. Centroid is the average of all data points
	 * @param s
	 * @return
	 */
	
	public static HashMap<String, Double> calculateCentroid(Set<LanguageModelEntity> s)
	{
		HashMap<String, Double> centroid = new HashMap<String, Double>();
		ArrayList<LanguageModelEntity> s1 = new ArrayList<LanguageModelEntity>(s);
		for(String ss:s1.get(0).mixture.keySet())
		{
			centroid.put(ss, 0.0);
		}
		for(LanguageModelEntity l:s)
		{
			for(String ss:l.mixture.keySet())
			{
				double val = centroid.get(ss)+l.mixture.get(ss);
				centroid.put(ss, val);
			}
		}
		for(String ss:centroid.keySet())
		{
			double avg = centroid.get(ss)/s.size();
			centroid.put(ss, avg);
		}
		return centroid;
	}
	
	/**
	 * given a set s of vectors, this function returns the summation of the vectors in the collection
	 * @param s
	 * @return
	 */
	
	public static HashMap<String, Double> calculateSummationVector(Set<LanguageModelEntity> s)
	{
		HashMap<String, Double> centroid = new HashMap<String, Double>();
		ArrayList<LanguageModelEntity> s1 = new ArrayList<LanguageModelEntity>(s);
		if(s.size()==0)
		{
			for(int i=0;i<128;i++)
			{
				centroid.put("e"+i, 0.0);
			}
		}
		else
		{
			for(String ss:s1.get(0).mixture.keySet())
			{
				centroid.put(ss, 0.0);
			}
			for(LanguageModelEntity l:s)
			{
				for(String ss:l.mixture.keySet())
				{
					double val = centroid.get(ss)+l.mixture.get(ss);
					centroid.put(ss, val);
				}
			}
		}
		/*for(String ss:centroid.keySet())
		{
			double avg = centroid.get(ss)/s.size();
			centroid.put(ss, avg);
		}*/
		return centroid;
	}
	
	/**
	 * calculates the euclidean distance between two points
	 * @param centroid
	 * @param lm
	 * @return
	 */
	
	public static double euclideanDistance(HashMap<String, Double> centroid, HashMap<String, Double> lm)
	{
		double dist = 0.0;
		for(String ss:centroid.keySet())
		{
			dist = dist + ((centroid.get(ss)-lm.get(ss))*(centroid.get(ss)-lm.get(ss)));
		}
		return Math.sqrt(dist);
	}
	
	/**
	 * for a cluster s, calculates the intra-cluster distance
	 * @param s
	 * @return
	 */
	
	public static double calculateIntraClusterDistance(Set<LanguageModelEntity> s)
	{
		HashMap<String, Double> centroid = calculateCentroid(s);
		double totalDist = 0.0;
		for(LanguageModelEntity l:s)
		{
			double dist = euclideanDistance(centroid, l.mixture);
			totalDist = totalDist + (dist*dist);
		}
		return totalDist;
	}
	
	
	/**
	 * returns a hashmap of entity to id
	 * @param entityMap
	 * @return
	 * @throws Exception
	 */
	
	public static HashMap<String, String> createStringToIdMapping1(String entityMap) throws Exception
	{
		BufferedReader br3 = new BufferedReader(new FileReader(entityMap));
		String line1;
		HashMap<String, String> stringToId = new HashMap<String, String>();
		
		while((line1=br3.readLine())!=null)
		{
			StringTokenizer tok = new StringTokenizer(line1,"\t");
			String a2 = tok.nextToken();
			String b = tok.nextToken();
			//String c = tok.nextToken();
			stringToId.put(b, a2);
		}
		return stringToId;
	}
	
	/**
	 * 
	 * @param entityMap: entityMap is the path to the file containing the mapping of ids to entities. returns a hashmap of id to entity
	 * @return
	 * @throws Exception
	 */
	
	public static HashMap<String, String> createStringToIdMapping(String entityMap) throws Exception
	{
		AdjList a = new AdjList();
		BufferedReader br3 = new BufferedReader(new FileReader(entityMap));
		String line1;
		HashMap<String, String> stringToId = new HashMap<String, String>();
		
		while((line1=br3.readLine())!=null)
		{
			StringTokenizer tok = new StringTokenizer(line1,"\t");
			String a2 = a.stem(tok.nextToken());
			//String b = b1.stem(tok.nextToken());
			String c = tok.nextToken();
			stringToId.put(a2, c);
		}
		return stringToId;
		
	}
	
	

	
	

	
	
	public static void checkEntityOverlap() throws Exception
	{
		GetPropertyValues properties = new GetPropertyValues();
		HashMap<String, String> hm = properties.getPropValues();
		String file = hm.get("wikipedia-all-links-tsv");
		BufferedReader br1 = new BufferedReader(new FileReader(file));
		
		BufferedReader br2 = new BufferedReader(new FileReader(hm.get("input-concepts")));
		
		ReadSubgraph rr = new ReadSubgraph();
		AdjList a = rr.readFromFile(br1);
		System.out.println("read all links");
		String line;
		ArrayList<String> input = new ArrayList<String>();
		while((line=br2.readLine())!=null)
		{
			input.add(line.trim().toLowerCase().replace(" ", "_"));
		}
		
		AdjList aa = rr.readKB(new BufferedReader(new FileReader("/mnt/dell/teknowbase/Re-Extractions/versions/finalVersions/TeKnowbase.tsv")));
		BufferedWriter bw = new BufferedWriter(new FileWriter("/home/prajna/scores1"));
		for(String s:input)
		{
			ArrayList<Edge> ee = a.getAdjList().get(s);
			if(ee!=null)
			{
			System.out.println(ee.size());
			int c=0;
			for(Edge e:ee)
			{
				if(aa.getAdjList().get(e.getName())!=null) c=c+1;
			}
			bw.write(s+"\t"+ee.size()+"\t"+c+"\n");
			}
			else
			{
				bw.write(s+"\t0\t0\n");
			}
		}
		bw.close();
	}
	
	/**
	 * hierarchical clustering given a set of vectors
	 * @param s: set of vectors
	 * @param bw: File to write the clusters
	 * @throws Exception
	 */
	
	public static void clusterHierarchicalPhrases(Set<LanguageModelEntity> s, BufferedWriter bw) throws Exception
	{
		HierarchicalClusterer<LanguageModelEntity> clClusterer = new CompleteLinkClusterer<LanguageModelEntity>(COSINE_DISTANCE);
		Dendrogram<LanguageModelEntity> completeLinkDendrogram = clClusterer.hierarchicalCluster(s);
		HashMap<String, Double> grandCentroid = calculateCentroid(s);
		
		//System.out.println("set size is: "+completeLinkDendrogram.size());
		//if(bw!=null) bw.write("\n"+i+"\n\n");
		HashMap<Integer, Double> distancesForKs = new HashMap<Integer, Double>();
		for(int c=completeLinkDendrogram.size()-1;c>=2;c--)
		{
			Set<Set<LanguageModelEntity>> ss = completeLinkDendrogram.partitionK(c);		
			if(ss==null) return;
			double totalIntraClusterDistance=0.0;
			double ssb = 0.0;
			for(Set<LanguageModelEntity> s1:ss)
			{
				HashMap<String, Double> centroid = calculateCentroid(s1);
				double dist1 = euclideanDistance(grandCentroid, centroid);
				ssb = ssb + dist1;
				double intraClusterDistance = calculateIntraClusterDistance(s1);
				totalIntraClusterDistance = totalIntraClusterDistance + intraClusterDistance;
				for(LanguageModelEntity l:s1)
				{
					if(bw!=null) bw.write(l.entity+"\t");
				}
				if(bw!=null) bw.write("\n");
			}
			//System.out.println("number of clusters: "+c+", value: "+(ssb/totalIntraClusterDistance)*((completeLinkDendrogram.size()-c)/(c-1)));
			distancesForKs.put(c,(ssb/totalIntraClusterDistance)*((completeLinkDendrogram.size()-c)/(c-1)));
			if(bw!=null) bw.write("\n\n");
		}
	}
	

	
	/**
	 * 
	 * @param i
	 * @param changedName
	 * @param nClusters
	 * @param s
	 * @param bw
	 * @param newAdjList
	 * @param newAdjList1
	 * @param newAdjList2
	 * @throws Exception
	 */
	
	public static void clusterHierarchical(String i, String changedName, int nClusters, Set<LanguageModelEntity> s, BufferedWriter bw, HashMap<String, ArrayList<Edge>> newAdjList, HashMap<String, ArrayList<Edge>> newAdjList1, HashMap<String, ArrayList<Edge>> newAdjList2, HashMap<String, ArrayList<Edge>> allAspectsCombined, BufferedWriter bww) throws Exception
	{
		if(bww!=null) bww.write(i+" has entered clustering \n");
		//System.out.println(i);
		//if(i.equals("b-tree")) System.out.println("yeah, b-tree is present");
		HierarchicalClusterer<LanguageModelEntity> clClusterer = new CompleteLinkClusterer<LanguageModelEntity>(COSINE_DISTANCE);
		Dendrogram<LanguageModelEntity> completeLinkDendrogram = clClusterer.hierarchicalCluster(s);
		HashMap<String, Double> grandCentroid = calculateCentroid(s);
		
		//System.out.println("set size is: "+completeLinkDendrogram.size());
		//if(bw!=null) bw.write("\n"+i+"\n\n");
		HashMap<Integer, Double> distancesForKs = new HashMap<Integer, Double>();
		for(int c=completeLinkDendrogram.size()-1;c>=2;c--)
		{
			Set<Set<LanguageModelEntity>> ss = completeLinkDendrogram.partitionK(c);		
			if(ss==null) return;
			double totalIntraClusterDistance=0.0;
			double ssb = 0.0;
			for(Set<LanguageModelEntity> s1:ss)
			{
				HashMap<String, Double> centroid = calculateCentroid(s1);
				double dist1 = euclideanDistance(grandCentroid, centroid);
				ssb = ssb + dist1;
				double intraClusterDistance = calculateIntraClusterDistance(s1);
				totalIntraClusterDistance = totalIntraClusterDistance + intraClusterDistance;
				for(LanguageModelEntity l:s1)
				{
					if(bw!=null) bw.write(l.entity+"\t");
				}
				if(bw!=null) bw.write("\n");
			}
			//System.out.println("number of clusters: "+c+", value: "+(ssb/totalIntraClusterDistance)*((completeLinkDendrogram.size()-c)/(c-1)));
			distancesForKs.put(c,(ssb/totalIntraClusterDistance)*((completeLinkDendrogram.size()-c)/(c-1)));
			if(bw!=null) bw.write("\n\n");
		}
		Set<Entry<Integer, Double>> entries = distancesForKs.entrySet();
		List<Entry<Integer, Double>> listOfEntries1 = new ArrayList<Entry<Integer, Double>>(entries);
		Comparator<Entry<Integer, Double>> valueComparator = new Comparator<Entry<Integer, Double>>() 
		{
			 public int compare(Entry<Integer, Double> e1, Entry<Integer, Double> e2)
			 {
				 Double v1 = e1.getValue();
				 Double v2 = e2.getValue();
				 return v1.compareTo(v2);
			 }
		};
		Collections.sort(listOfEntries1, valueComparator);
		Set<Set<LanguageModelEntity>> ss;
		if(listOfEntries1.size()>1)
		{
			int optK = listOfEntries1.get(listOfEntries1.size()-1).getKey();
			//System.out.println("optK value is: "+optK);
			ss = completeLinkDendrogram.partitionK(optK);
			if(bw!=null)
				bw.write("The optimum cluster is this: \n\n");
		}
		else
		{
			//System.out.println("else part");
			ss = new HashSet<Set<LanguageModelEntity>>();
			ss.add(s);
			if(bw!=null)
				bw.write("The optimum cluster is this: \n\n");
		}
		Set<LanguageModelEntity> s2 = new HashSet<LanguageModelEntity>();
		Set<LanguageModelEntity> s5 = new HashSet<LanguageModelEntity>();
		for(Set<LanguageModelEntity> s1:ss)
		{
			for(LanguageModelEntity l:s1)
			{
				if(l.entity.equals(changedName))
				{
					//System.out.println(s1.size());
					s2 = s1;
					break;
				}
			}
			for(LanguageModelEntity l:s1)
			{
				if(bw!=null)
					bw.write(l.entity+"\t");
			}
			if(bw!=null) bw.write("\n");
			HashMap<String, Double> centroid = calculateCentroid(s1);
			double min=9999999;
			LanguageModelEntity ll1=new LanguageModelEntity();
			if(s1.size()==1) continue;
			for(LanguageModelEntity l:s1)
			{
				double dist1 = euclideanDistance(centroid, l.mixture);
				if(min>dist1) 
				{
					ll1.mixture=l.mixture;
					ll1.entity=l.entity;
					min=dist1;
				}
			}
			s5.add(ll1);
			
		}
		Comparator<Set<LanguageModelEntity>> sizeComparator = new Comparator<Set<LanguageModelEntity>>() 
		{
			 public int compare(Set<LanguageModelEntity> e1, Set<LanguageModelEntity> e2)
			 {
				 Integer v1 = e1.size();
				 Integer v2 = e2.size();
				 return v1.compareTo(v2);
			 }
		};
		List<Set<LanguageModelEntity>> ll = new ArrayList<Set<LanguageModelEntity>>(ss);
		Collections.sort(ll, sizeComparator);
		Set<LanguageModelEntity> s3 = new HashSet<LanguageModelEntity>();
		Set<LanguageModelEntity> s4 = new HashSet<LanguageModelEntity>();
		int c=0;
		for(int j=ll.size()-1;j>=0;j--)
		{
			if(c==2) break;
			boolean flag=false;
			for(LanguageModelEntity l1:ll.get(j))
			{
				if(l1.entity.equals(changedName))
				{
					flag=true;
					break;
				}
			}
			if(!flag) 
			{
				if(c==0)
					s3 = ll.get(j);
				else if(c==1)
					s4 = ll.get(j);
				c++;
			}
			else
			{
				flag=false;
			}
			
			
		}
		//if(bww!=null) bww.write("size of s5 for "+changedName+" is: "+s5.size()+"\n");
		ArrayList<Edge> ee = new ArrayList<Edge>();
		ArrayList<Edge> ee1 = new ArrayList<Edge>();
		ArrayList<Edge> ee2 = new ArrayList<Edge>();
		ArrayList<Edge> ee3 = new ArrayList<Edge>();
		for(LanguageModelEntity l:s2)
		{
			ee.add(new Edge(0,l.entity));
		}
		for(LanguageModelEntity l:s3)
		{
			ee1.add(new Edge(0,l.entity));
		}
		for(LanguageModelEntity l:s4)
		{
			ee2.add(new Edge(0,l.entity));
		}
		for(LanguageModelEntity l:s5)
		{
			ee3.add(new Edge(0,l.entity));
			//if(bww!=null) bww.write(changedName+"\t"+l.entity+"\n");
		}
		//System.out.println(ee.size());
		if (newAdjList!=null) newAdjList.put(changedName, ee);
		if (newAdjList1!=null) newAdjList1.put(changedName, ee1);
		if (newAdjList2!=null) newAdjList2.put(changedName, ee2);
		if (allAspectsCombined!=null) allAspectsCombined.put(changedName, ee3);
		if(bw!=null) bw.close();
		
	}
	
	
	/**
	 * old function for generating language models
	 * @throws Exception
	 */
	
	
	
	/**
	 * iterates over the prerequisite graph of the input concepts and prints those which contain less than 20 nodes
	 * @param inputConcepts
	 * @throws Exception
	 */
	
	public void checkPrerequisiteGraphForSize(ArrayList<String> inputConcepts, String tableName) throws Exception
	{
		int c1=0;
		for(String input:inputConcepts)
		{
			File f1 = new File("/mnt/dell/prajna/refdScores/conceptWiseScores/"+input);
			if(!f1.exists()) continue;
			else
			{
				
				//System.out.println(input);
			}
			File f2 = new File("/mnt/dell/prajna/refdScores/conceptWiseScores/"+input+"/prerequisiteGraph"+tableName);
			if(!f2.exists())
			{
				f2 = new File("/mnt/dell/prajna/refdScores/conceptWiseScores/"+input+"/prerequsiteGraph"+tableName);
			}
			if(!f2.exists())
			{
				f2 = new File("/mnt/dell/prajna/refdScores/lowRecall/"+input+"/prerequisiteGraphrefd"+tableName);

			}
			
			if(f2.exists())
			{
				c1++;
				//System.out.println(input);
				BufferedReader br = new BufferedReader(new FileReader(f2.getAbsolutePath()));
				String line;
				int c=0;
				HashSet<String> nodes = new HashSet<String>();
				while((line=br.readLine())!=null)
				{
					StringTokenizer tok = new StringTokenizer(line);
					ArrayList<String> tokens = new ArrayList<String>();
					while(tok.hasMoreTokens())
					{
						tokens.add(tok.nextToken());
					}
					if(tokens.size()!=2) continue;
					nodes.add(tokens.get(0));
					nodes.add(tokens.get(1));
				}
				if(nodes.size()==19)
				{
					System.out.println(input+"\t"+nodes.size()+"\t"+f2.getAbsolutePath());
				}
			}
			
		}
		System.out.println(c1);
	}
	
	/**
	 * select those concepts that return some non-empty prerequisite graph for both the primary and the aspect1 frame
	 * @param folder
	 * @throws Exception
	 */
	
	public static void selectElements(String folder) throws Exception
	{
		GetPropertyValues properties = new GetPropertyValues();
		HashMap<String, String> hm = properties.getPropValues();
		BufferedReader br = new BufferedReader(new FileReader(hm.get("input-concepts")));
		//BufferedWriter bw1 = new BufferedWriter(new FileWriter("/mnt/dell/prajna/refdScores/lowRecallOriginalRefD"));
		String line;
		ArrayList<String> inputConcepts = new ArrayList<String>();
		while((line=br.readLine())!=null)
		{
			inputConcepts.add(line);
		}
		for(String i:inputConcepts)
		{
			File f1 = new File("/mnt/dell/prajna/refdScores/"+folder+"/"+i+"/prerequsiteGraphrefdclusteredcombinedframesonlyaspect1");
			File f2 = new File("/mnt/dell/prajna/refdScores/"+folder+"/"+i+"/prerequsiteGraphrefdclusteredcombinedframesonlyaspect2");

			if(!f1.exists() || !f2.exists()) continue;
			BufferedReader br1 = new BufferedReader(new FileReader("/mnt/dell/prajna/refdScores/"+folder+"/"+i+"/prerequsiteGraphrefdclusteredcombinedframesonlyaspect1"));
			BufferedReader br2 = new BufferedReader(new FileReader("/mnt/dell/prajna/refdScores/"+folder+"/"+i+"/prerequsiteGraphrefdclusteredcombinedframesonlyaspect2"));
			BufferedReader br3 = new BufferedReader(new FileReader("/mnt/dell/prajna/refdScores/"+folder+"/"+i+"/prerequsiteGraphrefdclusteredcombinedframesonly"));

			HashSet<String> concepts1 = new HashSet<String>();
			String line1;
			while((line1=br1.readLine())!=null)
			{
				StringTokenizer tok = new StringTokenizer(line1,"\t");
				ArrayList<String> tokens = new ArrayList<String>();
				while(tok.hasMoreTokens())
				{
					tokens.add(tok.nextToken());
				}
				if(tokens.size()!=2) continue;
				concepts1.addAll(tokens);
			}
			
			HashSet<String> concepts2 = new HashSet<String>();
			while((line1=br2.readLine())!=null)
			{
				StringTokenizer tok = new StringTokenizer(line1,"\t");
				ArrayList<String> tokens = new ArrayList<String>();
				while(tok.hasMoreTokens())
				{
					tokens.add(tok.nextToken());
				}
				if(tokens.size()!=2) continue;
				concepts2.addAll(tokens);
			}
			
			HashSet<String> concepts3 = new HashSet<String>();
			while((line1=br3.readLine())!=null)
			{
				StringTokenizer tok = new StringTokenizer(line1,"\t");
				ArrayList<String> tokens = new ArrayList<String>();
				while(tok.hasMoreTokens())
				{
					tokens.add(tok.nextToken());
				}
				if(tokens.size()!=2) continue;
				concepts3.addAll(tokens);
			}
			if(concepts1.size()>0 && concepts3.size()>0) 
			{
				System.out.println(i);
				//bw1.write(i+"\n");
			}
			
		}
		//bw1.close();
		/*ArrayList<String> elements = new ArrayList<String>();
		for(String s:a.getAdjList().keySet())
		{
			HashMap<String, String> h = new HashMap<String, String>();
			ArrayList<Edge> e = a.getAdjList().get(s);
			for(Edge ee:e)
			{
				h.put(ee.getRelationaName(), "");
			}
			if(h.get("typeOf")!=null && h.get("computing_and_it_abbreviations")==null)
			{
				elements.add(s);
			}
		}
		Random randomizer = new Random();
		System.out.println(randomizer.nextInt(4));
		int c=0;
		int l = elements.size();
		System.out.println("elements size: "+elements.size());
		//elements.get(randomizer.nextInt(elements.size()));
		while(c<100)
		{
			c++;
			int r = randomizer.nextInt(l);
			String random = elements.get(r);
			System.out.println(random);
		}*/
	
	}
	
	/**
	 * reads a file containing pairs of nodes and adds a relation "subcategoryOf" between them
	 * @param file
	 * @throws Exception
	 */
	
	public static void addRelation(String file) throws Exception
	{
		BufferedReader br = new BufferedReader(new FileReader(file));
		BufferedWriter bw1 = new BufferedWriter(new FileWriter(file+"_relation_added"));
		String line;
		while((line=br.readLine())!=null)
		{
			StringTokenizer tok = new StringTokenizer(line," ");
			String a = tok.nextToken();
			String b = tok.nextToken();
			bw1.write(a+"\tsubcategoryOf\t"+b+"\n");
		}
		bw1.close();
	}
	
	/**
	 * gathers all concepts and writes them to a file
	 * @throws Exception
	 */
	
	public static void gatherAllConcepts() throws Exception
	{
		File f1 = new File("/mnt/dell/prajna/refdScores/conceptWiseScores");
		File f2 = new File("/mnt/dell/prajna/refdScores/lm");
		File f3 = new File("/mnt/dell/prajna/refdScores/lowRecall");
		BufferedWriter bw1 = new BufferedWriter(new FileWriter("/mnt/dell/prajna/refdScores/allPossibleConcepts"));
		HashSet<String> h = new HashSet<String>();
		for(File f:f1.listFiles())
		{
			if(f.isDirectory())
			{
				h.add(f.getName());
			}
		}
		for(File f:f2.listFiles())
		{
			if(f.isDirectory())
			{
				h.add(f.getName());
			}
		}
		for(File f:f3.listFiles())
		{
			if(f.isDirectory())
			{
				h.add(f.getName());
			}
		}
		for(String h1:h)
		{
			bw1.write(h1+"\n");
		}
		bw1.close();
	}
	
	/**
	 * reads file and returns a hashmap of most similiar entities to it
	 * @param file
	 * @return
	 */
	
	public static HashMap<String, ArrayList<String>> readSimiliarEntities(String file) throws Exception
	{
		BufferedReader br = new BufferedReader(new FileReader(file));
		String line;
		HashMap<String, ArrayList<String>> h = new HashMap<String, ArrayList<String>>();
		AdjList b = new AdjList();
		while((line=br.readLine())!=null)
		{
			StringTokenizer tok = new StringTokenizer(line,"\t");
			ArrayList<String> tokens = new ArrayList<String>();
			while(tok.hasMoreTokens())
			{
				tokens.add(b.stem(tok.nextToken()));
			}
			String input = tokens.get(0);
			tokens.remove(0);
			h.put(input, tokens);
		}
		return h;
	}
	
	
	
	public static void convertSetToLM(String query, ArrayList<String> neigh, HashMap<String, LanguageModelEntity> s, String outfile) throws Exception
	{
		//BufferedReader br = new BufferedReader(new FileReader(file));
		BufferedWriter bw1 = new BufferedWriter(new BufferedWriter(new FileWriter(outfile)));
		String line;
		/*ArrayList<String> neigh = new ArrayList<String>();
		while((line=br.readLine())!=null)
		{
			neigh.add(line);
		}*/
		AdjList a = new AdjList();
		
		Set<LanguageModelEntity> s1 = new HashSet<LanguageModelEntity>();
		/*for(Edge e:ee1)
		{
			if(e.getRelationaName().equals("typeOf") || e.getRelationaName().equals("typeof"))
			{
				ee.add(e);
			}
		}*/
		AdjList b = new AdjList();
		for(String e:neigh)
		{
			//entity = e.getName().replace("-", "_");
			String entity = e;
			if(s.get(entity)==null) 
			{
				if(s.get(b.removeDisambiguation(entity))==null)
				{
					if(s.get(b.stem(b.removeDisambiguation(entity)))==null)
					{
						System.out.println(entity+" embedding is null");
						continue;
					}
					else
					{
						System.out.println(entity+" embedding is not null");
						s1.add(s.get(b.stem(b.removeDisambiguation(entity))));
					}
				}
				else
				{
					System.out.println(entity+" embedding is not null");
					s1.add(s.get(b.removeDisambiguation(entity)));
				}
				
			}
			else 
			{
				s1.add(s.get(entity));
			}
		}
		System.out.println(s1.size()-1);
		clusterHierarchical(query, query, s1.size()-1, s1, bw1, null, null, null, null, null);
		bw1.close();

	}
	
	
	
	
	public static void getWikiNeigh(String wiki, String query, String outfile) throws Exception
	{
		BufferedWriter bw = new BufferedWriter(new FileWriter(outfile));
		ReadSubgraph rr = new ReadSubgraph();
		AdjList a = rr.readFromFile(new BufferedReader(new FileReader(wiki)));
		ArrayList<Edge> n1 = a.getAdjList().get(query);
		System.out.println("size of neighbors of "+query+" is: "+n1.size());
		
		ArrayList<String> neighbours1 = a.getListInString(n1);
		System.out.println("size of neighbors of "+query+" is: "+n1.size());

		/*************** line added to experiment *****/
		neighbours1.add(query);
		/*************** line added to experiment *****/
		for(String neighbour:neighbours1)
		{
			bw.write(neighbour+"\n");
		}
		bw.close();
	}
	
	
	/**
	 * returns k-most similar entities from among the Wikipedia neighbors
	 * @param entityMap
	 * @param embedding
	 * @param inputfile
	 * @param k
	 * @param wiki
	 */
	
	public static void returnKMostSimilarWikipedia(String entityMap, String embedding, String inputfile, int k, String wiki, String sim) throws Exception
	{
		ReadSubgraph rr = new ReadSubgraph();
		AdjList a = rr.readFromFile(new BufferedReader(new FileReader(wiki)));
		HashMap<String, String> stringToId = createStringToIdMapping(entityMap);
		HashMap<String, LanguageModelEntity> emb = readEmbeddingsFromFile(embedding,stringToId,a);
		ArrayList<String> input = new ArrayList<String>();
		BufferedReader br = new BufferedReader(new FileReader(inputfile));
		BufferedWriter bw = new BufferedWriter(new FileWriter(sim));
		String line;
		while((line=br.readLine())!=null)
		{
			input.add(line);
		}
		for(String i:input)
		{
			bw.write("\n"+i+"\n");
			ArrayList<Edge> ee = a.getAdjList().get(i);
			LanguageModelEntity l1 = emb.get(i);
			if(l1==null) continue;
			if(ee==null) continue;
			HashMap<String, Double> h = new HashMap<String, Double>();
			for(Edge e:ee)
			{
				LanguageModelEntity l = emb.get(e.getName());
				if(l==null) continue;
				Double d = l1.cosineSimilarity(l);
				h.put(e.getName(), d);
			}
			ArrayList<Entry<String, Double>> distances = new ArrayList<Entry<String, Double>>(h.entrySet());
			Collections.sort(distances, valueComparator);
			int count=0;
			for(Entry<String, Double> s:distances)
			{
				count = count + 1;
				//if(count>k) break; 
				bw.write(s.getKey()+"\t"+s.getValue()+"\n");
			}
		}
		bw.close();
	}
	
	/**
	 * Returns the k most similar entities to a given entity
	 * @param entityMap
	 * @param embedding
	 * @param inputfile
	 * @param k
	 * @throws Exception
	 */
	
	public static void returnKMostSimilar(String entityMap, String embedding, String inputfile, int k, String outfile) throws Exception
	{
		HashMap<String, String> stringToId = createStringToIdMapping(entityMap);
		HashMap<String, LanguageModelEntity> emb = readEmbeddingsFromFile(embedding,stringToId,new AdjList());
		ArrayList<String> input = new ArrayList<String>();
		BufferedReader br = new BufferedReader(new FileReader(inputfile));
		BufferedWriter bw = new BufferedWriter(new FileWriter(outfile));
		String line;
		while((line=br.readLine())!=null)
		{
			input.add(line);
		}
		for(String i:input)
		{
			System.out.println(i);
			HashMap<String, Double> distance = new HashMap<String, Double>();
			String inp = extractEmbedding(emb, i);
			System.out.println(inp);
			bw.write("\n"+inp+"\n");
			LanguageModelEntity l1 = emb.get(inp);
			System.out.println(emb.size());
			for(String e:emb.keySet())
			{
				//System.out.println(e);
				if(e==null) continue;
				if(e.equals(inp)) continue;
				LanguageModelEntity l = emb.get(e);
				Double d = l1.cosineSimilarity(l);
				distance.put(e,d);
			}
			ArrayList<Entry<String, Double>> distances = new ArrayList<Entry<String, Double>>(distance.entrySet());
			Collections.sort(distances, valueComparator);
			int count=0;
			for(Entry<String, Double> s:distances)
			{
				count = count + 1;
				//if(count>k) break; 
				bw.write(s.getKey()+"\t"+s.getValue()+"\n");
			}
		}
		bw.close();
		
	}
	
	public static Double calculateEntropy(ArrayList<Double> dd)
	{
		double sum=0.0;
		for(Double d:dd)
		{
			sum = sum + d;
		}
		Double entropy = 0.0;
		for(Double d1:dd)
		{
			if(d1>0.0)
				entropy = entropy - (d1/sum)*Math.log(d1/sum);
		}
		return entropy;
		
	}
	
	/**
	 * given 2 embedding spaces, this function assigns labels like "application", "algorithm" or "implementation".
	 * @param entityMap: map of all the entities to numbers
	 * @param embedding1: embedding space for algorithm
	 * @param embedding2: embedding space for implementation
	 * @param embedding3: embedding space for application
	 * @param inputfile: set of concepts
	 * @param outputfile: file where the labels will be written
	 * @param wiki: wikipedia file, can be any other graph to read the neighbors
	 * @throws Exception
	 */
	
	public static void assignLabelsBasedOnEntropy(String entityMap, String embedding1, String embedding2, String embedding3, String inputfile, String outputfile, String wiki) throws Exception
	{
		ReadSubgraph rr = new ReadSubgraph();
		AdjList a = rr.readFromFile(new BufferedReader(new FileReader(wiki)));
		HashMap<String, String> stringToId = createStringToIdMapping(entityMap);
		HashMap<String, LanguageModelEntity> emb1 = readEmbeddingsFromFile(embedding1,stringToId,a);
		HashMap<String, LanguageModelEntity> emb2 = readEmbeddingsFromFile(embedding2,stringToId,a);
		HashMap<String, LanguageModelEntity> emb3 = readEmbeddingsFromFile(embedding3,stringToId,a);
		ArrayList<String> input = new ArrayList<String>();
		BufferedWriter bw = new BufferedWriter(new FileWriter(outputfile));

		BufferedReader br = new BufferedReader(new FileReader(inputfile));
		String line;
		while((line=br.readLine())!=null)
		{
			input.add(line);
		}
		for(String i:input)
		{
			System.out.println(i);
			HashMap<String, Double> entropyList = new HashMap<String, Double>();
			HashMap<String, String> labels = new HashMap<String, String>();
			String inp1 = extractEmbedding(emb1, i);
			String inp2 = extractEmbedding(emb2, i);
			String inp3 = extractEmbedding(emb3, i);
			bw.write("\n"+i+"\n");
			LanguageModelEntity l1 = emb1.get(inp1);
			LanguageModelEntity l2 = emb2.get(inp2);
			LanguageModelEntity l3 = emb3.get(inp3);
			
			
			
			ArrayList<Edge> ee = a.getAdjList().get(i);
			
			if(l1==null || l2==null || l3==null) continue;
			if(ee==null) continue;
			
			for(Edge e:ee)
			{
				Double d1, d2, d3;
				LanguageModelEntity l = emb1.get(e.getName());
			//	System.out.println(l);
				if(l==null) d1=0.0;
				else d1 = l1.cosineSimilarity(l);
				//distance1.put(e.getName(), l1.cosineSimilarity(l));
				l = emb2.get(e.getName());
			//	System.out.println(l);
				if(l==null) d2=0.0;
				else d2 = l2.cosineSimilarity(l);
				//distance2.put(e.getName(), l2.cosineSimilarity(l));
				l = emb3.get(e.getName());
			//	System.out.println(l);
				if(l==null) d3=0.0;
				else d3 = l3.cosineSimilarity(l);
				
				
				if(d1==0.0 && d2==0.0 && d3==0.0)
				{
					entropyList.put(e.getName(), 9999999.0);
					labels.put(e.getName(),"cant_decide");
					continue;
				}
				
				//distance3.put(e.getName(), l3.cosineSimilarity(l));
				ArrayList<Double> dd = new ArrayList<Double>();
				dd.add(d1);
				dd.add(d2);
				dd.add(d3);
				if(d1>d2)
				{
					if(d1>d3)
					{
						labels.put(e.getName(), "algorithm");
					}
					else
					{
						labels.put(e.getName(), "application");
					}
				}
				else
				{
					if(d2>d3)
					{
						labels.put(e.getName(), "implementation");
					}
					else
					{
						labels.put(e.getName(), "application");
					}
				}
				Double entropy = calculateEntropy(dd);
				System.out.println(e.getName()+"\t"+d1+"\t"+d2+"\t"+d3+"\t"+entropy+"\n");
				entropyList.put(e.getName(), entropy);
			}
			
			ArrayList<Entry<String, Double>> entropyLists = new ArrayList<Entry<String, Double>>(entropyList.entrySet());
			Collections.sort(entropyLists, valueComparator);
			
			for(Entry<String, Double> s:entropyLists)
			{
				
				//if(count>k) break; 
				bw.write(s.getKey()+"\t"+labels.get(s.getKey())+"\t"+s.getValue()+"\n");
			}
			
		}
		bw.close();
		
	}
	
	public static void computeRefDForCNN(String wiki, String query1, String query2, String outfile1, String outfile2, double thresh) throws Exception
	{
		ReadSubgraph rr = new ReadSubgraph();
		DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
	    Date  date = new Date();
	    System.out.println("started "+dateFormat.format(date));
	    
		AdjList a = rr.readFromFile(new BufferedReader(new FileReader(wiki)));
		
		date = new Date();
		System.out.println("read wikipedia dump "+dateFormat.format(date));
		
		ArrayList<String> neighbours1 = a.getListInString(a.getAdjList().get(query1));
		HashSet<String> hh = new HashSet<String>();
		hh.addAll(neighbours1);
		
		for(String s:neighbours1)
		{
			ArrayList<String> neighbours11 = a.getListInString(a.getAdjList().get(s));
			hh.addAll(neighbours11);
		}
		HashMap<String, Double> prereq_score1 = new HashMap<String, Double>();
		for(String h:hh)
		{
			double refd = a.referenceDistance(query1, h);
			prereq_score1.put(h, refd);
		}
		 date = new Date();
		 System.out.println("computed refd scores for 1st query "+dateFormat.format(date));
		
		// convertSetToLM(hm.get("query"), hm.get("neighbors"),hm.get("emb"),hm.get("mappings"), hm.get("outfile"));
		 
		ArrayList<Entry<String, Double>> entryset1 = new ArrayList<Entry<String, Double>>(prereq_score1.entrySet());
		Collections.sort(entryset1, valueComparator);
		
		
		ArrayList<String> neighbours2 = a.getListInString(a.getAdjList().get(query2));
		HashSet<String> hh1 = new HashSet<String>();
		hh1.addAll(neighbours2);
		
		for(String s:neighbours2)
		{
			ArrayList<String> neighbours22 = a.getListInString(a.getAdjList().get(s));
			hh1.addAll(neighbours22);
		}
		HashMap<String, Double> prereq_score2 = new HashMap<String, Double>();
		for(String h:hh1)
		{
			double refd = a.referenceDistance(query2, h);
			prereq_score2.put(h, refd);
		}
		date = new Date();
		 System.out.println("computed refd scores for 2nd query "+dateFormat.format(date));
		
		ArrayList<Entry<String, Double>> entryset2 = new ArrayList<Entry<String, Double>>(prereq_score2.entrySet());
		Collections.sort(entryset2, valueComparator);
		
		BufferedWriter bww1 = new BufferedWriter(new FileWriter(outfile1+"_graph"));
		BufferedWriter bww2 = new BufferedWriter(new FileWriter(outfile2+"_graph"));
		
		HashSet<String> preqs1 = new HashSet<String>();
		for(Entry<String, Double> e :entryset1)
		{
			if(e.getValue()>=thresh)
				preqs1.add(e.getKey());
		}
		
		
		HashSet<String> preqs2 = new HashSet<String>();
		for(Entry<String, Double> e :entryset2)
		{
			if(e.getValue()>=thresh)
				preqs2.add(e.getKey());
		}
		
		BufferedWriter bw1 = new BufferedWriter(new FileWriter(outfile1));
		BufferedWriter bw2 = new BufferedWriter(new FileWriter(outfile2));
		
		for(Entry<String, Double> e:entryset1)
		{
			bw1.write(e.getKey()+"\t"+e.getValue()+"\n");
			if(e.getValue()>=thresh) bww1.write(query1+"\t"+e.getKey()+"\n");
		}
		
		for(Entry<String, Double> e:entryset2)
		{
			bw2.write(e.getKey()+"\t"+e.getValue()+"\n");
			if(e.getValue()>=thresh) bww2.write(query2+"\t"+e.getKey()+"\n");
		}
		for(String s:preqs1)
		{
			for(String s1:preqs1)
			{
				double refd = a.referenceDistance(s, s1);
				if(refd>=thresh)
				{
					bww1.write(s+"\t"+s1+"\n");
				}
				/*else if (refd<=-0.07)
				{
					bww1.write(s1+"\t"+s+"\n");
				}*/
			}
		}
		
		for(String s:preqs2)
		{
			for(String s1:preqs2)
			{
				double refd = a.referenceDistance(s, s1);
				if(refd>=thresh)
				{
					bww2.write(s+"\t"+s1+"\n");
				}
				/*else if (refd<=-0.07)
				{
					bww2.write(s1+"\t"+s+"\n");
				}*/
			}
		}
		
		bw1.close();
		bw2.close();
		bww1.close();
		bww2.close();

	}
	
	public static void main(String args[]) throws Exception
	{
		GetPropertyValues properties = new GetPropertyValues();
		HashMap<String, String> hm = properties.getPropValues();
		
		getWikiNeigh(hm.get("wikipedia"),hm.get("query"),hm.get("neighbors"));
		returnKMostSimilar(hm.get("mappings"), hm.get("emb"), hm.get("queries"), Integer.parseInt(hm.get("k")), hm.get("outfile"));
		//computeRefDForCNN(hm.get("wikipedia"),hm.get("query1"),hm.get("query2"),hm.get("outfile1"),hm.get("outfile2"), Double.parseDouble(hm.get("thresh")));
		
		//returnKMostSimilarWikipedia(hm.get("embeddingEntities"), hm.get("embedding"), hm.get("input-concepts"), 10, hm.get("wikipedia-all-links-tsv"), hm.get("output-file1"));
		//assignLabelsBasedOnEntropy(hm.get("embeddingEntities"), hm.get("embedding-algorithm"), hm.get("embedding-implementation"), hm.get("embedding-application"), hm.get("input-concepts"), hm.get("output-file1"), hm.get("wikipedia-all-links-tsv"));
		//returnKMostSimilar(hm.get("embeddingEntities"), hm.get("embedding"), hm.get("input-concepts"), 10);
		//generateClusters(hm.get("latest-category-graph"), hm.get("entity-name"), hm.get("wikipedia-all-links-tsv"), hm.get("embeddingEntities"), hm.get("embedding"), hm.get("output-file"), hm.get("input-concepts"));
		
	//	computeKMostSimiliarEntity(hm.get("input-concepts"),hm.get("embedding"),hm.get("embeddingEntities"),hm.get("suffix"),hm.get("latest-category-graph"));
	//	retrieveCandidates(hm.get("most-similiar-sibling-latest-non-unique"),hm.get("output-file"),hm.get("table-name"));
		/*String file = hm.get("node2vec-frames-weighted");
		ReadSubgraph rr = new ReadSubgraph();
		BufferedReader br1 = new BufferedReader(new FileReader(file));
		AdjList a1 = rr.readFromFile(br1);
		br1.close();
		
		HashMap<String, ArrayList<Edge>> hm1 = new HashMap<String, ArrayList<Edge>>();
		for(String s:a1.getAdjList().keySet())
		{
			ArrayList<Edge> e = a1.getAdjList().get(s);
			HashSet<String> h = new HashSet<String>();
			for(Edge ee:e)
			{
				h.add(ee.getName());
			}
			if(h.contains(s)) h.remove(s);
			ArrayList<Edge> e1 = new ArrayList<Edge>();
			for(String hh:h)
			{
				e1.add(new Edge(1,hh));
			}
			hm1.put(s, e1);
		}
		AdjList a2 = new AdjList(hm1);
		findSimiliarEntitiesFromAll(hm.get("latest-category-graph"), hm.get("latest-category-graph.entities"), hm.get("latest-category-graph.emd"), hm.get("word2vec-stemmed.entities"), hm.get("word2vec-stemmed.emd"), hm.get("all-possible-concepts"), a2, a1);
		computeKMostSimiliarEntity(hm.get("all-possible-concepts"),hm.get("latest-category-graph.emd"),hm.get("latest-category-graph.entities"),"latestNonUnique");
		computeKMostSimiliarEntity(hm.get("all-possible-concepts"),hm.get("ijcnlp-weighted.emd"),hm.get("ijcnlp.entities"),"weightedNonUnique");
		computeKMostSimiliarEntity(hm.get("all-possible-concepts"),hm.get("ijcnlp-directed.emd"),hm.get("ijcnlp.entities"),"directedNonUnique");
		computeKMostSimiliarEntity(hm.get("all-possible-concepts"),hm.get("ijcnlp.emd"),hm.get("ijcnlp.entities"),"ijcnlpNonUnique");
		computeKMostSimiliarEntity(hm.get("all-possible-concepts"),hm.get("word2vec-stemmed.emd"),hm.get("word2vec-stemmed.entities"),"word2vec");

	//selectElements("lowRecall");
		//improveRecall(a2, hm.get("closest-entity"), "refdclusterednode2vecframesonlyweighted", a1);
		/*File f = new File("/mnt/dell/prajna/refdScores/lowRecall");
		for(File f1:f.listFiles())
		{
			if(f1.isDirectory())
			{
				File f2 = new File("/mnt/dell/prajna/refdScores/lowRecall/"+f1.getName()+"/prerequsiteGraphrefdclusterednode2vecframesonlyweightedRecallImprovedTypeOf");
				if(f2.exists())
				{
					System.out.println(f1.getName());
				}
			}
		}*/
		/*ReadSubgraph rr = new ReadSubgraph();
		BufferedReader br = new BufferedReader(new FileReader(hm.get("closest-entity")));
		String line1;
		HashMap<String, String> closestEntity = new HashMap<String, String>();
		while((line1=br.readLine())!=null)
		{
			StringTokenizer tok = new StringTokenizer(line1,"\t");
			closestEntity.put(tok.nextToken(), tok.nextToken());
		}
		AdjList kb1 = rr.readKB(new BufferedReader(new FileReader(hm.get("latest-category-graph"))));
		BufferedWriter bw = new BufferedWriter(new FileWriter("/mnt/dell/prajna/refdScores/conceptWithTypeOfs"));
		for(String s:closestEntity.keySet())
		{
			ArrayList<Edge> e = kb1.getAdjList().get(s);
			if(e!=null)
			{
				for(Edge ee:e)
				{
					if(ee.getRelationaName().equals("typeof"))
					{
						bw.write(s+"\t"+ee.getName()+"\n");
					}
				}
			}
		}
		bw.close();
		ReadSubgraph rr = new ReadSubgraph();
		AdjList aa = rr.readFromFile(new BufferedReader(new FileReader("/mnt/dell/prajna/refdScores/lowRecall/symmetric_multiprocessing/prerequsiteGraphrefdclusterednode2vecframesonlyweighted")));
		AdjList aa1 = rr.readFromFile(new BufferedReader(new FileReader("/mnt/dell/prajna/refdScores/lowRecall/asymmetric_multiprocessing/prerequsiteGraphrefdclusterednode2vecframesonlyweighted")));
		AdjList aa2 = aa1.makeCopy();
		
		for(String ss:aa1.getAdjList().keySet())
		{
			if(ss.equals("asymmetric_multiprocessing"))
			{
				ArrayList<Edge> ee = aa1.getAdjList().get(ss);
				aa2.getAdjList().put("symmetric_multiprocessing", ee);
				aa2.getAdjList().remove("asymmetric_multiprocessing");
			}
			else if(ss.equals("symmetric_multiprocessing")) continue;
			else
			{
				aa2.getAdjList().put(ss, aa1.getAdjList().get(ss));
			}
		
		}
		BufferedWriter bw = new BufferedWriter(new FileWriter("/mnt/dell/prajna/refdScores/lowRecall/symmetric_multiprocessing/replaced"));
		aa2.writeToFile(bw);
		bw.close();*/
		//combineKBAndTextEmbeddings(hm.get("latest-category-graph"), hm.get("latest-category-graph.entities"), hm.get("latest-category-graph.emd"), hm.get("word2vec-stemmed.entities"), hm.get("word2vec-stemmed.emd"),hm.get("operator"),hm.get("combined-embeddings"));
		//computeAllAspectFramesForAll(hm.get("latest-category-graph"), hm.get("embeddingEntities"), hm.get("embedding"), hm.get("wikipedia-all-links-tsv"), hm.get("prefix-to-store-clusters"), hm.get("theory-frame"),hm.get("alternate-frame1"),hm.get("alternate-frame2"),hm.get("all-aspects"),"",hm.get("dir-to-store-clusters"),hm.get("dir-to-store-frames"));
		//computeAlternateAspectFramesForAll(hm.get("ijcnlp"), hm.get("ijcnlp.entities"), hm.get("ijcnlp-weighted.emd"), hm.get("wikipedia-all-links-tsv"), "bottomUpNode2VecWeighted", "newFramesClusteringWeighted");
		//selectElements("lowRecall");
		//readDBPediaTriples("","");
		//LanguageModelEntity e = new LanguageModelEntity();
		//e.findSimilarEntities();
		/*ReadSubgraph rr = new ReadSubgraph();
		AdjList a = rr.readKB(new BufferedReader(new FileReader("/mnt/dell/teknowbase/Re-Extractions/versions/finalVersions/TeKnowbase.tsv")));
		BufferedReader br1 = new BufferedReader(new FileReader("/mnt/dell/prajna/data/freebase/freebase-easy-latest/facts.txt"));
		BufferedWriter bw = new BufferedWriter(new FileWriter("/mnt/dell/prajna/data/dbpedia/allDbpediaFiles/commonRelationsFreebase"));
		String line;
		HashMap<String, String> h = new HashMap<String, String>();
		while((line=br1.readLine())!=null)
		{
			//if(!line.contains(",")) continue;
			StringTokenizer tok = new StringTokenizer(line,"\t");
			ArrayList<String> tokens = new ArrayList<String>();
			while(tok.hasMoreTokens())
			{
				tokens.add(tok.nextToken());
			}
		//	System.out.println(tokens.size());
			if(tokens.size()!=4) 
			{
				System.out.println("fail");
				continue;
			}
			/*int endIndex = tokens.get(0).lastIndexOf("/");
		    if (endIndex != -1)  
		    {
		        String newstr = tokens.get(0).substring(endIndex+1, tokens.get(0).length()).replace("\" ","").toLowerCase(); // not forgot to put check if(endIndex != -1)
		        h.put(newstr,"");
		        //System.out.println(newstr);
		        
		    }
			h.put(tokens.get(1), "");
		    
			
		}
		HashSet<String> commonRelations = new HashSet<String>();
		for(String s:a.getAdjList().keySet())
		{
			ArrayList<Edge> e = a.getAdjList().get(s);
			for(Edge ee:e)
			{
				if(h.get(ee.getRelationaName())!=null)
				{
					commonRelations.add(ee.getRelationaName());
				}
			}
		}
		for(String c:commonRelations)
		{
			bw.write(c+"\n");
		}
		bw.close();
		
	//	a.removeTriples("sameAs");
	//	a.setRelTypes(a.getRelTypes());
		/*LanguageModelEntity.a = a;
		LanguageModelEntity.initilizeUnigramAndBigram();
		HashMap<String, HashMap<String, ArrayList<String>>> hh = new HashMap<String, HashMap<String, ArrayList<String>>>();
		for(String aa:a.getRelTypes().keySet())
		{
			HashMap<String, ArrayList<String>> newhh = new HashMap<String, ArrayList<String>>();
			for(String bb:a.getRelTypes().get(aa).keySet())
			{
				ArrayList<String> cc = a.getRelTypes().get(aa).get(bb);
				ArrayList<String> dd = new ArrayList<String>(cc);
				newhh.put(bb, dd);
			}
			hh.put(aa, newhh);
		}
		
		
		//System.out.println(a.getRelTypes().get("computer_science").size());
		AdjList newAdjList = a.constructDocument("computer_science");
		a.setRelTypes(hh);
		//System.out.println(a.getAdjList().size());
		//System.out.println(a.getRelTypes().size());
		//System.out.println(a.getRelTypes().get("computer_science").size());*/
		/*AdjList a = rr.readKB(new BufferedReader(new FileReader("/mnt/dell/teknowbase/Re-Extractions/versions/finalVersions/reExtractionsUniqueCanonicalisedPostProcessedOnlyPageNamesBadSourcesRemovedBadCategoriesRemovedStemmedUnique.tsv")));
		a.removeTriples("sameAs");
		a.setRelTypes(a.getRelTypes());
		GetPropertyValues properties = new GetPropertyValues();
		HashMap<String, String> hm = properties.getPropValues();
		String file = hm.get("wikipedia-all-links-tsv");
		BufferedReader br1 = new BufferedReader(new FileReader(file));
		//BufferedReader br2 = new BufferedReader(new FileReader(hm.get("input-concepts")));
		AdjList a1 = rr.readFromFile(br1);
		System.out.println("read all links");
		HashSet<String> hh = new HashSet<String>();
		for(String aa:a.getAdjList().keySet())
		{
			hh.add(aa);
			if(a1.getAdjList().get(aa)!=null)
			{
				ArrayList<Edge> ee = a1.getAdjList().get(aa);
				for(Edge e:ee)
				{
					hh.add(e.getName());
				}
			}
		}
		BufferedWriter bw = new BufferedWriter(new FileWriter("/mnt/dell/teknowbase/entities/entitiesAndTheir1hopNeighbours"));
		for(String aa:hh)
		{
			bw.write(aa+"\n");
		}
		bw.close();
		//AdjList a1 = new AdjList(a.getAdjList());
		//AdjList newAdjList1 = a.constructDocument("computer_science");
		//a.setRelTypes(hh);*/
		//AdjList newAdjList2 = a.constructDocument("computer_science");
		//System.out.println(newAdjList1.getAdjList().size());
		//AdjList newAdjList1 = a.constructDocument("computer_science");
	}
}
