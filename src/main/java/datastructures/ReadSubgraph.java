/*
 * Author: Prajna Upadhyay
 * This class calculates the RefD scores between pairs of concepts. It reads an input subgraph built on the concepts located at most 2 hops from the input concepts. For each of them, it calculates the reference distance between every pair using threading */

package datastructures;

import java.util.ArrayList;

import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.StringTokenizer;
import java.io.*;
import java.util.*;
import java.util.Map.Entry;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;



public class ReadSubgraph 
{
	// Generates an adjacency list by reading a file containing edges. if there are 3 columns, treats the last column as the edge weight. else, assigns a default weight of 1 to the edge
	public AdjList readFromFile(BufferedReader br) 
	{
		HashMap<String, ArrayList<Edge>> hm = new HashMap<String, ArrayList<Edge>>();
		HashMap<String, ArrayList<Edge>> hm1 = new HashMap<String, ArrayList<Edge>>();
		try
		{
		//System.out.println("in read from file");
		String line;
		
		while((line=br.readLine())!=null)
		{
			//System.out.println(line);
			StringTokenizer tokenizer = new StringTokenizer(line);
			ArrayList<String> tokens = new ArrayList<String>();
			while(tokenizer.hasMoreTokens())
			{
				tokens.add(tokenizer.nextToken());
			}
			if(tokens.size()!=2) continue;
			String a = tokens.get(0).trim().toLowerCase().replace(" ", "_").replace("–", "-");
			String b = tokens.get(1).trim().toLowerCase().replace(" ", "_").replace("–", "-");
			float weight=1;
			if(tokenizer.hasMoreTokens())
			{
				weight = Float.parseFloat(tokenizer.nextToken());
			}
			if(hm.get(a)!=null)
			{
				ArrayList<Edge> c = hm.get(a);
				c.add(new Edge(weight,b));
				hm.put(a, c);
			}
			else
			{
				ArrayList<Edge> c = new ArrayList<Edge>();
				c.add(new Edge(weight,b));
				hm.put(a, c);
			}
			if(hm.get(b)==null) hm.put(b,new ArrayList<Edge>());
		}
		
		}
		catch(Exception e)
		{
			System.out.println("Exception "+e);
		}
		for(String s:hm.keySet())
		{
			ArrayList<Edge> e = hm.get(s);
			HashMap<String, Float> hh = new HashMap<String, Float>();
			for(Edge ee:e)
			{
				hh.put(ee.getName(),ee.getValue());
			}
			ArrayList<Edge> e1 = new ArrayList<Edge>();
			for(String s1:hh.keySet())
			{
				e1.add(new Edge(hh.get(s1),s1));
			}
			hm1.put(s, e1);
		}
		//System.out.println("done reading from file in the function, not returned yet");
		return new AdjList(hm1);
	}
	
	/*
	 * A function to read a knowledge base stored in the form of "entity,relationType,entity"*/
	
	public AdjList readKB(BufferedReader br)
	{
		// TODO have handled stemming of the nodes in the KB, have to run and check it.
		HashMap<String, ArrayList<Edge>> hm = new HashMap<String, ArrayList<Edge>>();
		HashMap<String, HashMap<String, ArrayList<String>>> relType = new HashMap<String, HashMap<String, ArrayList<String>>>();
		HashMap<String, HashMap<String, ArrayList<String>>> relTypeRelation = new HashMap<String, HashMap<String, ArrayList<String>>>();
		HashMap<String, HashMap<String, HashMap<String, String>>> sourceInfo = new HashMap<String, HashMap<String, HashMap<String, String>>>();
		
		try
		{
		System.out.println("in read from file");
		String line;
		
		while((line=br.readLine())!=null)
		{
			//System.out.println(line);
			ArrayList<String> tokens = new ArrayList<String>();
			StringTokenizer tokenizer = new StringTokenizer(line,"\t");
			while(tokenizer.hasMoreTokens())
			{
				tokens.add(tokenizer.nextToken());	
			}
			if(tokens.size()<3) continue;
			//String a = tokenizer.nextToken().trim().replace(" ", "_").toLowerCase();
			String a = tokens.get(0).toLowerCase().replace(" ", "_");
			//String b = tokenizer.nextToken().trim().replace("(relatedTo)","").trim();
			//String b = tokenizer.nextToken().trim();
			String b = tokens.get(1).toLowerCase().replace(" ", "_");
			//if(b.equals("is(relatedTo)")) b = "isterminology(relatedTo)";
			//if(b.equals("is(relatedTo)")) b = "is (relatedTo)";
			//String d = b1.stem(b1.removeDisambiguation(tokenizer.nextToken()).trim().replace(" ", "_").toLowerCase());
			//String d = tokenizer.nextToken().trim().replace(" ", "_").toLowerCase();
			String d = tokens.get(2).toLowerCase().replace(" ", "_");
			ArrayList<String> remainingItems = new ArrayList<String>();
			if(tokens.size()>3)
			{
				for(int i=3;i<tokens.size();i++)
				{
					remainingItems.add(tokens.get(i));
				}
			}
			//if(remainingItems.size()==0) continue;
			if(a.equals(d)) continue;
			if(hm.get(a)!=null)
			{
				ArrayList<Edge> c = hm.get(a);
				Edge e = new Edge(0,d);
				e.setRelationName(b);
				boolean flag =  false;
				for(Edge e1:c)
				{
					if(e1.getName().equals(e.getName()) && e1.getRelationaName().equals(e.getRelationaName())) 
					{
						flag = true;
						break;
					}
				}
				
				if(!flag)	c.add(e);
				hm.put(a, c);
				
			}
			else
			{
				ArrayList<Edge> c = new ArrayList<Edge>();
				Edge e = new Edge(0,d);
				e.setRelationName(b);
				c.add(e);
				hm.put(a, c);
			}
			if(hm.get(d)==null) hm.put(d,new ArrayList<Edge>());
			if(relType.get(a)!=null)
			{
				HashMap<String, ArrayList<String>> hm1 = relType.get(a);
				if(hm1.get(d)!=null)
				{
					ArrayList<String> relList = (ArrayList<String>) hm1.get(d);
					if(!relList.contains(b))
						relList.add(b);
					hm1.put(d, relList);
				}
				else
				{
					ArrayList<String> relList = new ArrayList<String>();
					relList.add(b);
					hm1.put(d, relList);
				}
				//hm1.put(d, b);
				relType.put(a, hm1);
			}
			else if(relType.get(a)==null)
			{
				HashMap<String, ArrayList<String>> hm1 = new HashMap<String, ArrayList<String>>();
				ArrayList<String> relList  = new ArrayList<String>();
				relList.add(b);
				hm1.put(d, relList);
				relType.put(a, hm1);
			}
			if(relType.get(d)==null) relType.put(d, new HashMap<String, ArrayList<String>>());
			if(relTypeRelation.get(b)!=null)
			{
				HashMap<String, ArrayList<String>> hm1 = relTypeRelation.get(b);
				if(hm1.get(a)!=null)
				{
					ArrayList<String> relList = (ArrayList<String>) hm1.get(a);
					if(!relList.contains(d))
						relList.add(d);
					hm1.put(a, relList);
				}
				else
				{
					ArrayList<String> relList = new ArrayList<String>();
					relList.add(d);
					hm1.put(a, relList);
				}
				//hm1.put(d, b);
				relTypeRelation.put(b, hm1);
			}
			else if(relTypeRelation.get(b)==null)
			{
				HashMap<String, ArrayList<String>> hm1 = new HashMap<String, ArrayList<String>>();
				ArrayList<String> relList  = new ArrayList<String>();
				relList.add(d);
				hm1.put(a, relList);
				relTypeRelation.put(b, hm1);
			}
			if(remainingItems.size()>0)
			{
				if(sourceInfo.get(a)==null)
				{
					HashMap<String, HashMap<String, String>> ss1 = new HashMap<String, HashMap<String, String>>();
					HashMap<String, String> ss2 = new HashMap<String, String>();
					ss2.put(d,remainingItems.get(0));
					ss1.put(b, ss2);
					sourceInfo.put(a, ss1);
				}
				else
				{
					HashMap<String, HashMap<String, String>> ss1 = sourceInfo.get(a);
					if(ss1.get(b)==null)
					{
						HashMap<String, String> ss2 = new HashMap<String, String>();
						ss2.put(d, remainingItems.get(0));
						ss1.put(b, ss2);
					}
					else
					{
						HashMap<String, String> ss2 = ss1.get(b);
						ss2.put(d, remainingItems.get(0));
						ss1.put(b, ss2);
					}
					sourceInfo.put(a, ss1);
				}
			}
			//System.out.println(line);
		}
		
		}
		catch(Exception e)
		{
			System.out.println("Exception "+e);
		}
		System.out.println("done reading the knowledge base, not returned yet");
		
		AdjList a1 = new AdjList(hm);
		System.out.println("waiting 1");
		a1.setRelTypes(relType);
		System.out.println("waiting 1");
		a1.setSourceInfo(sourceInfo);
		System.out.println("waiting 1");
		a1.relTypeRelation = relTypeRelation;
		return a1;
	}
	
	// Creates the data structure storing the refd scores for an input concept
	
	public static void main(String args[])
	{
		System.out.println("just got started");
		ReadSubgraph r = new ReadSubgraph();
		try
		{
		main.java.apps.GetPropertyValues properties = new main.java.apps.GetPropertyValues();
		HashMap<String, String> hm = properties.getPropValues();
		String file = hm.get("wikipedia-all-links-tsv");
		BufferedReader br1 = new BufferedReader(new FileReader(file));
		ReadSubgraph rr = new ReadSubgraph();
		AdjList a1 = rr.readFromFile(br1);
		System.out.println("read all links");
		
		BufferedReader br2 = new BufferedReader(new FileReader(hm.get("wikipedia-first-para-first-section-tsv")));
		AdjList a2 = rr.readFromFile(br2);
		System.out.println("read all links");
		
		BufferedReader br = new BufferedReader(new FileReader(hm.get("input-concepts1")));
		//File f = new File(hm.get("sparse-concepts"));
		ArrayList<String> inputConcepts1 = new ArrayList<String>();
		String line;
		while((line=br.readLine())!=null)
		{
			String cc = line.trim().replace(" ", "_").toLowerCase();
			if(!inputConcepts1.contains(cc))
				inputConcepts1.add(cc);
		}
		br = new BufferedReader(new FileReader(hm.get("input-concepts2")));
		ArrayList<String> inputConcepts2 = new ArrayList<String>();
		//String line;
		while((line=br.readLine())!=null)
		{
			String cc = line.trim().replace(" ", "_").toLowerCase();
			if(!inputConcepts2.contains(cc))
				inputConcepts2.add(cc);
		}
		
		AdjList a = rr.readKB(new BufferedReader(new FileReader(hm.get("technical-kb"))));
		BufferedWriter bw = new BufferedWriter(new FileWriter("/mnt/dell/prajna/refdScores/frameAnalysis"));
		for(String s:inputConcepts1)
		{
			if(a.getAdjList().get(s)!=null && a1.getAdjList().get(s)!=null && a2.getAdjList().get(s)!=null)
			{
			ArrayList<Edge> com = a.getAdjList().get(s);
			ArrayList<Edge> com1 = a1.getAdjList().get(s);
			ArrayList<Edge> com2 = a2.getAdjList().get(s);
			ArrayList<String> comString = a.getListInString(com);
			ArrayList<String> com1String = a.getListInString(com1);
			ArrayList<String> com2String = a.getListInString(com2);
			
			bw.write(s+"\t"+comString.size()+"\t"+com1String.size()+"\t"+com2String.size());
			}
		}
		/*File f = new File("/mnt/dell/prajna/refdScores/conceptWiseScores/");
		ArrayList<String> inputConcepts = new ArrayList<String>();
		for(File f1:f.listFiles())
		{
			for(File f2:f1.listFiles())
			{
				if(f2.getName().equals("original"))
				{
					if(a1.getAdjList().get(f1.getName())!=null)
					{
						inputConcepts.add(f1.getName());
					}
				}
				
			}
		}*/
		//BufferedWriter bw = new BufferedWriter(new FileWriter(hm.get("refdScores")+"/allRefdScores_"+f.getName()));
		long startTime = System.nanoTime();
		//a.computeRefDValuesAlternateForAllConceptWiseTKBThreaded(inputConcepts,"TKB");
		long endTime = System.nanoTime();
		System.out.println("Took "+(endTime - startTime) + " ns"); 
		//r3.writeToFile(bw);
		//ReturnConceptWiseRefDScores rr = new ReturnConceptWiseRefDScores();
		
		/*for(String inputConcept:inputConcepts)
		{
			ReturnConceptWiseRefDScores.writeConceptWiseScores(r3, inputConcept, hm, "tkbFrameOldKB");
			
		}*/
		
		//bw.close();
		//brr.close(); 
		}
		catch(Exception e)
		{
			System.out.println("Exception occured "+e);
		}
		
		
	}
}
