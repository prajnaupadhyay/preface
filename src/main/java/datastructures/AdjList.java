/*Author: Prajna Devi Upadhyay\
 * A class to represent the Wikipedia graph
 * */
/* Change the adjList HashMap to incorporate edge weights also: done
 * */
package datastructures;

import java.io.*;
import java.net.URLEncoder;
import java.util.*;
import java.util.Map.Entry;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;
import java.util.regex.Pattern;

import datastructures.LanguageModelEntity;
import datastructures.UtilityFunctions;
import javatools.parsers.PlingStemmer;

import java.sql.DriverManager;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;


import java.util.regex.Matcher;
import java.util.regex.Pattern;



public class AdjList 
{
	public HashMap<String, ArrayList<Edge>> adjList;
	HashMap<String, HashMap<String, ArrayList<String>>> relType; 
	HashMap<String, HashMap<String, ArrayList<String>>> relTypeRelation; 
	HashMap<String, HashMap<String, HashMap<String, String>>> sourceInfo;
	ConcurrentHashMap<String, ArrayList<Edge>> concAdjList;

	public AdjList (HashMap<String, ArrayList<Edge>> adjlist1)
	{ 
		this.adjList = adjlist1;
	
	}
	
	
	
	public AdjList() 
	{
		// TODO Auto-generated constructor stub
	}
	
	


	public void setAdjList(ConcurrentHashMap<String, ArrayList<Edge>> h)
	{
		this.concAdjList = h;
	}

	public HashMap<String, ArrayList<Edge>> getAdjList()
	{
		return this.adjList;
	}
	
	public ConcurrentHashMap<String, ArrayList<Edge>> getConcAdjList()
	{
		return this.concAdjList;
	}
	
	public void setAdjList(HashMap<String, ArrayList<Edge>> hm)
	{
		this.adjList = hm;
	}
	
	public HashMap<String, HashMap<String, ArrayList<String>>> getRelTypes()
	{
		return relType;
	}
	
	public HashMap<String, HashMap<String, ArrayList<String>>> getRelTypesRelation()
	{
		return relTypeRelation;
	}
	
	public void setRelTypes(HashMap<String, HashMap<String, ArrayList<String>>> relType)
	{
		this.relType = relType;
	}
	
	public void setRelTypesRelation(HashMap<String, HashMap<String, ArrayList<String>>> relType)
	{
		this.relTypeRelation = relType;
	}
	
	
	public void setSourceInfo(HashMap<String, HashMap<String, HashMap<String, String>>> sourceInfo)
	{
		this.sourceInfo = sourceInfo;
	}
	
	public HashMap<String, HashMap<String, HashMap<String, String>>> getSourceInfo()
	{
		return this.sourceInfo;
	}
	
	static Comparator<Entry<String, Integer>> valueComparator = new Comparator<Entry<String, Integer>>() 
	{
		 public int compare(Entry<String, Integer> e1, Entry<String, Integer> e2)
		 {
			 Integer v1 = e1.getValue();
			 Integer v2 = e2.getValue();
			 return v2.compareTo(v1);
		 }
	};
	
	
	/**
	 * retruns the number of edges in this graph
	 * @return
	 */
	
	public int numberOfEdges()
	{
		int n=0;
		for(String s:this.getAdjList().keySet())
		{
			ArrayList<Edge> e = this.getAdjList().get(s);
			n = n + e.size();
		}
		return n;
	}
	/**
	 * Given a disambiguation index, this function returns the neighbours of the original versions of the input concept's name
	 * @param disamb: disambiguation index
	 * @param input: input concept
	 * @return
	 */
	
	public ArrayList<Edge> returnNeighboursUsingDisambiguation(HashMap<String, ArrayList<String>> disamb, String input)
	{
		
		ArrayList<String> l1 = disamb.get(input);
		if(l1==null) 
		{
			
			return new ArrayList<Edge>();
		}
		else
		{
			for(String s:l1)
			{
				ArrayList<Edge> e1 = this.getAdjList().get(s);
				if(s!=null)
				{
					return e1;
					
				}
				
			}
			return new ArrayList<Edge>();
		}
	}
	
	
	
	
	/**
	 * 
	 * returns the incident adjacency list for a graph*/
	
	public HashMap<String, ArrayList<String>> getIncidentList()
	{
		HashMap<String, ArrayList<String>> incidentList = new HashMap<String, ArrayList<String>>();
		try
		{
			Iterator it = this.getAdjList().entrySet().iterator();
		    while (it.hasNext()) 
		    {
		        Map.Entry pair = (Map.Entry)it.next();
		        ArrayList<Edge> list = (ArrayList<Edge>) pair.getValue();
		        
		        for(int i = 0;i<list.size();i++)
		        {
		        	ArrayList<String> inList;
		        	if(incidentList.get(list.get(i).getName())==null)
		        	{
		        		inList=new ArrayList<String>();
		        	}
		        	else
		        	{
		        		inList=incidentList.get(list.get(i).getName());
		        	}
		        	if(!inList.contains((String)pair.getKey()))
		        		inList.add((String)pair.getKey());
		        	incidentList.put(list.get(i).getName(), inList);
		        }
		        
		       // it.remove(); // avoids a ConcurrentModificationException
		    }
		    for(String key:this.getAdjList().keySet())
		    {
		    	if(incidentList.get(key)==null)
		    	{
		    		incidentList.put(key, new ArrayList<String>());
		    	}
		    }
		    
		}
		catch(Exception e)
		{
			System.out.println("Exception occured "+e);
		}
		return incidentList;
	}
	
	/**
	 * generates the incident list for a graph and the adjacency list of each node is a list of 'Edge's
	 * @return
	 */
	
	
	public HashMap<String, ArrayList<Edge>> getIncidentListEdgeType()
	{
		HashMap<String, ArrayList<Edge>> incidentList = new HashMap<String, ArrayList<Edge>>();
		try
		{
			Iterator it = this.getAdjList().entrySet().iterator();
		    while (it.hasNext()) 
		    {
		        Map.Entry pair = (Map.Entry)it.next();
		        ArrayList<Edge> list = (ArrayList<Edge>) pair.getValue();
		        
		        for(int i = 0;i<list.size();i++)
		        {
		        	ArrayList<Edge> inList;
		        	if(incidentList.get(list.get(i).getName())==null)
		        	{
		        		inList=new ArrayList<Edge>();
		        	}
		        	else
		        	{
		        		inList=incidentList.get(list.get(i).getName());
		        	}
		        	ArrayList<String> inListString = this.getListInString(inList);
		        	if(!inListString.contains((String)pair.getKey()))
		        		inList.add(new Edge(0, (String)pair.getKey(),list.get(i).getRelationaName()));
		        	incidentList.put(list.get(i).getName(), inList);
		        }
		        
		       // it.remove(); // avoids a ConcurrentModificationException
		    }
		    for(String key:this.getAdjList().keySet())
		    {
		    	if(incidentList.get(key)==null)
		    	{
		    		incidentList.put(key, new ArrayList<Edge>());
		    	}
		    }
		    
		}
		catch(Exception e)
		{
			System.out.println("Exception occured "+e);
		}
		return incidentList;
	}
	
	
	public void writeToFile(BufferedWriter bw)
	{
		try
		{
		Iterator it = this.getAdjList().entrySet().iterator();
	    while (it.hasNext()) 
	    {
	        Map.Entry pair = (Map.Entry)it.next();
	        ArrayList<Edge> list = (ArrayList<Edge>) pair.getValue();
	       // System.out.println(list.size());
	        for(int i = 0;i<list.size();i++)
	        {
	        	if(list.get(i).getRelationaName()==null)
	        		bw.write(pair.getKey() + "\t" + list.get(i).getName()+"\n");
	        	else
	        		bw.write(pair.getKey() + "\t" + list.get(i).getRelationaName()+"\t"+list.get(i).getName()+"\n");
	        }
	        
	       // it.remove(); // avoids a ConcurrentModificationException
	    }
	   // bw.close();
		}
		catch(Exception e)
		{
			System.out.println("Exception occurred: "+e);
		}
	}
	
	
	
	/**
	 * merges graph a1 with this graph and writes to file
	 * @param a1
	 * @param file
	 * @throws Exception
	 */
	
	public void mergeAdjacencyList(AdjList a1, String file) throws Exception
	{
		HashMap<String, HashMap<String, HashSet<String>>> hm = new HashMap<String, HashMap<String, HashSet<String>>>();
		for(String s:a1.getAdjList().keySet())
		{
			HashMap<String, HashSet<String>> hh;
			if(hm.get(s)==null)
			{
				hh = new HashMap<String, HashSet<String>>();
			}
			else
			{
				hh = hm.get(s);
			}
			for(Edge e:a1.getAdjList().get(s))
			{
				String r = e.getRelationaName();
				String n = e.getName();
				HashSet<String> hh1;
				if(hh.get(r)==null)
				{
					hh1 = new HashSet<String>();
					
				}
				else
				{
					hh1 = hh.get(r);
				}
				hh1.add(n);
				hh.put(r, hh1);
			}
			hm.put(s, hh);
		}
		for(String s:this.getAdjList().keySet())
		{
			HashMap<String, HashSet<String>> hh;
			if(hm.get(s)==null)
			{
				hh = new HashMap<String, HashSet<String>>();
			}
			else
			{
				hh = hm.get(s);
			}
			for(Edge e:this.getAdjList().get(s))
			{
				String r = e.getRelationaName();
				String n = e.getName();
				HashSet<String> hh1;
				if(hh.get(r)==null)
				{
					hh1 = new HashSet<String>();
					
				}
				else
				{
					hh1 = hh.get(r);
				}
				hh1.add(n);
				hh.put(r, hh1);
			}
			hm.put(s, hh);
		}
		
		BufferedWriter bw = new BufferedWriter(new FileWriter(file));
		for(String s:hm.keySet())
		{
			for(String s1:hm.get(s).keySet())
			{
				HashSet<String> h1 = hm.get(s).get(s1);
				for(String s2:h1)
				{
					bw.write(s+"\t"+s1+"\t"+s2+"\n");
					if(!s1.contains("_inverse"))
					{
						if(hm.get(s2)!=null)
						{
							if(hm.get(s2).get(s1+"_inverse")!=null)
							{
								if(hm.get(s2).get(s1+"_inverse").contains(s))
								{
									
								}
								else
								{
									bw.write(s2+"\t"+s1+"_inverse\t"+s+"\n");
								}
							}
							else
							{
								bw.write(s2+"\t"+s1+"_inverse\t"+s+"\n");
							}
						}
						else
						{
							bw.write(s2+"\t"+s1+"_inverse\t"+s+"\n");
						}
					}
				}
			}
		}
		bw.close();
		
	}
	
	public void writeToFileWithSource(BufferedWriter bw, HashMap<String, HashMap<String, HashMap<String, String>>> sourceInfo)
	{
		try
		{
		Iterator it = this.getAdjList().entrySet().iterator();
	    while (it.hasNext()) 
	    {
	        Map.Entry pair = (Map.Entry)it.next();
	        ArrayList<Edge> list = (ArrayList<Edge>) pair.getValue();
	       // System.out.println(list.size());
	        for(int i = 0;i<list.size();i++)
	        {
	        	if(list.get(i).getRelationaName()==null)
	        		bw.write(pair.getKey() + "\t" + list.get(i).getName()+"\n");
	        	else
	        		bw.write(pair.getKey() + "\t" + list.get(i).getRelationaName()+"\t"+list.get(i).getName()+"\t"+sourceInfo.get(pair.getKey()).get(list.get(i).getRelationaName()).get(list.get(i).getName()).replace(" ", "_")+"\n");
	        }
	        
	       // it.remove(); // avoids a ConcurrentModificationException
	    }
	    bw.close();
		}
		catch(Exception e)
		{
			System.out.println("Exception occurred: "+e);
		}
	}
	
	public void writeToFileWithScores(BufferedWriter bw)
	{
		try
		{
		if(this.getAdjList()!=null) 
		{
			Iterator it = this.getAdjList().entrySet().iterator();
		    while (it.hasNext()) 
		    {
		        Map.Entry pair = (Map.Entry)it.next();
		        ArrayList<Edge> list = (ArrayList<Edge>) pair.getValue();
		       // System.out.println(list.size());
		        for(int i = 0;i<list.size();i++)
		        {
		        	if(list.get(i).getRelationaName()==null)
		        		bw.write(pair.getKey() + "\t" + list.get(i).getName()+"\t"+list.get(i).getValue()+"\n");
		        }
		        
		       // it.remove(); // avoids a ConcurrentModificationException
		    }
		}
	    bw.close();
		}
		catch(Exception e)
		{
			System.out.println("Exception occurred: "+e);
		}
	}
	
	/**
	 * given an entity a, it extracts all of its siblings. A sibling is one that is the child of all the parents a is the child of
	 * @param a
	 * @return
	 */
	
	public ArrayList<String> extractSiblings(String a)
	{
		HashMap<String, ArrayList<String>> incidentList = this.getIncidentList();
		ArrayList<Edge> neighbours = this.getAdjList().get(a);
		ArrayList<String> siblings = new ArrayList<String>();
		if(neighbours==null) return new ArrayList<String>();
		for(Edge e:neighbours)
		{
			if(e.getRelationaName().equals("typeOf"))
			{
				ArrayList<String> s1 = incidentList.get(e.getName());
				if(s1==null) continue;
				for(String s2:s1)
				{
					if(this.getRelTypes().get(s2).get(e.getName()).contains("typeOf"))
					{
						siblings.add(s2);
					}
				}
			}
		}
		return siblings;
	}
	
	/**
	 *  Given a list of nodes, returns a subgraph (AdjList) induced by the nodes
	 * @param nodesNames
	 * @return
	 */
	 
	public AdjList generateSubgraphAndNodesFromList(ArrayList<String> nodesNames1)
	{
		//RefDScores r = this.generateRefDScores();
		HashMap<String, ArrayList<Edge>> adjListNew = new  HashMap<String, ArrayList<Edge>>();
		HashSet<String> nodesNames = new HashSet<String>(nodesNames1);
		int c=0;
		 for(String s1:nodesNames)
	     {	
			 c++;
			 	//System.out.println(nodesNames.get(i));
	        	ArrayList<Edge> oldNeighbours = adjList.get(s1);
	        	
	        	if(oldNeighbours!=null)
	        	{
	        	//System.out.println(oldNeighbours);
	        	HashSet<String> newNeighbours = new HashSet<String>();
	        	for(Edge e:oldNeighbours)
	        	{
	        		//if(nodesNames.contains(e.getName()))
	        			newNeighbours.add(e.getName());
	        	}
	        	//newNeighbours.addAll(oldNeighbours);
	        	newNeighbours.retainAll(nodesNames);
	        	ArrayList<Edge> newerNeighbours = new ArrayList<Edge>();
	        	for(String s:newNeighbours)
	        	{
	        		//Float val = r.getRefdScoresForConcept(nodesNames.get(i)).get(s);
	        		newerNeighbours.add(new Edge(0, s));
	        	}
	        	adjListNew.put(s1, newerNeighbours);
	        	}
	        	else
	        	{
	        		System.out.println("null");
	        	}
	        	if(c%1000==0) System.out.println(c);
	        }
		
	        return new AdjList(adjListNew);
		
	}
	
	/**
	 * Given a list of nodes, returns a subgraph induced by this list along with relationship names
	 * @param nodesNames
	 * @return
	 */
	
	public AdjList generateSubgraphAndNodesFromListWithRelations(ArrayList<String> nodesNames)
	{
		//RefDScores r = this.generateRefDScores();
		HashMap<String, ArrayList<Edge>> adjListNew = new  HashMap<String, ArrayList<Edge>>();
		 for(int i=0;i<nodesNames.size();i++)
	        {	
			 	//System.out.println(nodesNames.get(i));
	        	ArrayList<Edge> oldNeighbours = adjList.get(nodesNames.get(i));
	        	
	        	if(oldNeighbours!=null)
	        	{
	        	//System.out.println(oldNeighbours);
	        	ArrayList<String> newNeighbours = new ArrayList<String>();
	        	for(Edge e:oldNeighbours)
	        	{
	        		//if(nodesNames.contains(e.getName()))
	        			newNeighbours.add(e.getName());
	        	}
	        	//newNeighbours.addAll(oldNeighbours);
	        	newNeighbours.retainAll(nodesNames);
	        	ArrayList<Edge> newerNeighbours = new ArrayList<Edge>();
	        	for(String s:newNeighbours)
	        	{
	        		
	        		//Float val = r.getRefdScoresForConcept(nodesNames.get(i)).get(s);
	        		newerNeighbours.add(new Edge(0, s));
	        	}
	        	adjListNew.put(nodesNames.get(i), newerNeighbours);
	        	}
	        }
	        return new AdjList(adjListNew);
		
	}
	
	/**
	 * 
	  Returns the list of edges as list of string*/
	public ArrayList<String> getListInString(ArrayList<Edge> n1)
	{
		ArrayList<String> neighbours1 = new ArrayList<String>();
		if(n1==null) return neighbours1;
		for(Edge e:n1)
		{
			neighbours1.add(e.getName());
		}
		return neighbours1;
	}
	

	/**
	 *  Returns the list induced by the list of strings from this adjacency list*/

	public ArrayList<Edge> getEdgeListInducedByInputConceptsNeighbours(String input, ArrayList<String> neigh)
	{
		ArrayList<Edge> edgeNeighs = this.adjList.get(input);
		ArrayList<Edge> newEdgeNeighs = new ArrayList<Edge>();
		for(Edge neighs:edgeNeighs)
		{
			if(neigh.contains(neighs.name))
			{
				newEdgeNeighs.add(neighs);
			}
		}
		return newEdgeNeighs;
	}
	/**
	 *  Given a HashMap<String, ArrayList<String>> as input, it returns the AdjList induced by it
	 * @param list1
	 * @return
	 */
	 
	public AdjList returnInducedAdjList(HashMap<String, ArrayList<String>> list1)
	{
		HashMap<String, ArrayList<Edge>> a = new HashMap<String, ArrayList<Edge>>();
		Iterator it = list1.entrySet().iterator();
	    while (it.hasNext()) 
	    {
	       Map.Entry pair = (Map.Entry)it.next();
	       String key = (String) pair.getKey();
	       ArrayList<String> neigh = (ArrayList<String>) pair.getValue();
	       ArrayList<Edge> edgeNeighs = getEdgeListInducedByInputConceptsNeighbours(key, neigh);
	       a.put(key,edgeNeighs);
	       // it.remove(); // avoids a ConcurrentModificationException
	    }
		return new AdjList(a);
		
	}
	
	/**
	 * function to convert the AdjList representation into a hashmap of string and string list*/
	
	public HashMap<String, ArrayList<String>> returnStringHashMap()
	{
		Iterator it = this.adjList.entrySet().iterator();
		HashMap<String, ArrayList<String>> newList = new HashMap<String, ArrayList<String>>();
		while(it.hasNext())
		{
			Map.Entry pair = (Map.Entry)it.next();
			String key = (String) pair.getKey();
			ArrayList<Edge> e = (ArrayList<Edge>) pair.getValue();
			ArrayList<String> s = getListInString(e);
			newList.put(key, s);
		}
		return newList;
	}
	
	/**
	 *  Takes a pair of input concepts and calculates RefD between them
	 * @param inputConcept1
	 * @param inputConcept2
	 * @return
	 */
	 
	
	public float referenceDistance(String inputConcept1, String inputConcept2)
	{
		ArrayList<Edge> n1 = this.getAdjList().get(inputConcept1);
		if(n1==null) return -9999;
		int aNeedsB = 0;
		int bNeedsA = 0;
		ArrayList<String> neighbours1 = getListInString(n1);
		/*************** line added to experiment *****/
		neighbours1.add(inputConcept1);
		/*************** line added to experiment *****/
		for(String neighbour:neighbours1)
		{
			if(getListInString(this.getAdjList().get(neighbour)).contains(inputConcept2))
			{
				aNeedsB++;
			}
		}
		ArrayList<Edge> n2 = this.getAdjList().get(inputConcept2);
		if(n2==null) return -9999;
		ArrayList<String> neighbours2 = getListInString(n2);
		/*************** line added to experiment *****/
		neighbours2.add(inputConcept2);
		/*************** line added to experiment *****/
		for(String neighbour:neighbours2)
		{
			if(getListInString(this.getAdjList().get(neighbour)).contains(inputConcept1))
			{
				bNeedsA++;
			}
			
		}
		if(n1.size()==0 || n2.size() == 0) return -9999;
		else
		{
			float refd = (float) ((float)aNeedsB/(float)(n1.size()+1)) - (float)((float)bNeedsA/(float)(n2.size()+1));
			return refd;
		}
	}
	
	
	public float referenceDistanceWithFrameSupplied(String inputConcept1, String inputConcept2, ArrayList<String> frame)
	{
		//ArrayList<Edge> n1 = this.getAdjList().get(inputConcept1);
		//if(n1==null) return -9999;
		int aNeedsB = 0;
		int bNeedsA = 0;
		ArrayList<String> neighbours1 = frame;
		ArrayList<String> n1 = frame;
		/*************** line added to experiment *****/
		neighbours1.add(inputConcept1);
		/*************** line added to experiment *****/
		for(String neighbour:neighbours1)
		{
			
			if(getListInString(this.getAdjList().get(neighbour)).contains(inputConcept2))
			{
				aNeedsB++;
			}
		}
		ArrayList<Edge> n2 = this.getAdjList().get(inputConcept2);
		if(n2==null) return -9999;
		ArrayList<String> neighbours2 = getListInString(n2);
		/*************** line added to experiment *****/
		neighbours2.add(inputConcept2);
		/*************** line added to experiment *****/
		for(String neighbour:neighbours2)
		{
			if(getListInString(this.getAdjList().get(neighbour)).contains(inputConcept1))
			{
				bNeedsA++;
			}
			
		}
		if(n1.size()==0 || n2.size() == 0) return -9999;
		else
		{
			float refd = (float) ((float)aNeedsB/(float)(n1.size()+1)) - (float)((float)bNeedsA/(float)(n2.size()+1));
			return refd;
		}
	}
	
	
	
	/**
	 *  Takes a pair of input concepts and calculates modified RefD between them. List "neigh1" consists of the concepts mentioned in the first para of node1 and neigh2 consists of the concepts mentioned in the first para of node2
	 * */
	
	public float referenceDistanceModified(String inputConcept1, String inputConcept2, ArrayList<String> neigh1, ArrayList<String> neigh2, HashMap<String, ArrayList<String>> wikipedia)
	{
		ArrayList<Edge> n1 = this.getAdjList().get(inputConcept1);
		int aNeedsB = 0;
		int bNeedsA = 0;
		int c1=0,c2=0;
		ArrayList<String> neighbours1 = getListInString(n1);
		for(String neighbour:neighbours1)
		{
			if(getListInString(this.getAdjList().get(neighbour)).contains(inputConcept2))
			{
				aNeedsB++;
			}
			if(wikipedia.get(neighbour)!=null)
			{
				if(wikipedia.get(neighbour).contains(inputConcept2))
				{
					c1++;
				}
			}
		/*	if(neigh1!=null)
			{
				if(neigh1.contains(neighbour))
				{
					aNeedsB++;
				}
			}*/
		}
		ArrayList<Edge> n2 = this.getAdjList().get(inputConcept2);
		ArrayList<String> neighbours2 = getListInString(n2);
		for(String neighbour:neighbours2)
		{
			if(getListInString(this.getAdjList().get(neighbour)).contains(inputConcept1))
			{
				bNeedsA++;
			}
		/*	if(neigh2!=null)
			{
				if(neigh2.contains(neighbour))
				{
				bNeedsA++;
				}
			}*/
			if(wikipedia.get(neighbour)!=null)
			{
				if(wikipedia.get(neighbour).contains(inputConcept1))
				{
					c2++;
				}
			}
			
			
		}
		int a1=0, b1=0;
		if(neigh1!=null) a1 = neigh1.size();
		if(neigh2!=null) b1 = neigh2.size();
		
		if((n1.size()+c1)==0 || (n2.size()+c1) == 0) return -9999;
		else
		{
			float refd = (float) ((float)aNeedsB/(float)(n1.size()+a1+c1)) - (float)((float)bNeedsA/(float)(n2.size()+b1+c2));
			return refd;
		}
	}
	
	
	public float jaccardSimilarity(ArrayList<String> category1, ArrayList<String> category2)
	{
		ArrayList<String> comCates1 = category1;
		ArrayList<String> comCates = new ArrayList<String>(new HashSet<String>(comCates1));
		ArrayList<String> allCategories1 = category1;
		allCategories1.addAll(category2);
		ArrayList<String> allCategories = new ArrayList<String>(new HashSet<String>(allCategories1));
		comCates.retainAll(category2);
		return (float)(comCates.size())/(float)(allCategories.size());
	}
	
	/**
	 *  Returns the list of nodes for an adjacency list
	 * @return
	 */
	public ArrayList<String> getNodesList()
	{
		//System.out.println("in get nodes list");
		ArrayList<String> nodesList = new ArrayList<String>();
		Iterator it = this.getAdjList().entrySet().iterator();
	    while (it.hasNext()) 
	    {
	        Map.Entry pair = (Map.Entry)it.next();
	       // System.out.println((String)pair.getKey());
	       nodesList.add((String)pair.getKey());
	       
	        
	       // it.remove(); // avoids a ConcurrentModificationException
	    }
	    return nodesList;
	}
	/**
	 *  Returns true if the graph contains cycles. master function to call containsCycles1* */
	
	public boolean hasCycles(String name)
	{
	    return containsCycles1(name, new LinkedList<String>(), new LinkedList<String>(),0);
	}
	
	/**
	 * latest implementation to check whether a graph contains cycles or not starting traversal from an input node*/
	
	public boolean containsCycles1(String inputConcept, LinkedList<String> visited, LinkedList<String> onStack,int depth)
	{
		System.out.println("visited: "+inputConcept);
		visited.add(inputConcept);
		onStack.add(inputConcept);
		if(depth==4) return false;
		boolean hasCycle=false;
		if(adjList.get(inputConcept)==null) return false;
		for(Edge neigh: adjList.get(inputConcept))
		{
			if(!visited.contains(neigh.getName()))
			{
				hasCycle = hasCycle | containsCycles1(neigh.getName(), visited, onStack, depth+1);
			}
			else if(onStack.contains(neigh.getName()))
			{
				
				return true;
			}
		}
		onStack.remove(inputConcept);
		return hasCycle;
	}
	
	
	/**
	 *  detect and removes the edge that contains the minimum refd value in a cycle. Cycles are discovered by doing a DFS from the source node
	 * @param inputConcept
	 * @param visited
	 * @param onStack
	 * @param a1
	 * @param pred
	 * @param a3
	 */
	public void detectAndRemoveCycles(String inputConcept, LinkedList<String> visited, LinkedList<String> onStack, HashMap<String, ArrayList<Edge>> a1, HashMap<String, String> pred, AdjList a3)
	{
		System.out.println("visted: "+inputConcept);
		visited.add(inputConcept);
		onStack.add(inputConcept);
		//boolean hasCycle=false;
		for(Edge neigh: adjList.get(inputConcept))
		{
			if(!visited.contains(neigh.getName()))
			{
				pred.put(neigh.getName(), inputConcept);
				detectAndRemoveCycles(neigh.getName(), visited, onStack, a1, pred, a3);
			}
			else if(onStack.contains(neigh.getName()))
			{
				//ArrayList<Edge> e = new ArrayList<Edge>();
				String a = inputConcept;
				float minRefd = a3.referenceDistance(pred.get(a),a);
				Edge minE = new Edge();
				minE.setName(pred.get(a));
				minE.setRelationName(a);
				minE.setValue(minRefd);
				while(pred.get(a)!=null)
				{
					if(pred.get(a).equals(neigh.getName()))
					{
						float refd2 = a3.referenceDistance(a,pred.get(a));
						if(refd2 < minRefd) 
						{
							minRefd = refd2;
							minE.setName(pred.get(a));
							minE.setRelationName(a);
							minE.setValue(refd2);
						}
						ArrayList<Edge> newList = a1.get(minE.getName());
						ArrayList<Edge> newList1 = new ArrayList<Edge>();
						if(newList!=null)
						{
							for(Edge p:newList)
							{
								if(!p.getName().equals(minE.getRelationaName()))
								{
									newList1.add(p);
								}
							}
						}
						a1.put(minE.getName(), newList1);
						//bw.write(neigh+" is the dfs predecessor of "+a+"\n");
						//bw.write("Cycle ends"+"\n");
						//System.out.println(neigh.getName()+" is the dfs predecessor of "+a+"\n");
						//System.out.println("Cycle ends"+"\n");
						break;
					}
					else
					{
						Edge e1 = new Edge();
						e1.setName(pred.get(a));
						e1.setRelationName(a);
						
						float refd1 = a3.referenceDistance(pred.get(a),a);
						e1.setValue(refd1);
						if(refd1 < minRefd) 
						{
							minRefd = refd1;
							minE.setName(pred.get(a));
							minE.setRelationName(a);
							minE.setValue(refd1);
						}
						//bw.write(pred.get(a)+" is the dfs predecessor of "+a+"\n");
						//System.out.println(pred.get(a)+" is the dfs predecessor of "+a+"\n");
						a = pred.get(a);
					}
					
				}
				
				//return true;
			}
		}
		onStack.remove(inputConcept);
		//return hasCycle;
	}
	

	
	/**
	 * 
	 * 
	 * master function to call findCycle*/
	
	public void containsCycles(String inputConcept, BufferedWriter bw) throws Exception
	{
		boolean hasCycle=false;
		//LinkedList<String> queue = new LinkedList<String>();
		LinkedList<String> visited = new LinkedList<String>();
		LinkedList<String> onStack = new LinkedList<String>();
		HashMap<String, String> pred = new HashMap<String, String>();
		findCycle(inputConcept, visited, onStack, hasCycle, pred, bw,0);
		System.out.println("Graph has cycles "+hasCycle);
		//queue.add(inputConcept);
		// dfs(inputConcept, visited, pred);
		//return bfs(inputConcept, queue, visited, pred);
	}
	
	/**
	 * old implementation of finding whether a graph contains a cycle or not*/
	
	public void findCycle(String inputConcept, LinkedList<String> visited, LinkedList<String> onStack, boolean hasCycle, HashMap<String, String> pred, BufferedWriter bw, int depth) throws Exception
	{
		//System.out.println(inputConcept+" to be added to visited and stack");
		
		visited.add(inputConcept);
		onStack.add(inputConcept);
		if(depth==4) return;
		/*for(String i:visited)
		{
			System.out.println(i);
		}
		for(String j:onStack)
		{
			System.out.println(j);
		}*/
		if(adjList.get(inputConcept)==null) return;
		for(Edge neigh: adjList.get(inputConcept))
		{
			if(!visited.contains(neigh.getName()))
			{
				pred.put(neigh.getName(), inputConcept);
				findCycle(neigh.getName(), visited, onStack, hasCycle, pred, bw, depth+1);
			}
			else if(onStack.contains(neigh.getName()))
			{
				String a = inputConcept;
				bw.write("\nCycle starts\n");
				bw.write(inputConcept+" is the dfs predecessor of "+neigh.getName()+"\n");
				//System.out.println("\nCycle starts\n");
				//System.out.println(inputConcept+" is the dfs predecessor of "+neigh.getName()+"\n");
				hasCycle = true;
				while(pred.get(a)!=null)
				{
					if(pred.get(a).equals(neigh.getName()))
					{
						bw.write(neigh.getName()+" is the dfs predecessor of "+a+"\n");
						bw.write("Cycle ends"+"\n");
						//System.out.println(neigh.getName()+" is the dfs predecessor of "+a+"\n");
						//System.out.println("Cycle ends"+"\n");
						break;
					}
					else
					{
						bw.write(pred.get(a)+" is the dfs predecessor of "+a+"\n");
						System.out.println(pred.get(a)+" is the dfs predecessor of "+a+"\n");
						a = pred.get(a);
					}
					
				}
				
				return;
			}
		}
		onStack.remove(inputConcept);
	}
	
	
	/**
	 * dfs implementation of whether a graph contains a cycle or not*/
	
	public void dfs(String inputConcept, LinkedList<String> visited, HashMap<String, String> pred)
	{
		System.out.println(inputConcept);
		System.out.println("pred is: "+pred);
		if(adjList.get(inputConcept)==null) return;
		for(Edge neigh: adjList.get(inputConcept))
		{
			if(visited.contains(neigh.getName())) 
			{
				/*String a = inputConcept;
				System.out.println("Cycle starts");
				System.out.println(a+" has an edge to "+neigh);
				while(pred.get(a)!=null)
				{
					if(pred.get(a).equals(neigh))
					{
						System.out.println(neigh+" is the dfs predecessor of "+a);
						System.out.println("Cycle ends");
						break;
					}
					else
					{
						System.out.println(pred.get(a)+" is the dfs predecessor of "+a);
						a = pred.get(a);
					}
					
				}*/
				return;
			}
			else 
			{
				pred.put(neigh.getName(), inputConcept);
				dfs(neigh.getName(), visited, pred);
			}
		}
		visited.add(inputConcept);
		
	}
	
	/**
	 * 
	 * check if the graph contains a cycle given a input concept to start from*/
	
	public boolean bfs(String inputConcept, LinkedList<String> queue, LinkedList<String> visited, HashMap<String, String> pred)
	{
		while(queue.size()>0)
		{
			String a=queue.removeFirst();
			visited.add(a);
			for(Edge neigh: adjList.get(a))
			{
				if(!queue.contains(neigh) && !visited.contains(neigh))
				{
					queue.add(neigh.getName());
					pred.put(neigh.getName(), a);
				}
				else if(visited.contains(neigh))
				{
					String b=a;
					System.out.println(neigh);
					System.out.println(a);
					
					while(pred.get(b)!=null)
					{
						if(pred.get(b).equals(neigh))
						return true;
					}
				}
			}
		}
		return false;
	}
	
	/**
	 * 
	 * @param inputConcept
	 * @param queue
	 * @param visited
	 * @param pred
	 * @return
	 */
	
	
	public void allKHopPaths(String inputConcept, String target, int k, BufferedWriter bw) throws Exception
	{
		
		LinkedList<Edge> queue = new LinkedList<Edge>();
		queue.add(new Edge(0,inputConcept,""));
		HashMap<String, Edge> pred = new HashMap<String, Edge>();
		int count=0;
		while(queue.size()>0 && count<=k)
		{
			Edge a=queue.removeFirst();
			
			for(Edge neigh: adjList.get(a.getName()))
			{
				queue.add(neigh);
				
				pred.put(neigh.getName(), a);
				if(count==k && (neigh.getName().equals(target) || neigh.getName().equals(target.replace("category:", ""))))
				{
					String p = neigh.getName();
					bw.write(p+"\t"+neigh.getRelationaName()+"\t");
					
					int cc =1;
					while(pred.get(p)!=null && cc<k)
					{
						Edge eee = pred.get(p);
						p = eee.getName();
						bw.write(p+"\t"+eee.getRelationaName()+"\t");
						cc++;
					}
					bw.write(inputConcept+"\n");
				}
			}
			count++;
			
		}
		
		
		
	}
	
	/**
	 * returns true if the query node is reachable from the current node
	 * */
	
	public boolean isReachable(String query, String currentNode, LinkedList<String> visited)
	{
		boolean reachable=false;
		//System.out.println("visited: "+currentNode);
		//System.out.println(this.getAdjList().get(currentNode)+"\n");
		if(query.equals(currentNode))
		{
			return true;
		}
		else
		{
			if(this.getAdjList().get(currentNode)==null) return false;
			for(int i=0;i<this.getAdjList().get(currentNode).size();i++)
			{
				
				if(!visited.contains(this.getAdjList().get(currentNode).get(i).name))
				{
					visited.add(this.getAdjList().get(currentNode).get(i).name);
					//Edge e = new Edge((float)0,this.getAdjList().get(currentNode).get(i));
					reachable = reachable | isReachable(query, this.getAdjList().get(currentNode).get(i).name, visited);
				}
			}
		}
		return reachable;
	}
	
	
	
	/**
	 * 
	 * @param start: start node from which the bfs started
	 * @param query: end node to which the bfs should end
	 * @param currentNode: node traversed currently
	 * @param visited: list of nodes visited
	 * @param length: we are searching for paths of this length
	 * @param bw: file to write the found paths
	 * @return
	 */
	
	public void isReachableInHops(String start, String query, String currentNode, LinkedList<Edge> visited, ArrayList<Edge> path, int length, BufferedWriter bw, HashMap<String, HashMap<String, HashMap<String, String>>> paths) throws Exception
	{
		boolean reachable=false;
		//System.out.println(visited.size());
		//System.out.println("visited: "+currentNode);
		//System.out.println(this.getAdjList().get(currentNode)+"\n");
		if(query.equals(currentNode) && path.size()==length+1)
		{
			//System.out.print("\n"+start+"\t");
			HashMap<String, String> hh = paths.get(path.get(0).getName()).get(path.get(1).getName());
			hh.put(path.get(2).getName(), "");
			paths.get(path.get(0).getName()).put(path.get(1).getName(),hh);

			//bw.write(start+"\t");
			/*int c=0;
			for(Edge e:path)
			{
				if(c==0)
				{
					c++;
					continue;
				}
				c++;
				bw.write(e.getRelationaName()+"\t"+e.getName()+"\t");
				//System.out.print(e.getRelationaName()+"\t"+e.getName()+"\t");
			}*/
			//bw.write("\n");
			//System.out.println("\n");
			path.remove(path.size()-1);
			
		}
		else
		{
			if(!query.equals(currentNode) && path.size()==length+1)
			{
				//System.out.print("\n"+start+"\t");
				//bw.write(start+"\t");
				/*int c=0;
				for(Edge e:path)
				{
					if(c==0)
					{
						c++;
						continue;
					}
					c++;
					//bw.write(e.getRelationaName()+"\t"+e.getName()+"\t");
					//System.out.print(e.getRelationaName()+"\t"+e.getName()+"\t");
				}*/
				//System.out.println("\n");
				path.remove(path.size()-1);
				
			}
			
			else
			{
				for(int i=0;i<this.getAdjList().get(currentNode).size();i++)
				{
					
					if(path.size()==1)
					{
						HashMap<String, HashMap<String, String>> hh = paths.get(path.get(0));
						if(hh==null)
						{
							
						}
					}
					if(!this.getListInString(new ArrayList<Edge>(path)).contains(this.getAdjList().get(currentNode).get(i).name))
					{
						String a1 = this.getAdjList().get(currentNode).get(i).getName();
						String a2 = this.getAdjList().get(currentNode).get(i).getRelationaName();
						if(a2.equals("owl:sameas")) continue;
						Edge ee = new Edge(0,a1, a2);
						visited.add(ee);
						path.add(ee);
						//System.out.print(ee.getRelationaName()+"\t"+ee.getName()+"\t");
						//Edge e = new Edge((float)0,this.getAdjList().get(currentNode).get(i));
						isReachableInHops(start, query, this.getAdjList().get(currentNode).get(i).name, visited, path, length, bw, paths);
					//	visited.remove(visited.size()-1);
					}
					else
					{
						continue;
					}
				}
			}
		}
		//System.out.print("\n"+start+"\t");
		//bw.write(start+"\t");
		/*int c=0;
		for(Edge e:visited)
		{
			if(c==0)
			{
				c++;
				continue;
			}
			c++;
			//bw.write(e.getRelationaName()+"\t"+e.getName()+"\t");
			//System.out.print(e.getRelationaName()+"\t"+e.getName()+"\t");
		}*/
		//System.out.println("\n");
		//visited.remove(visited.size()-1);
		
	}
	
	
	/**
	 * is reachable via taxonomy
	 * @param query
	 * @param currentNode
	 * @param visited
	 * @return
	 */
	public boolean isReachableViaTaxonomy(String query, String currentNode, LinkedList<String> visited)
	{
		boolean reachable=false;
		//System.out.println("visited: "+currentNode);
		//System.out.println(this.getAdjList().get(currentNode)+"\n");
		if(query.equals(currentNode))
		{
			return true;
		}
		else
		{
			if(this.getAdjList().get(currentNode)==null) return false;
			
			for(int i=0;i<this.getAdjList().get(currentNode).size();i++)
			{
				
				if(!visited.contains(this.getAdjList().get(currentNode).get(i).name) && (this.getAdjList().get(currentNode).get(i).getRelationaName().contains("type") || this.getAdjList().get(currentNode).get(i).getRelationaName().contains("Type")))
				{
					visited.add(this.getAdjList().get(currentNode).get(i).name);
					//Edge e = new Edge((float)0,this.getAdjList().get(currentNode).get(i));
					reachable = reachable | isReachable(query, this.getAdjList().get(currentNode).get(i).name, visited);
				}
			}
		}
		return reachable;
	}
	
	
	/**
	 * delete a node from the graph*/
	public void deleteNode(String node)
	{
		HashMap<String, ArrayList<String>> incidentList = this.getIncidentList();
		ArrayList<String> inList = incidentList.get(node);
		for(String concept:inList)
		{
			this.adjList.get(concept).remove((String)node);
		}
		incidentList.remove((String)node);
		this.adjList.remove((String)node);
	}
	
	
	
	public String stem(String input)
	{
		return PlingStemmer.stem(input);
		
	}

	public String removeDisambiguation(String input)
	{
		//System.out.println("input to remove disambiguation: "+input);
		input = input.trim();
		int i = input.lastIndexOf("(");
		int j=0;
		for(int k=i+1;k<input.length();k++)
		{
			if(input.charAt(k)==')') 
			{
				j=k;
				break;
			}
			
		}
		//int j = input.lastIndexOf(")");
		if(i>0 && j>i)
		{
			String output = input.replace(input.substring(i-1,j+1), "");
			//System.out.println("output after removing disambiguation: "+output);
			return output.trim();
		}
		else
			return input;
	}

	/**
	 * Givena string as input, this function checks for the presence of the non-disambiguated versions in the adjacency list
	 * @param input
	 * @return
	 */
	
	public String handleDisambiguation(String input)
	{
		
		if(this.getAdjList().get(input)==null)
		{
			if(this.getAdjList().get(removeDisambiguation(input))==null)
			{
				if(this.getAdjList().get(stem(removeDisambiguation(input)))==null)
				{
					return "";
				}
				else
				{
					return stem(removeDisambiguation(input));
				}
			}
			else
			{
				return removeDisambiguation(input);
			}
		}
		else return input;
	}
	
	/**
	 * given a disambiguation index and a non-disambiguated string as an input, returns the first disambiguated string from the index
	 * @param disamb
	 * @param input
	 * @return
	 */
	
	public String returnStringUsingDisambiguation(HashMap<String, ArrayList<String>> disamb, String input)
	{
		//BuildTree b = new BuildTree();
		ArrayList<String> l1 = disamb.get(input);
		if(l1==null) 
		{
			
			return "";
		}
		else
		{
			for(String s:l1)
			{
				//ArrayList<Edge> e1 = this.getAdjList().get(s);
				if(s!=null)
				{
					return s;
					
				}
				
			}
			return "";
		}
	}
	

	
	/**
	 * make a copy of the adjlist
	 * @return
	 */
	
	public AdjList makeCopy()
	{
		HashMap<String, ArrayList<Edge>> hh = new HashMap<String, ArrayList<Edge>>();
		for(String ss:this.getAdjList().keySet())
		{
			ArrayList<Edge> ee = this.getAdjList().get(ss);
			ArrayList<Edge> ee1 = new ArrayList<Edge>();
			for(Edge e:ee)
			{
				ee1.add(e);
			}
			hh.put(ss, ee1);
		}
		return new AdjList(hh);
	}
	


	/**
	 * retruns all paths of length 2 in a graph. Returns a list of list. Each list contains 3 nodes in the order of the path they represent
	 * @return
	 */
	
	public ArrayList<ArrayList<String>> return2HopPaths()
	{
		ArrayList<ArrayList<String>> ss = new ArrayList<ArrayList<String>>();
		
		for(String s:this.getAdjList().keySet())
		{
			
			ArrayList<Edge> e = this.getAdjList().get(s);
			for(Edge e1:e)
			{
				String d = e1.getName();
				ArrayList<Edge> ee = this.getAdjList().get(d);
				for(Edge e2:ee)
				{
					ArrayList<String> s2 = new ArrayList<String>();
					String f = e2.getName();
					s2.add(f);
					s2.add(d);
					s2.add(s);
					System.out.println(f+" "+d+" "+s);
					ss.add(s2);
				}
			}
		}
		return ss;
	}
	

	
	/**
	 * 
	 * @param h: hashmap that stores the weight of each edge
	 * @param e: list of wikipedia neighbours. for each pair in this list, h has to be updated by 1
	 */
	
	public void updateEdgeWeight(HashMap<String, HashMap<String, Float>> h, ArrayList<Edge> e)
	{
		ArrayList<String> ee = this.getListInString(e);
		Collections.sort(ee);
		for(int i=0;i<ee.size()-1;i++)
		{
			String e1 = ee.get(i);
			for(int j=i+1;j<ee.size();j++)
			{
				String e2 = ee.get(j);
				HashMap<String, Float> h1 = h.get(e1);
				HashMap<String, Float> h2 = h.get(e2);
				if(h1==null)
				{
					h1 = new HashMap<String, Float>();
				}
				if(h2==null)
				{
					h2 = new HashMap<String, Float>();
				}
				if(h1.get(e2)==null)
				{
					h1.put(e2, (float) 1);
				}
				else
				{
					double d = h1.get(e2);
					h1.put(e2, (float) d+1);
				}
				if(h2.get(e1)==null)
				{
					h2.put(e1, (float) 1.0);
				}
				else
				{
					double d = h2.get(e1);
					h2.put(e1, (float) d+1);
				}
				h.put(e1, h1);
				h.put(e2, h2);
			}
		}
	}
	

	/**
	 * return an AdjList where all triples participate in arelation named as "name"
	 * @param name
	 * @return
	 * @throws Exception
	 */
	
	public AdjList returnTriplesOfName(String name) throws Exception
	{
		ReadSubgraph rr = new ReadSubgraph();
		BufferedWriter bw = new BufferedWriter(new FileWriter("/home/prajna/workspace/"+name+".tsv"));

		for(String a1:this.getAdjList().keySet())
		{
			for(Edge s:this.getAdjList().get(a1))
			{
				if(s.getRelationaName().equals(name) || s.getRelationaName().equals(name.toLowerCase()))
				{
					bw.write(a1+"\t"+name+"\t"+s.name+"\n");
				}
			}
		}
		bw.close();
		AdjList aa = rr.readKB(new BufferedReader(new FileReader("/home/prajna/workspace/"+name+".tsv")));
		return aa;
	}
	/**
	 * given a graph kb and a relation "relation", this generates meta-paths for the relation of length "length". The paths are written in "outfile"
	 * @param relation
	 * @param outfile
	 * @param kb1: knowledge graph from where the triples for relation described by "relation" are extracted
	 * @param kb2: knowledge graph where we search for meta-paths between the entities involved in relation described by "relation" in kb1
	 * @param length
	 * @throws Exception
	 */
	
	public static void generateMetaPathsOfLengthMaster(String relation, String outfile, String kb1, String kb2, int length) throws Exception
	{
		BufferedWriter bw = new BufferedWriter(new FileWriter(outfile));
		ReadSubgraph rr = new ReadSubgraph();
		AdjList a = rr.readKB(new BufferedReader(new FileReader(kb1)));
		AdjList app = a.returnTriplesOfName(relation);
		AdjList a1 = rr.readKB(new BufferedReader(new FileReader(kb2)));
		//AdjList a1 = aa.makeCopy();
		/*System.out.println("number of edges before "+a1.numberOfEdges());
		for(String s:a.getAdjList().keySet())
		{
			ArrayList<Edge> e = a.getAdjList().get(s);
			for(Edge ee:e)
			{
				if(ee.getRelationaName().equals("owl:sameas") || ee.getRelationaName().equals("owl#sameas")) continue;
				Edge ee1 = new Edge(0,s,ee.getRelationaName()+"_inverse");
				a1.getAdjList().get(ee.getName()).add(ee1);
			}
		}*/
		
		/*BufferedWriter bw2 = new BufferedWriter(new FileWriter("/home/prajna/tkb_with_inverses"));
		a1.writeToFile(bw2);
		bw2.close();
		*/
		System.out.println("number of edges after "+a1.numberOfEdges());
		ArrayList<Edge> eee = new ArrayList<Edge>();
		System.out.println("number of edges in: "+app.numberOfEdges());
		for(String aa1:app.getAdjList().keySet())
		{
			if(a1.getAdjList().get(aa1)==null) continue;
			ArrayList<Edge> e1 = app.getAdjList().get(aa1);
			for(Edge e:e1)
			{
				if(a1.getAdjList().get(e.getName())==null) continue;
				a1.allKHopPaths(aa1, e.getName(),length, bw);
			}
		}
		bw.close();
		BufferedReader br = new BufferedReader(new FileReader(outfile));
		BufferedWriter bw1 = new BufferedWriter(new FileWriter(outfile.replace(".tsv", "")+"_with_counts.tsv"));
		String line;
		HashMap<String, Integer> meta_paths = new HashMap<String, Integer>();
		
		while((line=br.readLine())!=null)
		{
			StringTokenizer tok = new StringTokenizer(line,"\t");
			ArrayList<String> list1 = new ArrayList<String>();
			while(tok.hasMoreTokens())
			{
				list1.add(tok.nextToken());
			}
			String l = "";
			for(int k=list1.size()-2;k>=1;k=k-2)
			{
				l = l + list1.get(k)+"\t";
			}
			if(meta_paths.get(l)!=null)
			{
				meta_paths.put(l, meta_paths.get(l)+1);
			}
			else
			{
				meta_paths.put(l, 1);
			}
		}
		ArrayList<Entry<String, Integer>> aa1 = new ArrayList<Entry<String, Integer>>(meta_paths.entrySet());
		Collections.sort(aa1,valueComparator);
		for(Entry<String, Integer> dd:aa1)
		{
			bw1.write(dd.getKey()+dd.getValue()+"\n");
		}
		bw1.close();
	}
	
	/**
	 * generates meta-paths of length 2 
	 * @param relation
	 * @param outfile
	 * @param kb
	 * @throws Exception
	 */
	
	public static void generateMetaPaths(String relation, String outfile, String kb) throws Exception
	{
		ReadSubgraph rr = new ReadSubgraph();
		AdjList a = rr.readKB(new BufferedReader(new FileReader(kb)));
		AdjList app = a.returnTriplesOfName(relation);
		System.out.println("application size: "+app.getAdjList().size());
		BufferedWriter bw = new BufferedWriter(new FileWriter(outfile));
		HashMap<String, ArrayList<Edge>> incidentlist = a.getIncidentListEdgeType();
		AdjList ilist = new AdjList(incidentlist);
		HashMap<String, HashMap<String, ArrayList<String>>> incRelType = ilist.returnRelList();
		System.out.println(incRelType.size());
		HashMap<String, HashMap<String, Integer>> counts = new HashMap<String, HashMap<String, Integer>>();
		for(String aa:app.getAdjList().keySet())
		{
			int innum = a.getAdjList().get(aa).size() + incidentlist.get(aa).size();
			for(Edge e:app.getAdjList().get(aa))
			{
				String c = e.getName();
				//System.out.println(a.getAdjList().get(c));
				//System.out.println(incidentlist.get(c));
				int outnum = a.getAdjList().get(c).size() + incidentlist.get(c).size();
				String temp1=aa;
				String temp2=c;
				/*if(innum<outnum)
				{
					temp1=aa;
					temp2=c;
				}
				else
				{
					temp1=c;
					temp2=aa;
				}*/
				
				for(Edge e2:a.getAdjList().get(temp1))
				{
					if(e2.getName().equals(temp2)) continue;
					//System.out.println("here is: "+e2.getName());
					if(a.getRelTypes().get(e2.getName()).get(temp2)!=null)
					{
						//System.out.println("not null");
						for(String r:a.getRelTypes().get(e2.getName()).get(temp2))
						{
							//System.out.println("found");
							if(counts.get(e2.getRelationaName())==null)
							{
								counts.put(e2.getRelationaName(), new HashMap<String, Integer>());
							}
							HashMap<String, Integer> counts1 = counts.get(e2.getRelationaName());
							if(counts1.get(r)==null)
							{
								counts1.put(r, 1);
							}
							else
							{
								int c1 = counts1.get(r);
								counts1.put(r, c1+1);
							}
							counts.put(e2.getRelationaName(), counts1);
							bw.write(temp1+"\t"+e2.getRelationaName()+"\t"+e2.getName()+"\t"+r+"\t"+temp2+"\n");
						}
					}
					if(incRelType.get(e2.getName()).get(temp2)!=null)
					{
						for(String r:incRelType.get(e2.getName()).get(temp2))
						{
							if(counts.get(e2.getRelationaName())==null)
							{
								counts.put(e2.getRelationaName(), new HashMap<String, Integer>());
							}
							HashMap<String, Integer> counts1 = counts.get(e2.getRelationaName());
							if(counts1.get(r+"_inverse")==null)
							{
								counts1.put(r+"_inverse", 1);
							}
							else
							{
								int c1 = counts1.get(r+"_inverse");
								counts1.put(r+"_inverse", c1+1);
							}
							counts.put(e2.getRelationaName(), counts1);
							//System.out.println("found");
							
							bw.write(temp1+"\t"+e2.getRelationaName()+"\t"+e2.getName()+"\t"+r+"_inverse\t"+temp2+"\n");
						}
					}
				}
				
				for(Edge e2:ilist.getAdjList().get(temp1))
				{
					//if(e2.getName().equals(temp2)) continue;
					//System.out.println("here is: "+e2.getName());
					if(incRelType.get(e2.getName()).get(temp2)!=null)
					{
						//System.out.println("not null");
						for(String r:incRelType.get(e2.getName()).get(temp2))
						{
							if(counts.get(e2.getRelationaName()+"_inverse")==null)
							{
								counts.put(e2.getRelationaName()+"_inverse", new HashMap<String, Integer>());
							}
							HashMap<String, Integer> counts1 = counts.get(e2.getRelationaName()+"_inverse");
							if(counts1.get(r+"_inverse")==null)
							{
								counts1.put(r+"_inverse", 1);
							}
							else
							{
								int c1 = counts1.get(r+"_inverse");
								counts1.put(r+"_inverse", c1+1);
							}
							counts.put(e2.getRelationaName()+"_inverse", counts1);
							//System.out.println("found");
							bw.write(temp1+"\t"+e2.getRelationaName()+"_inverse\t"+e2.getName()+"\t"+r+"_inverse\t"+temp2+"\n");
						}
					}
					if(a.getRelTypes().get(e2.getName()).get(temp2)!=null)
					{
						for(String r:a.getRelTypes().get(e2.getName()).get(temp2))
						{
							if(counts.get(e2.getRelationaName()+"_inverse")==null)
							{
								counts.put(e2.getRelationaName()+"_inverse", new HashMap<String, Integer>());
							}
							HashMap<String, Integer> counts1 = counts.get(e2.getRelationaName()+"_inverse");
							if(counts1.get(r)==null)
							{
								counts1.put(r, 1);
							}
							else
							{
								int c1 = counts1.get(r);
								counts1.put(r, c1+1);
							}
							counts.put(e2.getRelationaName()+"_inverse", counts1);
							//System.out.println("found");
							bw.write(temp1+"\t"+e2.getRelationaName()+"_inverse\t"+e2.getName()+"\t"+r+"\t"+temp2+"\n");
						}
					}
				}
				
			}
		}
		BufferedWriter bw1 = new BufferedWriter(new FileWriter(outfile.replace(".tsv", "")+"_with_counts.tsv"));
		HashMap<String, Integer> hm = new HashMap<String, Integer>();
		
		ArrayList<Entry<String, HashMap<String, Integer>>> ss = new ArrayList<Entry<String, HashMap<String, Integer>>>(counts.entrySet());
		
		for(String dd:counts.keySet())
		{
			for(String dd1:counts.get(dd).keySet())
			{
				//bw1.write(dd+"\t"+dd1+"\t"+counts.get(dd).get(dd1)+"\n");
				hm.put(dd+"\t"+dd1, counts.get(dd).get(dd1));
			}
		}
		ArrayList<Entry<String, Integer>> aa = new ArrayList<Entry<String, Integer>>(hm.entrySet());
		Collections.sort(aa,valueComparator);
		for(Entry<String, Integer> dd:aa)
		{
			bw1.write(dd.getKey()+"\t"+dd.getValue()+"\n");
		}
		bw1.close();
		bw.close();
	}
	
	/**
	 * returns a hashmap of entity to entity with all the relations that they participate in
	 * @return
	 */
	
	public HashMap<String, HashMap<String, ArrayList<String>>> returnRelList()
	{
		HashMap<String, HashMap<String, ArrayList<String>>> relType = new HashMap<String, HashMap<String, ArrayList<String>>>();
		for(String a:this.getAdjList().keySet())
		{
			for(Edge e:this.getAdjList().get(a))
			{
				String d = e.getName();
				String b = e.getRelationaName();
				if(relType.get(a)!=null)
				{
					HashMap<String, ArrayList<String>> hm1 = relType.get(a);
					if(hm1.get(d)!=null)
					{
						ArrayList<String> relList = (ArrayList<String>) hm1.get(d);
						if(!relList.contains(b))
							relList.add(b);
						hm1.put(d, relList);
					}
					else
					{
						ArrayList<String> relList = new ArrayList<String>();
						relList.add(b);
						hm1.put(d, relList);
					}
					//hm1.put(d, b);
					relType.put(a, hm1);
				}
				else if(relType.get(a)==null)
				{
					HashMap<String, ArrayList<String>> hm1 = new HashMap<String, ArrayList<String>>();
					ArrayList<String> relList  = new ArrayList<String>();
					relList.add(b);
					hm1.put(d, relList);
					relType.put(a, hm1);
				}
				if(relType.get(d)==null) relType.put(d, new HashMap<String, ArrayList<String>>());
			}
		}
		this.relType = relType;
		return relType;
	}
	
	public static void addTriples(String kb1, String kb2, String file) throws Exception
	{
		ReadSubgraph rr = new ReadSubgraph();
		AdjList a1 = rr.readKB(new BufferedReader(new FileReader(kb1)));
		AdjList a2 = rr.readKB(new BufferedReader(new FileReader(kb2)));
		a1.mergeAdjacencyList(a2,file);
		
		
	}
	
}
