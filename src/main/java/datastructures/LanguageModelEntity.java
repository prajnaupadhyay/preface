package datastructures;

//import preprocess.Preprocess.Edge;

import java.io.*;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.*;
import java.util.Arrays;
import java.util.Map.Entry;

import com.aliasi.cluster.*;
import com.aliasi.util.*;
import com.aliasi.features.*;

import java.lang.Math;
import java.net.URLDecoder;
import java.net.URLEncoder;



/**
 * Class to represent an entity as a vector of entities and relations, based on how close are they to them
 * @author prajna
 *
 */

public class LanguageModelEntity {
	
	 String entity;
	 HashMap<String, Double> unigram;
	 HashMap<String, HashMap<String, Double>> bigram;
	 HashMap<String, Double> mixture;
	 AdjList newAdjList;
	static AdjList a;
	static HashMap<String, HashMap<String, String>> er;
	static HashMap<String, HashMap<String, String>> re;
	static HashMap<String, String> unigramO;
	public static Comparator<Entry<String, Double>> valueComparator = new Comparator<Entry<String, Double>>() 
	{
		 public int compare(Entry<String, Double> e1, Entry<String, Double> e2)
		 {
			 Double v1 = e1.getValue();
			 Double v2 = e2.getValue();
			 return v2.compareTo(v1);
		 }
	};
	
	public static Comparator<Entry<String, Double>> valueComparatorIncreasing = new Comparator<Entry<String, Double>>() 
	{
		 public int compare(Entry<String, Double> e1, Entry<String, Double> e2)
		 {
			 Double v1 = e1.getValue();
			 Double v2 = e2.getValue();
			 return v1.compareTo(v2);
		 }
	};
	
	public static Comparator<Entry<Set<String>, Double>> valueComparatorSetIncreasing = new Comparator<Entry<Set<String>, Double>>() 
	{
		 public int compare(Entry<Set<String>, Double> e1, Entry<Set<String>, Double> e2)
		 {
			 Double v1 = e1.getValue();
			 Double v2 = e2.getValue();
			 return v1.compareTo(v2);
		 }
	};
	
	
	public static Comparator<Entry<Set<LanguageModelEntity>, Double>> valueComparatorSet = new Comparator<Entry<Set<LanguageModelEntity>, Double>>() 
	{
		 public int compare(Entry<Set<LanguageModelEntity>, Double> e1, Entry<Set<LanguageModelEntity>, Double> e2)
		 {
			 Double v1 = e1.getValue();
			 Double v2 = e2.getValue();
			 return v1.compareTo(v2);
		 }
	};
	
	public static Comparator<Entry<LanguageModelEntity, Double>> valueComparatorEntity = new Comparator<Entry<LanguageModelEntity, Double>>() 
	{
		 public int compare(Entry<LanguageModelEntity, Double> e1, Entry<LanguageModelEntity, Double> e2)
		 {
			 Double v1 = e1.getValue();
			 Double v2 = e2.getValue();
			 return v1.compareTo(v2);
		 }
	};
	
	public static Comparator<Entry<ArrayList<String>, Double>> valueComparatorList = new Comparator<Entry<ArrayList<String>, Double>>() 
	{
		 public int compare(Entry<ArrayList<String>, Double> e1, Entry<ArrayList<String>, Double> e2)
		 {
			 Double v1 = e1.getValue();
			 Double v2 = e2.getValue();
			 return v2.compareTo(v1);
		 }
	};
			
	static Distance<LanguageModelEntity> COSINE_DISTANCE = new Distance<LanguageModelEntity>() 
	{
		public double distance(LanguageModelEntity lm1, LanguageModelEntity lm2) 
		{
			HashMap<String, Double> s1 = lm1.mixture;
			HashMap<String, Double> s2 = lm2.mixture;
			double diff =0.0;
			for(String aaa:s1.keySet())
			{
				double val1 = s1.get(aaa);
				double val2 = s2.get(aaa);
				diff = diff + (val1 - val2)*(val1 - val2);
			}
			double dist = Math.sqrt(diff);
			//System.out.println(dist);
			return dist;
		}
	};
	
	public LanguageModelEntity(String entity, AdjList a, HashMap<String, Double> unigram, HashMap<String, HashMap<String, Double>> bigram, HashMap<String, Double> mixture)
	{
		this.entity = entity;
		LanguageModelEntity.a = a;
		this.unigram = unigram;
		this.bigram = bigram;
		this.mixture = mixture;
	}
	
	public LanguageModelEntity()
	{
		
	}
	
	public void setMixture(HashMap<String, Double> mixture)
	{
		this.mixture = mixture;
	}
	
	public HashMap<String, Double> getMixture()
	{
		return this.mixture;
	}
	
	public String getName()
	{
		return this.entity;
	}
	/**
	 * normalizes the vector
	 */
	
	public void normalize()
	{
		double mag = this.magnitude();
		
		HashMap<String, Double> mixtureNormalized = new HashMap<String, Double>();
		if(mag!=0)
		{
			for(String s:this.mixture.keySet())
			{
				mixtureNormalized.put(s, mixture.get(s)/mag);
			}
		}
		else
		{
			for(String s:this.mixture.keySet())
			{
				mixtureNormalized.put(s, 0.0);
			}
		}
		this.mixture = mixtureNormalized;
	}
	
	/**
	 * set the entity of this language model
	 * @param entity
	 */
	
	public void setEntity(String entity)
	{
		this.entity = entity;
	}
	
	public HashMap<String, Double> estimateProbabilityUnigram(AdjList newAdjList)
	{
		HashMap<String,Double> lm = new HashMap<String, Double>();
		HashMap<String, Double> wordFreq = new HashMap<String, Double>();
		HashMap<String, ArrayList<String>> incidentList = newAdjList.getIncidentList();
		double total=0;
		System.out.println("estimating unigram probability");
		for(String f:unigramO.keySet())
		{
			ArrayList<Edge> n = newAdjList.getAdjList().get(f);
			ArrayList<String> n1 = incidentList.get(f);
			if(n!=null && n1!=null)
			{
				lm.put(f, (n.size()+n1.size())*1.0);
				total = total + n.size()+n1.size();
			}
			else if(n!=null && n1==null)
			{
				lm.put(f, (n.size())*1.0);
				total = total +  (n.size())*1.0;
			}
			else if(n==null && n1!=null)
			{
				lm.put(f, (n1.size())*1.0);
				total = total + (n1.size())*1.0;
			}
			else if(n==null && n1==null)
			{
				lm.put(f,0.0);	
			}
			
		}
		for(String f:unigramO.keySet())
		{
			wordFreq.put(f, (double) (lm.get(f)/total));
			
		}
		return wordFreq;
	}
	
	
	public HashMap<String, HashMap<String, Double>> estimateProbabilityBigram(AdjList newAdjList) throws Exception
	{
		HashMap<String,HashMap<String, Double>> lm = new HashMap<String,HashMap<String, Double>>();
		HashMap<String,HashMap<String, Double>> wordFreq = new HashMap<String,HashMap<String, Double>>();
		HashMap<String, ArrayList<String>> incidentList = newAdjList.getIncidentList();
		HashMap<String, HashMap<String, ArrayList<String>>> relTypes = newAdjList.getRelTypes();
		System.out.println("estimating bigram probability ");
		double total=0;
		
		for(String f:er.keySet())
		{
			ArrayList<Edge> n = newAdjList.getAdjList().get(f);
			if(n!=null)
			{
				double cc = 0;
				for(Edge ee:n)
				{
						HashMap<String, Double> hh = lm.get(f);
						if(hh==null)
						{
							hh = new HashMap<String, Double>();
							hh.put(ee.getRelationaName(), 1.0);
							lm.put(f, hh);
							total = total+1;
						}
						else
						{
							if(hh.get(ee.getRelationaName())!=null)
							{
								cc = hh.get(ee.getRelationaName());
								hh.put(ee.getRelationaName(), cc+1);
								lm.put(f, hh);
								total = total + 1;
							}
							else
							{
								hh.put(ee.getRelationaName(), 1.0);
								lm.put(f, hh);
								total = total + 1;
							}
						}
						
					
				}
			}
			
		}
		for(String f:re.keySet())
		{
			ArrayList<String> n1 = incidentList.get(f);
			
			if(n1!=null)
			{
				
				double cc =0;
				for(String ee:n1)
				{
					if(relTypes.get(ee)!=null)
					{
						ArrayList<String> rels = relTypes.get(ee).get(f);
						if(rels!=null)
						{
							for(String r:rels)
							{
								HashMap<String, Double> hh = lm.get(f);
								if(hh==null)
								{
									hh = new HashMap<String, Double>();
									
									hh.put(r, 1.0);
									lm.put(f, hh);
									total = total+1;
								}
								else
								{
									if(hh.get(r)!=null)
									{
										cc = hh.get(r);
										hh.put(r, cc+1);
										lm.put(f, hh);
										total = total + 1;
									}
									else
									{
										hh.put(r, 1.0);
										lm.put(f, hh);
										total = total + 1;
									}
								}
							}
							
						}
					}
				}
				
			}
			
		}
		for(String f:er.keySet())
		{
			HashMap<String, Double> nn;
			if(lm.get(f)==null)
			{
				nn = new HashMap<String, Double>();
			}
			else
			{
				nn = lm.get(f);
			}
			for(String g:er.get(f).keySet())
			{
				if(nn.get(g)==null)
				{
					nn.put(g, 0.0);
				}
			}
			lm.put(f, nn);
		}
		
		for(String f:re.keySet())
		{
			HashMap<String, Double> nn;
			if(lm.get(f)==null)
			{
				nn = new HashMap<String, Double>();
			}
			else
			{
				nn = lm.get(f);
			}
			for(String g:re.get(f).keySet())
			{
				if(nn.get(g)==null)
				{
					nn.put(g, 0.0);
				}
				
			}
			lm.put(f, nn);
		}
		
		for(String f:lm.keySet())
		{
			HashMap<String, Double> ww = new HashMap<String, Double>();
			HashMap<String, Double> hh = lm.get(f);
			for(String g:hh.keySet())
			{
				ww.put(g,hh.get(g)/total);
			}
			wordFreq.put(f, ww);
			
		}
		return wordFreq;
	}
	
	/**
	 * estimates the unigram probability
	 * @param entity
	 */
	
	public void estimateUnigramMaster() throws Exception
	{
		HashMap<String, Double> wordFreq1 = estimateProbabilityUnigram(newAdjList);
		//System.out.println("estimated probability of unigram for document");
		
		HashMap<String, Double> wordFreq2 = estimateProbabilityUnigram(a);
		//System.out.println("estimated probability of unigram for KB");
		
		HashMap<String, Double> wordFreq3 = new HashMap<String, Double>();
		
		for(String aa:wordFreq1.keySet())
		{
			wordFreq3.put(aa, 0.5*wordFreq1.get(aa)+0.5*wordFreq2.get(aa));
		}
		this.unigram = wordFreq3;
	}
	
	public static void initilizeUnigramAndBigram() 
	{
		unigramO = new HashMap<String, String>();
		for(String aa:a.getAdjList().keySet())
		{
			unigramO.put(aa,"");
		}
		
		er = new HashMap<String, HashMap<String, String>>();
		re = new HashMap<String, HashMap<String, String>>();
		
		
		for(String aa:a.getAdjList().keySet())
		{
			HashMap<String, String> kk;
			if(er.get(aa)==null)
			{
				kk = new HashMap<String, String>();
			}
			else
			{
				kk = er.get(aa);
			}
			for(Edge ee:a.getAdjList().get(aa))
			{
				String g = ee.getRelationaName();
				if(kk.get(g)==null)
				{
					kk.put(g, "");
					
				}
				
			}
			er.put(aa, kk);
			//System.out.println("starting to traverse graph");
			
		}
		//System.out.println(er.size());
		
		HashMap<String, ArrayList<String>> incidentList = a.getIncidentList();
		//System.out.println(incidentList.size());
		for(String aa:incidentList.keySet())
		{
			for(String bb:incidentList.get(aa))
			{
				ArrayList<String> rel = a.getRelTypes().get(bb).get(aa);
				HashMap<String, String> kk;
				if(re.get(aa)==null)
				{
					kk = new HashMap<String, String>();
				}
				else
				{
					kk = re.get(aa);
				}
				for(String cc:rel)
				{
					kk.put(cc, "");
				}
				re.put(aa, kk);
			}
			if(incidentList.get(aa).size()==0)
			{
				re.put(aa, new HashMap<String, String>());
			}
		}
	}
	
	/**
	 * estimates probability of bigrams
	 * @param entity
	 */
	
	public void estimateBigramMaster() throws Exception
	{
		
		HashMap<String, HashMap<String, Double>> wordFreq1 = estimateProbabilityBigram(newAdjList);
		
		HashMap<String, HashMap<String, Double>> wordFreq2 = estimateProbabilityBigram(a);
		HashMap<String, HashMap<String, Double>> wordFreq3 = new HashMap<String, HashMap<String, Double>>();
		
		//System.out.println("word freq1 size: "+wordFreq1.size());
		//System.out.println("word freq2 size: "+wordFreq2.size());
		
		for(String aa:wordFreq1.keySet())
		{
			
			HashMap<String, Double> w;
			if(wordFreq3.get(aa)==null)
			{
				w=new HashMap<String, Double>();
			}
			else
			{
				w=wordFreq3.get(aa);
			}
			HashMap<String, Double> ww1 = wordFreq1.get(aa);
			///System.out.println("ww1 size: "+ww1.size());
			HashMap<String, Double> ww2 = wordFreq2.get(aa);
			//System.out.println("ww2 size: "+ww2.size());
			for(String bb:ww1.keySet())
			{
				//System.out.println("aa is "+aa+", ww1.get("+bb+") is "+ww1.get(bb));
				//System.out.println("ww2.get("+bb+") is "+ww2.get(bb));
				double val1 = 0.5*ww1.get(bb);
				double val2 = 0.5*ww2.get(bb);
				w.put(bb, val1+val2);
			}
			wordFreq3.put(aa, w);
			
		}
		this.bigram = wordFreq3;
		//System.out.println("wordFreq3 size: "+wordFreq3.size());
	}
	
	/**
	 * mixes unigram and bigram distributions
	 */
	
	public void mix()
	{
		HashMap<String, Double> mixture1 = new HashMap<String, Double>();
		HashMap<String, Double> mixture2 = new HashMap<String, Double>();
		HashMap<String, Double> mixture3 = new HashMap<String, Double>();
		//System.out.println("unigram size: "+unigram.size());
		//System.out.println("bigram size: "+bigram.size());
		for(String a:unigram.keySet())
		{
			if(mixture1.get(a)==null)
			{
				mixture1.put(a, unigram.get(a));
			}
			else
			{
				double b = mixture1.get(a);
				mixture1.put(a, b+unigram.get(a));
			}
		}
		
		for(String a:bigram.keySet())
		{
			for(String b:bigram.get(a).keySet())
			{
				if(mixture2.get(a)==null)
				{
					mixture2.put(a, bigram.get(a).get(b));
				}
				else
				{
					double b1 = mixture2.get(a);
					mixture2.put(a, b1+bigram.get(a).get(b));
				}
			}
			if(bigram.get(a).size()==0)
			{
				mixture2.put(a, 0.0);
			}
		}
		
		//System.out.println("mixture1: "+mixture1.size());
		//System.out.println("mixture2: "+mixture2.size());

		for(String a:mixture1.keySet())
		{
			//System.out.println(a);
			mixture3.put(a,0.5*mixture1.get(a)+0.5*mixture2.get(a));
		}
		this.mixture = mixture3;
	}
	
	
	
	/**
	 * calculate euclidean distance between two language models
	 * @param e1
	 * @param e2
	 * @return
	 */
	
	public static Double distance(LanguageModelEntity e1, LanguageModelEntity e2)
	{
		HashMap<String, Double> h1 = e1.mixture;
		HashMap<String, Double> h2 = e2.mixture;
		double sum=0;
		for(String aa:h1.keySet())
		{
			sum = sum + Math.abs(h1.get(aa) - h2.get(aa));
		}
		return sum;
	}
	
	/**
	 * returns the dot product of the vectors described by LanguageModelEntity
	 * @param e1
	 * @return
	 */
	
	public Double dotProduct(LanguageModelEntity e1)
	{
		HashMap<String, Double> h1 = this.mixture;
		HashMap<String, Double> h2 = e1.mixture;
		double sum=0;
		for(String aa:h1.keySet())
		{
			sum = sum + (h1.get(aa) * h2.get(aa));
		}
		return sum;
	}
	
	/**
	 * returns cosine similarity between the entities
	 * @param e1
	 * @param e2
	 * @return
	 */
	
	public Double cosineSimilarity(LanguageModelEntity e1)
	{
		double sum = this.dotProduct(e1);
		double mag1 = this.magnitude();
		double mag2 = e1.magnitude();
		if(mag1*mag2==0) return 0.0;
		else return sum/(mag1*mag2);
	}
	
	/**
	 * adds the vector representations of 2 language model entities
	 * @param e1
	 * @return
	 */
	
	public LanguageModelEntity add(LanguageModelEntity e1)
	{
		if(this.mixture.size()==0) return e1;
		HashMap<String, Double> mixture1 = new HashMap<String, Double>();
		for(int i=0;i<128;i++)
		{
			double sum = e1.mixture.get("e"+i)+this.mixture.get("e"+i);
			mixture1.put("e"+i, sum);
		}
		LanguageModelEntity ll1 = new LanguageModelEntity();
		ll1.mixture=mixture1;
		return ll1;
		
	}
	
	public LanguageModelEntity scale(int scale)
	{
		double scale_1 = scale;
		if(this.mixture.size()==0) return this;
		HashMap<String, Double> mixture1 = new HashMap<String, Double>();
		for(int i=0;i<128;i++)
		{
			
			double new_val = this.mixture.get("e"+i)/scale;
			mixture1.put("e"+i, new_val);
		}
		LanguageModelEntity ll1 = new LanguageModelEntity();
		ll1.mixture=mixture1;
		return ll1;
		
	}
	
	
	
	/**
	 * returns the magnitude of the vector describing the LanguageModelEntity
	 * @param e1
	 * @return
	 */
	
	public  double magnitude() 
	{
		// TODO Auto-generated method stub
		double sum=0;
		for(String aa:this.mixture.keySet())
		{
			sum = sum + (this.mixture.get(aa)*this.mixture.get(aa));
		}
		return Math.sqrt(sum);
	}
	
	/**
	 * joins the mixture vectors of 2 language model entities
	 * @param ll
	 * @return
	 */
	
	public LanguageModelEntity join(LanguageModelEntity ll) {
		// TODO Auto-generated method stub
		HashMap<String, Double> mixture1 = new HashMap<String, Double>();
		int d=0;
		for(String s:this.mixture.keySet())
		{
			mixture1.put(s, this.mixture.get(s));
			d++;
		}
		/*for(String s:ll.mixture.keySet())
		{
			mixture1.put("e"+d, ll.mixture.get(s));
			d++;
		}*/
		int c=0;
		while(d<256)
		{
			mixture1.put("e"+d,ll.mixture.get("e"+c));
			c++;
			d++;
		}
		LanguageModelEntity ll1 = new LanguageModelEntity();
		ll1.mixture=mixture1;
		return ll1;
	}

	/**
	 * write the mixture vector to file specified by bw
	 * @param bw
	 * @throws Exception
	 */
	
	public void writeToFile(BufferedWriter bw, String s1, HashMap<String, String> stringToId1) throws Exception
	{
		bw.write("\n"+stringToId1.get(s1));
		for(String s:this.mixture.keySet())
		{
			if(!s.equals("e255"))
				bw.write(" "+this.mixture.get(s));
			else
				bw.write(" "+this.mixture.get(s));
		}
	}
	
	/**
	 * for every entity, returns the k-most similar siblings. Those that do not have any siblings return the most similar among all the other entities
	 * @param k
	 * @param s
	 * @param a
	 * @return
	 */
	
	public ArrayList<String> KMostSimiliarSiblings(int k, HashMap<String, LanguageModelEntity> s, AdjList a)
	{
		HashMap<String, Double> distances = new HashMap<String, Double>();
		ArrayList<String> siblings = a.extractSiblings(this.entity);
		if(siblings.size()==0)
		{
			siblings = new ArrayList<String>(s.keySet());
		}
		for(String s1:siblings)
		{
			LanguageModelEntity l = s.get(s1);
			if(l==null) continue;
			double sim = this.cosineSimilarity(l);
			distances.put(s1, sim);
		}
		
		Set<Entry<String, Double>> entries = distances.entrySet();
		List<Entry<String, Double>> listOfEntries1 = new ArrayList<Entry<String, Double>>(entries);
	
		Comparator<Entry<String, Double>> valueComparator = new Comparator<Entry<String, Double>>() 
		{
			 public int compare(Entry<String, Double> e1, Entry<String, Double> e2)
			 {
				 Double v1 = e1.getValue();
				 Double v2 = e2.getValue();
				 return v2.compareTo(v1);
			 }
		};
		Collections.sort(listOfEntries1, valueComparator);
		ArrayList<String> result = new ArrayList<String>();
		int c=0;
		HashSet<String> h1 = new HashSet<String>();
		for(Entry e:listOfEntries1)
		{
			//h1.add(b.stem(b.removeDisambiguation((String)e.getKey())));
			//System.out.println(this.entity+"\t"+e.getKey()+"\t"+e.getValue());
			result.add((String)e.getKey());
			c++;
			if(c==10) break;
			//if(h1.size()==10) break;
		}
		//return new ArrayList<String>(h1);
		return result;
		
	}
	
	/**
	 * Given a size k, it returns the k-most similiar entities to the input strings is the hashmap containing the embeddings
	 * @param k
	 * @param s
	 * @return
	 */
	
	public ArrayList<String> KMostSimiliar(int k, HashMap<String, LanguageModelEntity> s)
	{
		HashMap<String, Double> distances = new HashMap<String, Double>();
		for(String s1:s.keySet())
		{
			LanguageModelEntity l = s.get(s1);
			if(l==null) continue;
			double sim = this.cosineSimilarity(l);
			distances.put(s1, sim);
			
		}
		Set<Entry<String, Double>> entries = distances.entrySet();
		List<Entry<String, Double>> listOfEntries1 = new ArrayList<Entry<String, Double>>(entries);
	
		Comparator<Entry<String, Double>> valueComparator = new Comparator<Entry<String, Double>>() 
		{
			 public int compare(Entry<String, Double> e1, Entry<String, Double> e2)
			 {
				 Double v1 = e1.getValue();
				 Double v2 = e2.getValue();
				 return v2.compareTo(v1);
			 }
		};
		Collections.sort(listOfEntries1, valueComparator);
		ArrayList<String> result = new ArrayList<String>();
		int c=0;
		HashSet<String> h1 = new HashSet<String>();
		for(Entry e:listOfEntries1)
		{
			//h1.add(b.stem(b.removeDisambiguation((String)e.getKey())));
			//System.out.println(this.entity+"\t"+e.getKey()+"\t"+e.getValue());
			result.add((String)e.getKey());
			c++;
			if(c==10) break;
			//if(h1.size()==10) break;
		}
		//return new ArrayList<String>(h1);
		return result;
		
	}
	
	/**
	 * directly reads the embedding file instead of trying to map the entities to an entity list
	 * @param f
	 * @param h
	 * @return
	 * @throws Exception
	 */
	
	public static HashMap<String, LanguageModelEntity> readEmbeddingsFromFileAlternate(String f) throws IOException
	{
		BufferedReader br = new BufferedReader(new FileReader(f));
		HashMap<String, LanguageModelEntity> s = new HashMap<String, LanguageModelEntity>();
		String line;
		int c=0;
		while((line=br.readLine())!=null)
		{
			//System.out.println(line);
			if(c==0) 
			{
				c++;
				continue;
			}
			else
			{
				c++;
				StringTokenizer tok = new StringTokenizer(line," ");
				String cc = tok.nextToken();
			//	System.out.println(cc);
				String entity = cc;
			//	System.out.println(cc+"\t"+entity);
				LanguageModelEntity lm = new LanguageModelEntity(entity, a, new HashMap<String, Double>(), new HashMap<String, HashMap<String, Double>>(), new HashMap<String, Double>());
				int d = 0;
				while(tok.hasMoreTokens())
				{
					Double val = Double.parseDouble(tok.nextToken());
					lm.mixture.put("e"+d, val);
					d++;
					
				}
				//System.out.println(entity+" mixture size: "+lm.mixture.size());
				s.put(entity,lm);
			}
		}
		System.out.println("read embeddings size: "+s.size());
		return s;
	}
	
	/**
	 * reads the embeddings of all entities into a hashmap structure from a file
	 * @param f: file contains the embeddings
	 * @param h: hashmap of id to entity name
	 * @return
	 * @throws Exception
	 */
	
	public static HashMap<String, LanguageModelEntity> readEmbeddingsFromFile(String f, HashMap<String, String> h) throws Exception
	{
		BufferedReader br = new BufferedReader(new FileReader(f));
		HashMap<String, LanguageModelEntity> s = new HashMap<String, LanguageModelEntity>();
		String line;
		int c=0;
		while((line=br.readLine())!=null)
		{
			//System.out.println(line);
			if(c==0) 
			{
				c++;
				continue;
			}
			else
			{
				c++;
				StringTokenizer tok = new StringTokenizer(line," ");
				String cc = tok.nextToken();
			//	System.out.println(cc);
				String entity = h.get(cc);
			//	System.out.println(cc+"\t"+entity);
				LanguageModelEntity lm = new LanguageModelEntity(entity, a, new HashMap<String, Double>(), new HashMap<String, HashMap<String, Double>>(), new HashMap<String, Double>());
				int d = 0;
				while(tok.hasMoreTokens())
				{
					Double val = Double.parseDouble(tok.nextToken());
					lm.mixture.put("e"+d, val);
					d++;
					
				}
				//System.out.println(entity+" mixture size: "+lm.mixture.size());
				s.put(entity,lm);
			}
		}
		System.out.println("read embeddings size: "+s.size());
		return s;
	}
	
}
