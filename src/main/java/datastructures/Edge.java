/*Author: Prajna Upadhyay
 * Edge is a class type to represent the edge of a Wikipedia graph, consisting of the name of the destination node and the value (either refd or tf-idf value)*/

package datastructures;

public class Edge 
{
	float value;
	String name;
	String relName;
	public Edge(float value, String name)
	{
		this.value = value;
		this.name = name;
	}
	
	public Edge(float value, String name, String rel)
	{
		this.value = value;
		this.name = name;
		this.relName = rel;
	}
	
	public Edge()
	{
		//nothing
	}
	public String getName()
	{
		return this.name;
	}
	
	public void setName(String name)
	{
		this.name = name;
	}
	public float getValue()
	{
		return this.value;
	}
	public void setValue(float value)
	{
		this.value = value;
	}
	public String getRelationaName()
	{
		return relName;
	}
	public void setRelationName(String relName)
	{
		this.relName = relName;
	}
	
}
