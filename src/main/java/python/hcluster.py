import io
import os.path
import operator
import re
import numpy as np
import scipy.cluster.hierarchy

from math import exp
from math import sqrt
import fastcluster

from scipy import spatial

from datetime import datetime



class H_Clustering:
	def __init__(self, k = 10, tolerance = 0.0001, max_iterations = 100):
		self.k = k
		self.tolerance = tolerance
		self.max_iterations = max_iterations
		self.centroids=[None] * k
		self.classes={}
		dict_words={}
		
		f = open("/home/prajna/node2vec/node2vec/graph/tkb/mappings_nodes")
		for line in f:
			line = line[:-1]
			splitline = line.split("\t")
			dict_words[splitline[1]] = splitline[0]
		
		phrase_vec={}
		lines={}
		f.close()
		f = open("/home/prajna/node2vec/node2vec/emb/tkb_wlength_2.emd")
		line_number=0
		vec_of_vecs=[]
		for line in f:
			line = line[:-1]
			if line_number==0:
				line_number=line_number+1
				continue
			#elif line_number>=25000:
				#break
			else:
				line_number = line_number + 1
				splitline = line.split(" ")
				phrase = splitline[0]
				vec=[]
				for i in range(1, len(splitline)):
					vec.append(float(splitline[i]))
				phrase_vec[dict_words[phrase]] = vec
				
		
		print("read phrase embeddings")
		
		f = io.open("/home/prajna/faceted_prerequisites/query_wise_entities/pagerank")
		o1 = open("/home/prajna/faceted_prerequisites/hierarchical_clustering/output/clusters_tkb","w")
		o2 = open("/home/prajna/faceted_prerequisites/hierarchical_clustering/output/z_matrix_tkb","w")
		#o1 = open("lda/vec_to_string","w")
		#o2 = open("lda/words","w")
		
		index=0
		lines={}
		vec_of_vecs=[]
		for line in f:
			dict_line={}
			vec=[]
			line= line[:-1]
			print(line)
			#splitline = line.split(", ")
			text = line
			if text in phrase_vec:
				print("yes")
				vec_of_vecs.append(phrase_vec[text])
				lines[index]=text
				index = index + 1
		
		tot_len = len(lines)
		data=np.array([np.array(xi) for xi in vec_of_vecs])
		now = datetime.now()
		timestamp = datetime.timestamp(now)
		dt_object = datetime.fromtimestamp(timestamp)
		print("dt_object =", dt_object)
		
		print("read data")
		Z = fastcluster.linkage(data, method='single', metric='cosine', preserve_input='True')
		for z in Z:
			o2.write(str(z[0])+"\t"+str(z[1])+"\t"+str(z[2])+"\t"+str(z[3])+"\n")
		o2.close()
		clusters_stepwise={}
		index=0
		for z in Z:
			z1 = int(z[0])
			z2 = int(z[1])
			if(z1 in lines and z2 in lines):
				#print("assigned index: "+str(tot_len+index))
				lines[tot_len+index] = lines[z1]+"\t"+lines[z2]
			#else:
				#if z1 not in lines:
					#print(z1)
				#if z2 not in lines:
					#print(z2)
			index = index + 1
		for l in lines:
			if(l<tot_len):
				continue
			else:
				o1.write(lines[l]+"\n")
		o1.close()
		#Y = scipy.cluster.hierarchy.fcluster(Z, t=0.1, criterion='distance')
		
		#o1.write(str(Y))
		#o1.close()
		now = datetime.now()
		timestamp = datetime.timestamp(now)
		dt_object = datetime.fromtimestamp(timestamp)
		print("dt_object =", dt_object)
		#o1.write(str(phrase_vec))
		#o2.write(str(words_ordered))
		#o1.close()
		#o2.close()
		#centroids=["pagerank","algorithm","rank","graph","user","page","base","paper","network","result"]
		#centroidsLists = []
		'''
		for i in centroids:
			vec=[]
			for e in words_ordered:
				if e == i:
					vec.append(1)
				else:
					vec.append(0)
			centroidsLists.append(vec)
		C = np.array([np.array(xi) for xi in centroidsLists])
		
		#print(C)
		o = open("lda/pagerank_classes_random_centres","w")
		
		for i in range(self.k):
			self.centroids[i] = data[i]
		#print(self.centroids)
		isOptimal = True
		for i in range(self.max_iterations):
			print(i)
			self.classes = {}
			for i in range(self.k):
				self.classes[i] = []

		#find the distance between the point and cluster; choose the nearest centroid
			for features in data:
				distances = [spatial.distance.cosine(features, centroid) for centroid in self.centroids]
				#print(distances)
				classification = distances.index(min(distances))
				#print(classification)
				self.classes[classification].append(features)
			count=0
			dict_centroids=[]
			for c in self.centroids:
				dict_centroids.append((count,c))
				count=count+1
			previous = dict(dict_centroids)
			#print(previous)
			
			for classification in self.classes:
				self.centroids[classification] = np.average(self.classes[classification], axis = 0)
			for i in range(self.k):
				original_centroid = previous[i]
				curr = self.centroids[i]
				print(np.sum(curr - original_centroid))
				#if np.sum((curr - original_centroid)/original_centroid * 100.0) > self.tolerance:
					#isOptimal = False
			#if isOptimal:
				#break
		for e in self.classes:
			o.write("cluster "+str(e)+"\n")
			cluster = self.classes[e]
			for c in cluster:
				string_c=' '.join([str(elem) for elem in c])
				o.write(phrase_vec[string_c]+", ")
			o.write("\n\n")
		o.close()
		

	def _removeNonAscii(s): return "".join(i for i in s if ord(i)<128)

	def Euclidean_distance(self, feat_one, feat_two):
		squared_distance = 0
		for i in range(len(feat_one)):
			squared_distance += (feat_one[i] - feat_two[i])**2
		ed = sqrt(squared_distance)
		return ed;
	'''
now = datetime.now()
timestamp = datetime.timestamp(now)
dt_object = datetime.fromtimestamp(timestamp)
print("dt_object =", dt_object)
p = H_Clustering()
#obj=K_means()
#obj.__init__(self, k = 10, tolerance = 0.0001, max_iterations = 500)
