import io
import os.path
import operator
import re
import sys
import os

from math import exp
from rake_nltk import Metric, Rake

def _removeNonAscii(s): return "".join(i for i in s if ord(i)<128)


r = Rake()
dict1={}

#f = open("/media/kirtu/PhD/faceted_preq/queries/queries2")


directory = "/mnt/dell/Hemanth/iitd/research_papers_indexed/faceted_prerequisites/top_1000_results"
count=1
for filename in os.listdir(directory):
#for line in f:
	#line = line[:-1]
	print(count)
	count=count+1
	f1 = io.open(directory+"/"+filename)
	phrase_dict={}
	for line in f1:
		#print line
		line = line[:-1]
		line = _removeNonAscii(line)
		splitline = line.split("\t")
		#print len(splitline)
		if(len(splitline)>4 or len(splitline)<4):
			continue
		query = splitline[0]
		#print query
		if (query not in dict1):
			dict1[query]={}
		text = splitline[1]+". "+splitline[2]
		#text = text.lower().replace("http://"," ").replace("<strong>", "").replace("</strong>", "").replace("[","").replace("(","").replace(")","").replace(",","").replace(".","").replace("!","").replace("?","").replace(";","").replace(":","").replace("]", "").replace("%","")
		text = text.lower().replace("http://"," ").replace("<strong>", "").replace("</strong>", "").replace("[","").replace("(","").replace(")","").replace("]", "")

		#print ("earlier: "+text)
		text = re.sub('\d', "  ",text)
		#print ("later: "+text)
		#sentenceList = rake.split_sentences(text)
		r.extract_keywords_from_text(text)
		#print (r.get_ranked_phrases_with_scores())
		for e in r.get_ranked_phrases_with_scores():
			if (e[0]<5):
				continue
			splitline_e = e[1].split(" ")
			if(len(splitline_e)>5):
				continue
			score = exp(float(splitline[3]))
			score_updated = score*e[0]
			phrase = e[1]
			if(phrase in dict1[query]):
				phrase_dict[phrase]=dict1[query][phrase]+score_updated
			else:
				phrase_dict[phrase]=score_updated
	o = open("/mnt/dell/Hemanth/iitd/research_papers_indexed/faceted_prerequisites/phrases/"+filename,"w")
	sorted_x = sorted(phrase_dict.items(), key=operator.itemgetter(1), reverse=True)
	for ee in sorted_x:
		o.write(str(ee)+"\n")
	o.close()
