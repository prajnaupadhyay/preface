package facets;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.Statement;
import java.util.*;
import java.util.Map.Entry;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;
import java.io.*;
/** import statements for fastutil library
import it.unimi.dsi.fastutil.doubles.*;
import it.unimi.dsi.fastutil.ints.*;
import it.unimi.dsi.fastutil.chars.*;
import it.unimi.dsi.fastutil.objects.*;
import it.unimi.dsi.fastutil.*;*/

import com.google.common.graph.*;

import datastructures.AdjListCompactOld;
import main.java.org.ahocorasick.trie.Emit;
import main.java.org.ahocorasick.trie.Trie;
import datastructures.AdjList;
import datastructures.AdjListCompact;
import datastructures.Edge;
import datastructures.ReadSubgraph;

import datastructures.LanguageModelEntity;
import datastructures.UtilityFunctions;

public class Aspect {
	HashMap<String, ArrayList<String>> aspectNameToEntities;

	HashMap<String, HashMap<String, LanguageModelEntity>> aspectNameToEmbeddingSpace;

	static Comparator<Entry<String, Integer>> valueComparator = new Comparator<Entry<String, Integer>>() {
		public int compare(Entry<String, Integer> e1, Entry<String, Integer> e2) {
			Integer v1 = e1.getValue();
			Integer v2 = e2.getValue();
			return v2.compareTo(v1);
		}
	};
	static Comparator<Entry<String, Double>> doubleComparator = new Comparator<Entry<String, Double>>() {
		public int compare(Entry<String, Double> e1, Entry<String, Double> e2) {
			Double v1 = e1.getValue();
			Double v2 = e2.getValue();
			return v2.compareTo(v1);
		}
	};

	public Aspect() {

	}

	public Aspect(String algoEmbedding, String applicationEmbedding, String techniqueEmbedding, String impEmbedding,
			String entityMap) throws Exception {
		HashMap<String, ArrayList<String>> h1 = new HashMap<String, ArrayList<String>>();
		HashMap<String, HashMap<String, LanguageModelEntity>> a1 = new HashMap<String, HashMap<String, LanguageModelEntity>>();
		this.aspectNameToEntities = h1;
		UtilityFunctions u = new UtilityFunctions();
		HashMap<String, String> stringToId = u.createStringToIdMapping(entityMap);
		HashMap<String, LanguageModelEntity> emb1 = LanguageModelEntity.readEmbeddingsFromFile(algoEmbedding,
				stringToId);
		HashMap<String, LanguageModelEntity> emb2 = LanguageModelEntity.readEmbeddingsFromFile(applicationEmbedding,
				stringToId);
		HashMap<String, LanguageModelEntity> emb3 = LanguageModelEntity.readEmbeddingsFromFile(techniqueEmbedding,
				stringToId);
		HashMap<String, LanguageModelEntity> emb4 = LanguageModelEntity.readEmbeddingsFromFile(impEmbedding,
				stringToId);
		a1.put("algorithm", emb1);
		a1.put("application", emb2);
		a1.put("technique", emb3);
		a1.put("impl", emb4);
		this.aspectNameToEmbeddingSpace = a1;

	}

	

	/**
	 * reads the 'file' that lists the path sequence and its frequency. Returns a
	 * HashMap of String to Double after reading the file
	 * 
	 * @param file
	 * @return
	 * @throws Exception
	 */

	public static HashMap<String, Double> readFile(String file) throws Exception {
		HashMap<String, Double> counts = new HashMap<String, Double>();
		BufferedReader br = new BufferedReader(new FileReader(file));
		String line;
		while ((line = br.readLine()) != null) {
			StringTokenizer tok = new StringTokenizer(line, "\t");
			ArrayList<String> l = new ArrayList<String>();
			while (tok.hasMoreTokens()) {
				l.add(tok.nextToken());
			}
			String meta_path = "";
			for (int i = 0; i < l.size(); i++) {
				if (i == l.size() - 1) {

				} else {
					meta_path = meta_path + " " + l.get(i);
				}
			}
			counts.put(meta_path, Double.parseDouble(l.get(l.size() - 1)));

		}
		return counts;
	}

	public static void choose(String line, BufferedWriter bw, String app) throws Exception {
		StringTokenizer tok1 = new StringTokenizer(line, "\t");
		ArrayList<String> tokens1 = new ArrayList<String>();
		while (tok1.hasMoreTokens()) {
			tokens1.add(tok1.nextToken());
		}

		StringTokenizer tok = new StringTokenizer(tokens1.get(0), " ");
		ArrayList<String> tokens = new ArrayList<String>();
		while (tok.hasMoreTokens()) {
			tokens.add(tok.nextToken());
		}
		System.out.println(tokens.size());
		if ((tokens.get(1).contains(app) || tokens.get(1).contains(app)) && Double.parseDouble(tokens1.get(1)) > 0) {
			bw.write(tokens.get(0) + " " + tokens.get(1) + "\n");
		}
	}

	public static void chooseExtraPaths(String parent_folder, String file) throws Exception {
		BufferedReader br = new BufferedReader(new FileReader(file));
		BufferedWriter bw1 = new BufferedWriter(new FileWriter(parent_folder + "/application/selected_meta-paths"));
		BufferedWriter bw2 = new BufferedWriter(new FileWriter(parent_folder + "/algorithm/selected_meta-paths"));
		BufferedWriter bw3 = new BufferedWriter(new FileWriter(parent_folder + "/implementation/selected_meta-paths"));
		String line;
		int c = 0;
		int c2 = 0;
		while ((line = br.readLine()) != null) {
			c = c + 1;
			if (line.equals("\n"))
				continue;

			if (!line.startsWith(" ")) {
				if (line.equals("application")) {
					c2 = 1;
				} else if (line.equals("algorithm")) {
					c2 = 2;
				} else if (line.equals("implementation")) {
					c2 = 3;
				}
			} else if (line.startsWith(" ")) {
				if (c2 == 1) {
					choose(line, bw1, "applicationof");
				} else if (c2 == 2) {
					choose(line, bw2, "algorithmfor");
				} else if (c2 == 3) {
					choose(line, bw3, "implementationof");
				}

			}
		}
		bw1.close();
		bw2.close();
		bw3.close();

	}

	/**
	 * 
	 * @param app
	 * @param tech
	 * @param algo
	 * @param impl
	 * @throws Exception
	 */

	public static void extractImportantPaths(String app, String outfile) throws Exception {
		HashMap<String, Double> app_counts = readFile(app + "/meta-path-applicationof_length_2_with_counts.tsv");
		// HashMap<String, Double> tech_counts =
		// readFile(app+"/meta-path-techniquein_length_2_with_counts.tsv");
		HashMap<String, Double> algo_counts = readFile(app + "/meta-path-algorithmfor_length_2_with_counts.tsv");
		HashMap<String, Double> impl_counts = readFile(app + "/meta-path-implementationof_length_2_with_counts.tsv");
		HashSet<String> h1 = new HashSet<String>(app_counts.keySet());
		// h1.addAll(tech_counts.keySet());
		h1.addAll(algo_counts.keySet());
		h1.addAll(impl_counts.keySet());

		HashMap<String, Double> tf_idf_map_app = new HashMap<String, Double>();
		HashMap<String, Double> tf_idf_map_tech = new HashMap<String, Double>();
		HashMap<String, Double> tf_idf_map_algo = new HashMap<String, Double>();
		HashMap<String, Double> tf_idf_map_impl = new HashMap<String, Double>();
		for (String s : h1) {
			double tf1 = ((app_counts.get(s) == null) ? 0 : app_counts.get(s));
			double idf1 = Math.log(4 / (((app_counts.get(s) == null) ? 0 : 1) + ((algo_counts.get(s) == null) ? 0 : 1)
					+ ((impl_counts.get(s) == null) ? 0 : 1)));
			// double tf2 = (tech_counts.get(s)==null?0:tech_counts.get(s));

			double tf3 = (algo_counts.get(s) == null ? 0 : algo_counts.get(s));

			double tf4 = (impl_counts.get(s) == null ? 0 : impl_counts.get(s));

			if (tf1 != 0)
				tf_idf_map_app.put(s, tf1 * idf1);
			// if(tf2!=0) tf_idf_map_tech.put(s, tf2*idf1);
			if (tf3 != 0)
				tf_idf_map_algo.put(s, tf3 * idf1);
			if (tf4 != 0)
				tf_idf_map_impl.put(s, tf4 * idf1);
		}

		ArrayList<Entry<String, Double>> ee = new ArrayList<Entry<String, Double>>(tf_idf_map_app.entrySet());
		// ArrayList<Entry<String, Double>> ee1 = new ArrayList<Entry<String,
		// Double>>(tf_idf_map_tech.entrySet());
		ArrayList<Entry<String, Double>> ee2 = new ArrayList<Entry<String, Double>>(tf_idf_map_algo.entrySet());
		ArrayList<Entry<String, Double>> ee3 = new ArrayList<Entry<String, Double>>(tf_idf_map_impl.entrySet());

		Collections.sort(ee, doubleComparator);
		// Collections.sort(ee1, doubleComparator);
		Collections.sort(ee2, doubleComparator);
		Collections.sort(ee3, doubleComparator);

		BufferedWriter bw = new BufferedWriter(new FileWriter(outfile));
		bw.write("\n\napplication\n\n");
		for (Entry<String, Double> e : ee) {
			bw.write(e.getKey() + "\t" + e.getValue() + "\n");
		}
		/*
		 * bw.write("\n\ntechnique\n\n"); for(Entry<String, Double> e:ee1) {
		 * bw.write(e.getKey()+"\t"+e.getValue()+"\n"); }
		 */
		bw.write("\n\nalgorithm\n\n");
		for (Entry<String, Double> e : ee2) {
			bw.write(e.getKey() + "\t" + e.getValue() + "\n");
		}
		bw.write("\n\nimplementation\n\n");
		for (Entry<String, Double> e : ee3) {
			bw.write(e.getKey() + "\t" + e.getValue() + "\n");
		}
		bw.close();
		// chooseExtraPaths(hm.get("parent-folder1"),outfile);

	}

	/**
	 * 
	 * @param app
	 * @param tech
	 * @param algo
	 * @param impl
	 * @throws Exception
	 */

	public static void extractImportantPaths1(String app, String outfile) throws Exception {
		HashMap<String, Double> app_counts = readFile(app + "/meta-path-applicationof_length_2_with_counts.tsv");
		// HashMap<String, Double> tech_counts =
		// readFile(app+"/meta-path-techniquein_length_2_with_counts.tsv");
		HashMap<String, Double> algo_counts = readFile(app + "/meta-path-algorithmfor_length_2_with_counts.tsv");
		HashMap<String, Double> impl_counts = readFile(app + "/meta-path-implementationof_length_2_with_counts.tsv");
		HashSet<String> h1 = new HashSet<String>(app_counts.keySet());
		// h1.addAll(tech_counts.keySet());
		h1.addAll(algo_counts.keySet());
		h1.addAll(impl_counts.keySet());
		int app_count = 17175;
		int tech_count = 6402;
		int algo_count = 11994;
		int impl_count = 201958;

		HashMap<String, Double> tf_idf_map_app = new HashMap<String, Double>();
		// HashMap<String, Double> tf_idf_map_tech = new HashMap<String, Double>();
		HashMap<String, Double> tf_idf_map_algo = new HashMap<String, Double>();
		HashMap<String, Double> tf_idf_map_impl = new HashMap<String, Double>();
		for (String s : h1) {
			double tf1 = ((app_counts.get(s) == null) ? 0 : app_counts.get(s));
			double idf1 = 3 / (((app_counts.get(s) == null) ? 0 : (app_counts.get(s) / app_count))
					+ ((algo_counts.get(s) == null) ? 0 : (algo_counts.get(s) / algo_count))
					+ ((impl_counts.get(s) == null) ? 0 : (impl_counts.get(s) / impl_count)));
			// double tf2 = (tech_counts.get(s)==null?0:tech_counts.get(s));

			double tf3 = (algo_counts.get(s) == null ? 0 : algo_counts.get(s));

			double tf4 = (impl_counts.get(s) == null ? 0 : impl_counts.get(s));

			if (tf1 != 0)
				tf_idf_map_app.put(s, tf1 * idf1);
			// if(tf2!=0) tf_idf_map_tech.put(s, tf2*idf1);
			if (tf3 != 0)
				tf_idf_map_algo.put(s, tf3 * idf1);
			if (tf4 != 0)
				tf_idf_map_impl.put(s, tf4 * idf1);

			/*
			 * tf_idf_map_app.put(s, idf1); tf_idf_map_tech.put(s, idf1);
			 * tf_idf_map_algo.put(s, idf1); tf_idf_map_impl.put(s, idf1);
			 */
		}

		ArrayList<Entry<String, Double>> ee = new ArrayList<Entry<String, Double>>(tf_idf_map_app.entrySet());
		// ArrayList<Entry<String, Double>> ee1 = new ArrayList<Entry<String,
		// Double>>(tf_idf_map_tech.entrySet());
		ArrayList<Entry<String, Double>> ee2 = new ArrayList<Entry<String, Double>>(tf_idf_map_algo.entrySet());
		ArrayList<Entry<String, Double>> ee3 = new ArrayList<Entry<String, Double>>(tf_idf_map_impl.entrySet());

		Collections.sort(ee, doubleComparator);
		// Collections.sort(ee1, doubleComparator);
		Collections.sort(ee2, doubleComparator);
		Collections.sort(ee3, doubleComparator);

		BufferedWriter bw = new BufferedWriter(new FileWriter(outfile));
		bw.write("\n\napplication\n\n");
		for (Entry<String, Double> e : ee) {
			bw.write(e.getKey() + "\t" + e.getValue() + "\n");
		}
		/*
		 * bw.write("\n\ntechnique\n\n"); for(Entry<String, Double> e:ee1) {
		 * bw.write(e.getKey()+"\t"+e.getValue()+"\n"); }
		 */
		bw.write("\n\nalgorithm\n\n");
		for (Entry<String, Double> e : ee2) {
			bw.write(e.getKey() + "\t" + e.getValue() + "\n");
		}
		bw.write("\n\nimplementation\n\n");
		for (Entry<String, Double> e : ee3) {
			bw.write(e.getKey() + "\t" + e.getValue() + "\n");
		}
		bw.close();

	}

	/*
	 * public static void readGraphEfficient(String kb) throws Exception {
	 * BufferedReader br = new BufferedReader(new FileReader(kb));
	 * Object2ObjectLinkedOpenHashMap<String, ObjectArrayList<String>> h= new
	 * Object2ObjectLinkedOpenHashMap<String,
	 * ObjectArrayList<String>>(100000000,(float) 0.3);
	 * 
	 * String line; int count=0; while((line=br.readLine())!=null) { count++;
	 * StringTokenizer tok = new StringTokenizer(line,"\t"); ArrayList<String>
	 * tokens = new ArrayList<String>(); while(tok.hasMoreTokens()) {
	 * tokens.add(tok.nextToken()); } if(tokens.size()!=3) continue;
	 * ObjectArrayList<String> o; if(h.get(tokens.get(0))!=null) { o =
	 * h.get(tokens.get(0)); } else { o = new ObjectArrayList<String>(); }
	 * o.add(tokens.get(2)); h.put(tokens.get(0), o); if(count==100000000) break;
	 * tokens=null; //java.lang.Runtime.getRuntime().gc(); }
	 * 
	 * System.out.println(h.size()); }
	 */

	/**
	 * efficient way to read a graph. Uses Google Guava library to represent the
	 * graph compactly
	 * 
	 * @param kb
	 * @return
	 * @throws Exception
	 */

	public static AdjListCompact readGraphEfficientAlternate(String kb, String relMap, String nodeMap)
			throws Exception {

		MutableValueGraph<Integer, ArrayList<Integer>> weightedGraph = ValueGraphBuilder.directed()
				.allowsSelfLoops(true).build();
		HashMap<Integer, Integer> h1 = new HashMap<Integer, Integer>();

		BufferedReader br = new BufferedReader(new FileReader(kb));
		String line;

		while ((line = br.readLine()) != null) {
			StringTokenizer tok = new StringTokenizer(line, "\t");
			ArrayList<String> tokens = new ArrayList<String>();
			while (tok.hasMoreTokens()) 
			{
				tokens.add(tok.nextToken());
			}
			if (tokens.size() != 3)
				continue;
			Integer a = (Integer) Integer.parseInt(tokens.get(0));
			Integer b = (Integer) Integer.parseInt(tokens.get(2));
			Optional c1 = weightedGraph.edgeValue(a, b);
			if (c1.isPresent()) {
				ArrayList<Integer> c = (ArrayList<Integer>) c1.get();
				c.add(Integer.parseInt(tokens.get(1)));
				weightedGraph.putEdgeValue(a, b, c);
			} else {
				ArrayList<Integer> c = new ArrayList<Integer>();
				c.add(Integer.parseInt(tokens.get(1)));
				weightedGraph.putEdgeValue(a, b, c);
			}

		}
		br.close();
		HashMap<String, Integer> relmap = new HashMap<String, Integer>();
		HashMap<Integer, String> relmap1 = new HashMap<Integer, String>();
		BufferedReader br1 = new BufferedReader(new FileReader(relMap));
		while ((line = br1.readLine()) != null) {
			StringTokenizer tok = new StringTokenizer(line, "\t");
			ArrayList<String> tokens = new ArrayList<String>();
			while (tok.hasMoreTokens()) {
				tokens.add(tok.nextToken());
			}
			if (tokens.size() != 2)
				continue;
			relmap.put(tokens.get(0), Integer.parseInt(tokens.get(1)));
			relmap1.put(Integer.parseInt(tokens.get(1)), tokens.get(0));
		}
		br1.close();

		for (String s : relmap.keySet()) {
			for (String s1 : relmap.keySet()) {
				if (s.equals(s1))
					continue;
				if ((s1.endsWith("_inverse") && s1.replace("_inverse", "").equals(s))
						|| (s.endsWith("_inverse") && s.replace("_inverse", "").equals(s1))) {
					h1.put(relmap.get(s), relmap.get(s1));
					h1.put(relmap.get(s1), relmap.get(s));
				}
			}
		}

		HashMap<Integer, String> nodeIndex = new HashMap<Integer, String>();
		HashMap<String, Integer> nodeIndex1 = new HashMap<String, Integer>();
		BufferedReader br2 = new BufferedReader(new FileReader(nodeMap));
		while ((line = br2.readLine()) != null) {
			StringTokenizer tok = new StringTokenizer(line, "\t");
			ArrayList<String> tokens = new ArrayList<String>();
			while (tok.hasMoreTokens()) {
				tokens.add(tok.nextToken());
			}
			if (tokens.size() != 2)
				continue;
			nodeIndex.put(Integer.parseInt(tokens.get(1)), tokens.get(0));
			nodeIndex1.put(tokens.get(0), Integer.parseInt(tokens.get(1)));
		}

		AdjListCompact aa = new AdjListCompact(ImmutableValueGraph.copyOf(weightedGraph));
		aa.setInverses(h1);
		aa.setRelMap(relmap);
		aa.nodeMap = nodeIndex;
		aa.nodeMap1 = nodeIndex1;
		aa.relmap1 = relmap1;
		// Runtime r = Runtime.getRuntime();
		// r.gc();
		return aa;

	}
	
	/**
	 * reads the graph without considering the directions
	 * @param kb
	 * @param relMap
	 * @param nodeMap
	 * @return
	 * @throws Exception
	 */

	public static AdjListCompact readGraphEfficientAlternateUnd(String kb, String relMap, String nodeMap)
			throws Exception {

		MutableValueGraph<Integer, Double> weightedGraph = ValueGraphBuilder.undirected().allowsSelfLoops(true).build();
		HashMap<Integer, Integer> h1 = new HashMap<Integer, Integer>();

		BufferedReader br = new BufferedReader(new FileReader(kb));
		String line;

		while ((line = br.readLine()) != null) {
			StringTokenizer tok = new StringTokenizer(line, "\t");
			ArrayList<String> tokens = new ArrayList<String>();
			while (tok.hasMoreTokens()) 
			{
				tokens.add(tok.nextToken());
			}
			if (tokens.size() != 3)
				continue;
			Integer a = (Integer) Integer.parseInt(tokens.get(0));
			Integer b = (Integer) Integer.parseInt(tokens.get(2));
			Optional c1 = weightedGraph.edgeValue(a, b);
			if (c1.isPresent()) {
				continue;
			} else {
				
				weightedGraph.putEdgeValue(a, b, 1.0);
			}

		}
		br.close();
		HashMap<String, Integer> relmap = new HashMap<String, Integer>();
		HashMap<Integer, String> relmap1 = new HashMap<Integer, String>();
		BufferedReader br1 = new BufferedReader(new FileReader(relMap));
		while ((line = br1.readLine()) != null) {
			StringTokenizer tok = new StringTokenizer(line, "\t");
			ArrayList<String> tokens = new ArrayList<String>();
			while (tok.hasMoreTokens()) {
				tokens.add(tok.nextToken());
			}
			if (tokens.size() != 2)
				continue;
			relmap.put(tokens.get(0), Integer.parseInt(tokens.get(1)));
			relmap1.put(Integer.parseInt(tokens.get(1)), tokens.get(0));
		}
		br1.close();

		for (String s : relmap.keySet()) {
			for (String s1 : relmap.keySet()) {
				if (s.equals(s1))
					continue;
				if ((s1.endsWith("_inverse") && s1.replace("_inverse", "").equals(s))
						|| (s.endsWith("_inverse") && s.replace("_inverse", "").equals(s1))) {
					h1.put(relmap.get(s), relmap.get(s1));
					h1.put(relmap.get(s1), relmap.get(s));
				}
			}
		}

		HashMap<Integer, String> nodeIndex = new HashMap<Integer, String>();
		HashMap<String, Integer> nodeIndex1 = new HashMap<String, Integer>();
		BufferedReader br2 = new BufferedReader(new FileReader(nodeMap));
		while ((line = br2.readLine()) != null) {
			StringTokenizer tok = new StringTokenizer(line, "\t");
			ArrayList<String> tokens = new ArrayList<String>();
			while (tok.hasMoreTokens()) {
				tokens.add(tok.nextToken());
			}
			if (tokens.size() != 2)
				continue;
			nodeIndex.put(Integer.parseInt(tokens.get(1)), tokens.get(0));
			nodeIndex1.put(tokens.get(0), Integer.parseInt(tokens.get(1)));
		}
		
		AdjListCompact aa = new AdjListCompact();
		aa.setUndGraph(ImmutableValueGraph.copyOf(weightedGraph));

		//AdjListCompact aa = new AdjListCompact(ImmutableValueGraph.copyOf(weightedGraph));
		aa.setInverses(h1);
		aa.setRelMap(relmap);
		aa.nodeMap = nodeIndex;
		aa.nodeMap1 = nodeIndex1;
		aa.relmap1 = relmap1;
		// Runtime r = Runtime.getRuntime();
		// r.gc();
		return aa;

	}

	
	public static MutableValueGraph<String, Integer> readGraphEfficientWiki(String kb) throws Exception 
	{

		MutableValueGraph<String, Integer> weightedGraph = ValueGraphBuilder.directed().allowsSelfLoops(true)
				.build();

		BufferedReader br = new BufferedReader(new FileReader(kb));
		String line;

		while ((line = br.readLine()) != null) 
		{
			StringTokenizer tok = new StringTokenizer(line, "\t");
			ArrayList<String> tokens = new ArrayList<String>();
			while (tok.hasMoreTokens()) 
			{
				tokens.add(tok.nextToken());
			}
			if (tokens.size() != 2)
				continue;
			String a = tokens.get(0);
			String b = tokens.get(1);
			weightedGraph.putEdgeValue(a, b, 1);	
		}
		// Runtime r = Runtime.getRuntime();
		// r.gc();
		return weightedGraph;

	}
	
	/**
	 * add the graph written in graph file to wiki
	 * @param wiki
	 * @param graph
	 * @return
	 * @throws Exception
	 */
	
	public static MutableValueGraph<String, Integer> addGraph(MutableValueGraph<String, Integer> wiki, String graph) throws Exception
	{
		BufferedReader br = new BufferedReader(new FileReader(graph));
		String line;

		while ((line = br.readLine()) != null) 
		{
			StringTokenizer tok = new StringTokenizer(line, "\t");
			ArrayList<String> tokens = new ArrayList<String>();
			while (tok.hasMoreTokens()) 
			{
				tokens.add(tok.nextToken());
			}
			if (tokens.size() != 2)
				continue;
			String a = tokens.get(0);
			String b = tokens.get(1);
			wiki.putEdgeValue(a, b, 1);	
		}
		return wiki;
	}

	/**
	 * reading the graph without creating the supporting data structures
	 * 
	 * @param kb
	 * @return
	 * @throws Exception
	 */

	public static AdjListCompact readGraphEfficient1(String kb) throws Exception {

		MutableValueGraph<Integer, ArrayList<Integer>> weightedGraph = ValueGraphBuilder.directed()
				.allowsSelfLoops(true).build();

		BufferedReader br = new BufferedReader(new FileReader(kb));
		String line;

		while ((line = br.readLine()) != null) {
			StringTokenizer tok = new StringTokenizer(line, "\t");
			ArrayList<String> tokens = new ArrayList<String>();
			while (tok.hasMoreTokens()) {
				tokens.add(tok.nextToken());
			}
			if (tokens.size() != 3)
				continue;
			Integer a = (Integer) Integer.parseInt(tokens.get(0));
			Integer b = (Integer) Integer.parseInt(tokens.get(2));
			Optional c1 = weightedGraph.edgeValue(a, b);
			if (c1.isPresent()) {
				ArrayList<Integer> c = (ArrayList<Integer>) c1.get();
				c.add(Integer.parseInt(tokens.get(1)));
				weightedGraph.putEdgeValue(a, b, c);
			} else {
				ArrayList<Integer> c = new ArrayList<Integer>();
				c.add(Integer.parseInt(tokens.get(1)));
				weightedGraph.putEdgeValue(a, b, c);
			}

		}

		// Runtime r = Runtime.getRuntime();
		// r.gc();
		return new AdjListCompact(ImmutableValueGraph.copyOf(weightedGraph));

	}

	/**
	 * reads the selected meta paths listed inside folder2 and returns a list of
	 * lists
	 * 
	 * @param folder1: grandparent folder
	 * @param folder2: parent folder
	 * @return
	 * @throws Exception
	 */

	public ArrayList<ArrayList<String>> readMetaPathList(String folder1, String folder2) throws Exception {
		BufferedReader br1 = new BufferedReader(new FileReader(folder1 + "/" + folder2 + "/selected_meta-paths"));
		String line;
		ArrayList<ArrayList<String>> list1 = new ArrayList<ArrayList<String>>();
		while ((line = br1.readLine()) != null) {
			ArrayList<String> list = new ArrayList<String>();
			StringTokenizer tok = new StringTokenizer(line, " ");
			while (tok.hasMoreTokens()) {
				list.add(tok.nextToken());
			}
			list1.add(list);
		}
		return list1;
	}
	
	/**
	 * older code to read the graph efficient way to read a graph. All nodes are
	 * string type. Uses Google Guava library to represent the graph compactly
	 * 
	 * @param kb
	 * @return
	 * @throws Exception
	 */

	public static AdjListCompactOld readGraphEfficient(String kb) throws Exception 
	{

		MutableValueGraph<String, ArrayList<String>> weightedGraph = ValueGraphBuilder.directed().allowsSelfLoops(true)
				.build();

		BufferedReader br = new BufferedReader(new FileReader(kb));
		String line;

		while ((line = br.readLine()) != null) 
		{
			StringTokenizer tok = new StringTokenizer(line, "\t");
			ArrayList<String> tokens = new ArrayList<String>();
			while (tok.hasMoreTokens()) 
			{
				tokens.add(tok.nextToken());
			}
			if (tokens.size() != 3)
				continue;
			String a = tokens.get(0);
			String b = tokens.get(2);
			Optional c1 = weightedGraph.edgeValue(a, b);
			if (c1.isPresent()) {
				ArrayList<String> c = (ArrayList<String>) c1.get();
				c.add(tokens.get(1));
				weightedGraph.putEdgeValue(a, b, c);
			} 
			else 
			{
				ArrayList<String> c = new ArrayList<String>();
				c.add(tokens.get(1));
				weightedGraph.putEdgeValue(a, b, c);
			}

			// tok=null;
			// tokens=null;

		}
		// Runtime r = Runtime.getRuntime();
		// r.gc();
		return new AdjListCompactOld(weightedGraph);

	}
	/**
	 * 
	 * @param relation
	 * @param outfile
	 * @param kb1
	 * @param kb2
	 * @param length
	 * @param relmap1
	 * @param relmap2
	 * @param nodeMap1
	 * @param nodeMap2
	 * @throws Exception
	 */

	public static void generateMetaPathsOfLengthMaster(String relation, String outfile, String kb1, String kb2,
			int length) throws Exception {
		BufferedWriter bw = new BufferedWriter(new FileWriter(outfile));
		AdjListCompactOld a1 = readGraphEfficient(kb1);
		System.out.println("read first graph");
		AdjListCompactOld a2 = readGraphEfficient(kb2);
		System.out.println("read second graph");
		HashMap<String, Boolean> visited = new HashMap<String, Boolean>();
		
		ArrayList<String> path = new ArrayList<String>();
		//a2.findAllPaths("scieneer_common_lisp","scieneer_common_lisp", "common_lisp", visited, path, bw, length,1);
		//bw.close();

		
		  Set<EndpointPair> pair = a1.returnTriplesForEdgeLabel(relation);
		  System.out.println("retrieved pairs for relation");
		  
		  int c1=0; for(EndpointPair p:pair) 
		  { 
			  c1++; 
			  visited = new HashMap<String, Boolean>();
				path = new ArrayList<String>();
			  String u = (String) p.nodeU();
			  String v = (String) p.nodeV();
		  
		  //a2.findPaths(u, v, length, bw);
		  a2.findAllPaths(u, u, v, visited, path, bw, length, 1);
		  
		  System.out.println(c1); 
		  }
		 
		bw.close();
		System.out.println("found paths");
		a2.enumeratePaths(outfile);
		/*BufferedReader br = new BufferedReader(new FileReader(outfile));
		BufferedWriter bw1 = new BufferedWriter(new FileWriter(outfile.replace(".tsv", "") + "_with_counts.tsv"));
		String line;
		HashMap<String, Integer> meta_paths = new HashMap<String, Integer>();

		while ((line = br.readLine()) != null) {
			StringTokenizer tok = new StringTokenizer(line, "\t");
			ArrayList<String> list1 = new ArrayList<String>();
			while (tok.hasMoreTokens()) {
				list1.add(tok.nextToken());
			}
			String l = "";
			for (int k = list1.size() - 2; k >= 1; k = k - 2) {
				l = l + list1.get(k) + "\t";
			}
			if (meta_paths.get(l) != null) {
				meta_paths.put(l, meta_paths.get(l) + 1);
			} else {
				meta_paths.put(l, 1);
			}
		}
		ArrayList<Entry<String, Integer>> aa1 = new ArrayList<Entry<String, Integer>>(meta_paths.entrySet());
		Collections.sort(aa1, valueComparator);
		for (Entry<String, Integer> dd : aa1) {
			bw1.write(dd.getKey() + dd.getValue() + "\n");
		}
		bw1.close();*/

	}
	
	public void enumerate(String kb, String file) throws Exception
	{
		AdjListCompactOld a1 = readGraphEfficient(kb);
		System.out.println("read first graph");
		a1.enumeratePaths(file);
		
	}

	public static boolean determinePathsBetween(String teknowbase_mapped, String relmap, String nodemap, String file) throws Exception
	{
		  Aspect a = new Aspect();
		  AdjListCompact a1 = a.readGraphEfficientAlternate(teknowbase_mapped, relmap,nodemap); 
		  System.out.println("read graph and it worked");
	
	 	  BufferedReader br1 = new BufferedReader(new FileReader(file));
		  
		  HashSet<Integer> paths = new HashSet<Integer>(); 
		  HashMap<String, Double> path_scores = new HashMap<String, Double>();
		  String line;
		  while((line=br1.readLine())!=null) 
		  { 
			  StringTokenizer tok = new StringTokenizer(line," ");
			  ArrayList<String> tokens = new ArrayList<String>(); 
			  while(tok.hasMoreTokens()) 
			  {
				  tokens.add(tok.nextToken()); 
			  }
			  int ccc=0;
			  String pp="";
			  for(String t:tokens)
			  {
				  if(ccc<tokens.size()-1)
				  {
					  if(ccc==0)
						  pp=pp+t;
					  else
						  pp = pp + " "+t;
					  paths.add((a1.relmap.get(t)));
				  }
				  ccc++;
			  }
			  double score = Double.parseDouble(tokens.get(tokens.size()-1));
			  path_scores.put(pp, score);
		  }
		  
		  System.out.println("read all the meta-paths");
		  a1.getListOfNeighborsForEdgeLabel(new HashSet<Integer>(paths));
		  System.out.println("created index on all relations in meta-paths");
		  boolean exists_f=false;
		  while(true)
		  {
			  Scanner in = new Scanner(System.in);
			  System.out.println("Enter source");
			  String start = in. nextLine();
			  
			  System.out.println("Enter destination");
			  String dest = in. nextLine();
			  
			  int source=0;
			  int destination=0;
			  if(a1.nodeMap1.get(start)!=null && a1.nodeMap1.get(dest)!=null)
			  {
				   source = a1.nodeMap1.get(start);
				   destination = a1.nodeMap1.get(dest);
			  }
			  else
			  {
				  System.out.println("either the source or destination does not exist in the knowledge graph. Please enter another pair.");
				  
				  continue;
			  }
			  
			  for(String p:path_scores.keySet()) 
			  {
				  //System.out.println(p); 
				  StringTokenizer  tok = new StringTokenizer(p," "); 
				  ArrayList<String> tokens = new ArrayList<String>();
				  while(tok.hasMoreTokens())
				  {
					  tokens.add(tok.nextToken());
				  }
				  int ccc=0;
				  String pp="";
				  ArrayList<Integer> path1 = new ArrayList<Integer>();
				  for(String t:tokens)
				  {
					 
						  path1.add(a1.relmap.get(t));
					  
				  }
				
				/* boolean exists = a1.checkPathExists( source, destination, path1, path_scores.get(p)); 
				 exists_f = exists_f || exists;
				 if(exists_f) 
				 {
					 System.out.println("path is: "+p);
					 break;
				 }*/
				 // System.out.print("\npath is: "+p+"\n");
				  
				 a1.printPath(source, destination, path1, 0, new ArrayList<Integer>());
			  } 
			 // System.out.println(exists_f);
		  }
		  
	}
	
	public static void computeProbabilityPRAMaster(String teknowbase_mapped, String relmap, String nodemap, String queries1, String file, String prob_file) throws Exception
	{
		System.out.println(teknowbase_mapped);
		System.out.println(relmap);
		System.out.println(nodemap);
		
		Aspect a = new Aspect();
		//a.test(hm.get("teknowbase"), hm.get("outfile"), Integer.parseInt(hm.get("length-meta-path")));
		//a.generateMetaPathsOfLengthMaster(hm.get("relation"), hm.get("outfile"), hm.get("teknowbase"),hm.get("teknowbase"), Integer.parseInt(hm.get("length-meta-path")), hm.get("relmap"), hm.get("relmap"),hm.get("nodemap"), hm.get("nodemap"));
		//a.enumerate(hm.get("teknowbase"), hm.get("outfile"));
		
		  AdjListCompact a1 = a.readGraphEfficientAlternate(teknowbase_mapped, relmap, nodemap); 
		  System.out.println("read graph and it worked");
		  //ArrayList<Integer> path = new ArrayList<Integer>();
		  
		  //path.add(a1.relmap.get(hm.get("edge1")));
		  //path.add(a1.relmap.get(hm.get("edge2"))); ArrayList<String> queries = new ArrayList<String>(); 
		  ArrayList<String> queries = new ArrayList<String>();
		  BufferedReader br = new BufferedReader(new FileReader(queries1)); 
		  String line;
		  while((line=br.readLine())!=null) 
		  { 
			  queries.add(line); 
		  } 
		  br.close();
		  
		  //int source = a1.nodeMap1.get(hm.get("source"));
		  
		  BufferedReader br1 = new BufferedReader(new FileReader(file));
		  HashMap<Integer, HashMap<Integer, Double>> prob_list = new HashMap<Integer,
		  HashMap<Integer, Double>>();
		  
		  HashSet<Integer> paths = new HashSet<Integer>(); HashMap<String, Double>
		  path_scores = new HashMap<String, Double>();

		  while((line=br1.readLine())!=null) 
		  { 
			  StringTokenizer tok = new StringTokenizer(line," ");
			  ArrayList<String> tokens = new ArrayList<String>(); 
			  while(tok.hasMoreTokens()) 
			  {
				  tokens.add(tok.nextToken()); 
			  }
		  //System.out.println(tokens.get(0)+" "+tokens.get(1));
		  //System.out.println(tokens.get(1));
			  int ccc=0;
			  String pp="";
			  for(String t:tokens)
			  {
				  if(ccc<tokens.size()-1)
				  {
					  if(ccc==0)
						  pp=pp+t;
					  else
						  pp = pp + " "+t;
					  paths.add((a1.relmap.get(t)));
				  }
				  ccc++;
			  }
			  System.out.println("to be double: |"+tokens.get(tokens.size()-1)+"|");
			  int score = Integer.parseInt(tokens.get(tokens.size()-1));
			  path_scores.put(pp, (double)score);
		  
		  }
		 
		  System.out.println("read all the meta-paths");
		  a1.getListOfNeighborsForEdgeLabel(new HashSet<Integer>(paths));
		  System.out.println("created index on all relations in meta-paths");
		  
		  for(String p:path_scores.keySet()) 
		  { //System.out.println(p); 
			  StringTokenizer  tok = new StringTokenizer(p," "); 
			  ArrayList<String> tokens = new ArrayList<String>();
			  while(tok.hasMoreTokens())
			  {
				  tokens.add(tok.nextToken());
			  }
			  int ccc=0;
			  String pp="";
			  ArrayList<Integer> path1 = new ArrayList<Integer>();
			  for(String t:tokens)
			  {
				  if(ccc<tokens.size()-1)
				  {
					  if(ccc==0)
						  pp=pp+t;
					  else
						  pp = pp + " "+t;
					  path1.add((a1.relmap.get(t)));
				  }
				  ccc++;
			  }
			  
			  for(String q:queries) 
			  { //System.out.println(q); 
				  if(a1.nodeMap1.get(q)==null) continue;
				  int source = a1.nodeMap1.get(q);
				  a1.pathConstrainedRandomWalk(path1, source, path_scores.get(p), p, prob_list); 
			  } 
		  } 
		  System.out.println("computed scores"); 
		  BufferedWriter bw = new BufferedWriter(new FileWriter(prob_file)); 
		  for(int i:prob_list.keySet()) 
		  { 
			  double total=0; 
			  for(int j:prob_list.get(i).keySet())
			  { 
				  total = total + prob_list.get(i).get(j);
				  bw.write(a1.nodeMap.get(i)+"\t"+a1.nodeMap.get(j)+"\t"+prob_list.get(i).get(j )+"\n"); 
			  }
		  
		  }
		  
		  bw.close();
		  
		  br1.close();
	}
	

	public static void main(String args[]) throws Exception 
	{
		
		//generateMetaPathsOfLengthMaster(hm.get("relation"), hm.get("file"), hm.get("teknowbase-mapped"), hm.get("teknowbase-mapped"),Integer.parseInt(hm.get("length")));
		
		//generateKeywordsMaster(hm.get("location_master"), hm.get("entities"));
		//filterPhrasesWithEntities(hm.get("results_to_be_filtered"),hm.get("query"),hm.get("entities"),hm.get("results_to_be_written"), hm.get("bad_entities"));
		
		//a.randomWalkMasterHPC(hm.get("teknowbase-mapped"), hm.get("outfile1"), hm.get("folder"), hm.get("relmap"), hm.get("meta-path"),hm.get("nodemap"));
		
		//computeProbabilityPRAMaster(hm.get("teknowbase-mapped"), hm.get("relmap"),hm.get("nodemap"),hm.get("queries"),hm.get("file"),hm.get("prob-file"));
		//determinePathsBetween(hm.get("teknowbase-mapped"),hm.get("relmap"),hm.get("nodemap"), hm.get("file"));
		
		// test();
		// e.connect("evaluation", "root", "admin", "tkbforir_evaluation");
		// e.getPapersForAllQueries(hm.get("query-file"), hm.get("parent-folder"),
		// hm.get("heuristics"));
	
		 
		// a.computeProbabilityDistribution(Integer.parseInt(hm.get("source")),
		// hm.get("implementation-random-walk"), hm.get("prob-file"));
		// normalizeVectorsParallel(hm.get("embedding-application"),hm.get("embeddingEntities"));
		// findMaxMin(hm.get("embedding-application"), hm.get("maxmin"));
		// normalizeVectors(hm.get("embedding-application"), hm.get("outfile"),
		// hm.get("maxmin"), hm.get("queries"));
		
		// AdjListCompactOld a1 = a.readGraphEfficient(hm.get("teknowbase"));
		// DBAdjList d = new
		// DBAdjList("evaluation","tkbforir","123456","tkbforir_evaluation");
		// a1.addTriplesAnotherKB(hm.get("dbpedia-original"), hm.get("outfile"));
		// a.randomWalkMasterHPC(hm.get("yago"), hm.get("outfile"),
		// hm.get("parent-folder"), hm.get("yago-relmap"), hm.get("relation"),"");
		// a.randomWalkMaster(hm.get("dbpedia"), hm.get("outfile"),
		// hm.get("parent-folder"));
		// readGraphEfficient(hm.get("dbpedia"));
		// extractImportantPaths1(hm.get("parent-folder"), hm.get("outfile"));
		// insert_statement(hm.get("freebase-postprocessed"),"freebase_facts");
		// replaceNullCharcaters(hm.get("freebase"),hm.get("freebase-postprocessed"));
		// generateMetaPathsOfLengthDatabase(hm.get("relation"), hm.get("outfile"),
		// hm.get("latest-category-graph-no-duplicates"), hm.get("table-name1"),
		// Integer.parseInt(hm.get("length-meta-path")));

	}
}


