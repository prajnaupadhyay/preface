package main.java.apps;

/**
 * class to read LM representations of the facets and rank them
 */

public class Rank
{
    public static void main(String args[])
    {
        GetPropertyValues properties = new GetPropertyValues();
        HashMap<String, String> hm = properties.getPropValues();
        rank(hm.get("wikipedia"),hm.get("queries"),hm.get("phraseFolder"),hm.get("refdFolder"),hm.get("teknowbase-mapped"),hm.get("nodemap"),hm.get("relmap"));
    }
}
