package apps;
import facets.*;

import java.util.HashMap;

/**
 * class to estimate probabilities for the 2 components of our retrieval model
 */

public class Estimation
{
    public static void estimate(String wiki, String queries, String phraseFolder, String refdFolder, String tkb_mapped, String nodemap, String relmap) throws Exception
    {
        facets.PreFace.computeRefDScores(queries, wiki, phraseFolder, refdFolder);
        facets.PreFace.starClusterEntities(queries, tkb_mapped, nodemap, relmap);

    }

    public static void main(String args[]) throws Exception
    {
        main.java.apps.GetPropertyValues properties = new main.java.apps.GetPropertyValues();
        HashMap<String, String> hm = properties.getPropValues();
        estimate(hm.get("wikipedia"),hm.get("queries"),hm.get("phraseFolder"),hm.get("refdFolder"),hm.get("teknowbase-mapped"),hm.get("nodemap"),hm.get("relmap"));
    }
}
