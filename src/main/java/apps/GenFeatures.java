package apps;

import java.util.HashMap;

/**
 * Generates BOW+BOE features for each keyphrase extracted from top-1000 results of a query
 */

public class GenFeatures
{
    public static void genFeatures(String queries, String teknowbase_mapped, String nodemap, String relmap, String phraseFolder, String outputFolder) throws Exception
    {
        facets.PreFace.computePhraseBOE(queries, teknowbase_mapped, nodemap, relmap, phraseFolder, outputFolder);
    }

    public static void main(String args[]) throws Exception
    {
        main.java.apps.GetPropertyValues properties = new main.java.apps.GetPropertyValues();
        HashMap<String, String> hm = properties.getPropValues();

        genFeatures(hm.get("queries"),hm.get("teknowbase-mapped"),hm.get("nodemap"),hm.get("relmap"),hm.get("phraseFolder"),hm.get("outputFolder"));
    }
}
