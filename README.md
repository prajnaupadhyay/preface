#Source code of PreFace
There are four main parts in this code. First, compile the project using maven. Go to the source PrefaceCode and run the following command to compile the maven project. The dependencies are listed in pom.xml file.

mvn -e install -DskipTests

This will create a jar file at target/PrefaceCode-1.0-SNAPSHOT.jar. Use this jar to run the following functions to get results. Some parts of the code come with python scripts while the rest can be executed using this compiled jar.

## Extract Phrases
This script is used to extract phrases from the search results for each query. It is located under src/python/. Run the following command to extract phrases:

python extract_and_rank_phrases.py query_file search_results output 

###1) query_file: file containing the list of queries, each query in a single line

###2) search_results: folder containing the search results for each query. The folder consists of files named after each query. Each file is tab separated consisting of the following fields:

doc_title	doc_text	score

i) doc_title: title of the document. If there is no title, then leave this field blank

ii) doc_text: text of the document

iii) score: score assigned by the retrieval system for this document. If there is no score available, set it to 1.

###3) output: this points to a folder which will store the extracted phrases. Each file containing the phrases is named after the query.

## Cluster Phrases

The extracted phrases should now be clustered using features from TeKnowbase. To first construct the features for the phrases, use the following command:

java -cp target/PrefaceCode-1.0-SNAPSHOT.jar apps.GenFeatures

This command will create a folder phrase_boe under which the phrase representations will be stored. The next step is to cluster these phrases. Use the following command:

python hcluster.py

## Retrieval
This component performs the estimation and ranking of the facets. Run the following:

java -cp target/PrefaceCode-1.0-SNAPSHOT.jar apps.Estimation
java -cp target/PrefaceCode-1.0-SNAPSHOT.jar apps.Rank

# Configuration parameters
PreFace needs a number of parameters to be set to run:

## 1) queries: This parameter stores the location to the file containing queries. Each query is written on each line of the file.
## 2) wikipedia: Location to the wikipedia data dump
## 3) phraseFolder:
## 4) refdFolder:
## 5) teknowbase-mapped:
## 6) nodemap:
## 7) relmap:


